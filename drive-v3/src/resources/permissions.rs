use reqwest::Method;
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{objects, Credentials};

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/permissions",
    returns=objects::Permission,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to create a permission for a file or shared drive.
pub struct CreateRequest {
    /// A plain text custom message to include in the notification email.
    #[drive_v3(parameter)]
    email_message: Option<String>,

    /// This parameter will only take effect if the item is not in a shared
    /// drive and the request is attempting to transfer the ownership of the
    /// item.
    ///
    /// If set to `true`, the item will be moved to the new owner's My Drive
    /// root folder and all prior parents removed. If set to `false`,
    /// parents are not changed.
    #[drive_v3(parameter)]
    move_to_new_owners_root: Option<bool>,

    /// Whether to send a notification email when sharing to users or
    /// groups.
    ///
    /// This defaults to `true` for users and groups, and is not allowed for
    /// other requests. It must not be disabled for ownership transfers.
    #[drive_v3(parameter)]
    send_notification_email: Option<bool>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Whether to transfer ownership to the specified user and downgrade
    /// the current owner to a writer.
    ///
    /// This parameter is required as an acknowledgement of the side effect.
    #[drive_v3(parameter)]
    transfer_ownership: Option<bool>,

    /// Issue the request as a domain administrator.
    ///
    /// if set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,

    /// The permission which will be applied.
    #[drive_v3(body)]
    permission: Option<objects::Permission>,
}

#[request(
    method=Method::DELETE,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/permissions/{permission_id}",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
/// A request builder to delete a permission.
pub struct DeleteRequest {
    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Issue the request as a domain administrator.
    ///
    /// if set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/permissions/{permission_id}",
    returns=objects::Permission,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a permission by ID.
pub struct GetRequest {
    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Issue the request as a domain administrator.
    ///
    /// if set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/permissions",
    returns=objects::PermissionList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list a file's or shared drive's permissions.
pub struct ListRequest {
    /// The maximum number of permissions to return per page.
    ///
    /// When not set for files in a shared drive, at most 100 results will
    /// be returned. When not set for files that are not in a shared drive,
    /// the entire list will be returned.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`next_page_token`](objects::PermissionList::next_page_token) from
    /// the previous response.
    #[drive_v3(parameter)]
    page_token: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Issue the request as a domain administrator.
    ///
    /// if set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,
}

#[request(
    method=Method::PATCH,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/permissions/{permission_id}",
    returns=objects::Permission,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to update a permission for a file or shared drive.
pub struct UpdateRequest {
    /// Whether to remove the expiration date.
    #[drive_v3(parameter)]
    remove_expiration: Option<bool>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Whether to transfer ownership to the specified user and downgrade
    /// the current owner to a writer.
    ///
    /// This parameter is required as an acknowledgement of the side effect.
    #[drive_v3(parameter)]
    transfer_ownership: Option<bool>,

    /// Issue the request as a domain administrator.
    ///
    /// if set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,

    /// The permission which will be applied.
    #[drive_v3(body)]
    permission: Option<objects::Permission>,
}

/// Permissions for a file.
///
/// A permission grants a user, group, domain, or the world access to a file or
/// a folder hierarchy.
///
/// Some resource methods (such as [`permissions.update`](Permissions::update))
/// require a `permission_id`. Use the [`permissions.list`](Permissions::list)
/// method to retrieve the ID for a file, folder, or shared drive.
///
/// # Examples:
///
/// List the permission in a file
///
/// ```no_run
/// # use drive_v3::{Error, Credentials, Drive};
/// #
/// # let drive = Drive::new( &Credentials::from_file(
/// #     "../.secure-files/google_drive_credentials.json",
/// #     &["https://www.googleapis.com/auth/drive.file"],
/// # )? );
/// #
/// let file_id = "some-file-id";
/// let permission_list = drive.permissions.list(&file_id).execute()?;
///
/// if let Some(permissions) = permission_list.permissions {
///     for permission in permissions {
///         println!("{}", permission);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Permissions {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Permissions {
    /// Creates a new [`Permissions`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Creates a permission for a file or shared drive.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/permissions/create)
    /// for more information.
    ///
    /// # Warning
    ///
    /// Concurrent permissions operations on the same file are not supported;
    /// only the last update is applied.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Permission;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let permission = Permission {
    ///     permission_type: Some( "anyone".to_string() ),
    ///     role: Some( "reader".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let file_id = "some-file-id";
    ///
    /// let created_permission = drive.permissions.create(&file_id)
    ///     .permission(&permission)
    ///     .execute()?;
    ///
    /// assert_eq!(created_permission.permission_type, permission.permission_type);
    /// assert_eq!(created_permission.role, permission.role);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn create<T: AsRef<str>> ( &self, file_id: T ) -> CreateRequest {
        CreateRequest::new(&self.credentials, file_id)
    }

    /// Deletes a permission.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/permissions/delete)
    /// for more information.
    ///
    /// # Warning
    ///
    /// Concurrent permissions operations on the same file are not supported;
    /// only the last update is applied.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let permission_id = "some-permission-id";
    ///
    /// let response = drive.permissions.delete(&file_id, &permission_id).execute();
    ///
    /// assert!( response.is_ok() );
    /// # Ok::<(), Error>(())
    /// ```
    pub fn delete<T, U> ( &self, file_id: T, permission_id: U ) -> DeleteRequest
        where
            T: AsRef<str>,
            U: AsRef<str>
    {
        DeleteRequest::new(&self.credentials, file_id, permission_id)
    }

    /// Gets a permission by ID.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/permissions/get)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let permission_id = "some-permission-id";
    ///
    /// let permission = drive.permissions.get(&file_id, &permission_id).execute()?;
    ///
    /// println!("This is the file's permission:\n{}", permission);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get<T, U> ( &self, file_id: T, permission_id: U ) -> GetRequest
        where
            T: AsRef<str>,
            U: AsRef<str>
    {
        GetRequest::new(&self.credentials, file_id, permission_id)
    }

    /// Lists a file's or shared drive's permissions.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/permissions/list)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let permission_list = drive.permissions.list(&file_id).execute()?;
    ///
    /// if let Some(permissions) = permission_list.permissions {
    ///     for permission in permissions {
    ///         println!("{}", permission);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list<T: AsRef<str>> ( &self, file_id: T ) -> ListRequest {
        ListRequest::new(&self.credentials, file_id)
    }

    /// Updates a permission with patch semantics.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/permissions/update)
    /// for more information.
    ///
    /// # Warning
    ///
    /// Concurrent permissions operations on the same file are not supported;
    /// only the last update is applied.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Permission;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let updated_permission = Permission {
    ///     permission_type: Some( "anyone".to_string() ),
    ///     role: Some( "commenter".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let file_id = "some-file-id";
    /// let permission_id = "some-permission-id";
    ///
    /// let permission = drive.permissions.update(&file_id, &permission_id)
    ///     .permission(&updated_permission)
    ///     .execute()?;
    ///
    /// assert_eq!(permission.permission_type, updated_permission.permission_type);
    /// assert_eq!(permission.role, updated_permission.role);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn update<T, U> ( &self, file_id: T, permission_id: U ) -> UpdateRequest
        where
            T: AsRef<str>,
            U: AsRef<str>
    {
        UpdateRequest::new(&self.credentials, file_id, permission_id)
    }
}

#[cfg(test)]
mod tests {
    use super::Permissions;
    use crate::{ErrorKind, objects, resources};
    use crate::utils::test::{INVALID_CREDENTIALS, VALID_CREDENTIALS};

    fn get_resource() -> Permissions {
        Permissions::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Permissions {
        Permissions::new(&INVALID_CREDENTIALS)
    }

    fn get_files_resource() -> resources::Files {
        resources::Files::new(&VALID_CREDENTIALS)
    }

    fn delete_file( file: &objects::File ) -> crate::Result<()> {
        get_files_resource().delete( file.clone().id.unwrap() ).execute()
    }

    fn get_test_file_metadata() -> objects::File {
        objects::File {
            name: Some( "test.txt".to_string() ),
            description: Some( "a test file".to_string() ),
            mime_type: Some( "text/plain".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_drive_file() -> crate::Result<objects::File> {
        let metadata = get_test_file_metadata();

        get_files_resource().create()
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()
    }

    fn get_test_permission( ) -> objects::Permission {
        objects::Permission {
            permission_type: Some( "anyone".to_string() ),
            role: Some( "reader".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_drive_permission( file: &objects::File ) -> crate::Result<objects::Permission> {
        let test_permission = get_test_permission();

        get_resource().create( &file.clone().id.unwrap() )
            .permission(&test_permission)
            .execute()
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn create_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_permission = get_test_permission();

        let response = get_resource().create( &test_drive_file.clone().id.unwrap() )
            .permission(&test_permission)
            .execute();

        assert!( response.is_ok() );

        let permission = response.unwrap();
        assert_eq!(permission.permission_type, test_permission.permission_type);
        assert_eq!(permission.role, test_permission.role);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn create_invalid_test() {
        let response = get_invalid_resource().create("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn delete_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_permission = get_test_drive_permission(&test_drive_file).unwrap();

        let response = get_resource().delete(
            &test_drive_file.clone().id.unwrap(),
            &test_drive_permission.clone().id.unwrap(),
            )
            .execute();

        assert!( response.is_ok() );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn delete_invalid_test() {
        let response = get_invalid_resource().delete("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn get_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_permission = get_test_drive_permission(&test_drive_file).unwrap();

        let response = get_resource().get(
            &test_drive_file.clone().id.unwrap(),
            &test_drive_permission.clone().id.unwrap(),
            )
            .execute();

        assert!( response.is_ok() );

        let permission = response.unwrap();
        assert_eq!(permission, test_drive_permission);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn get_invalid_test() {
        let response = get_invalid_resource().get("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_permission = get_test_drive_permission(&test_drive_file).unwrap();

        let response = get_resource().list( &test_drive_file.clone().id.unwrap() )
            .execute();

        assert!( response.is_ok() );

        let permissions = response.unwrap().permissions.unwrap();
        assert!( permissions.contains(&test_drive_permission) );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn list_invalid_test() {
        let response = get_invalid_resource().list("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn update_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_permission = get_test_drive_permission(&test_drive_file).unwrap();

        let updated_permission = objects::Permission {
            role: Some( "commenter".to_string() ),
            ..Default::default()
        };

        let response = get_resource().update(
                &test_drive_file.clone().id.unwrap(),
                &test_drive_permission.clone().id.unwrap(),
            )
            .permission(&updated_permission)
            .execute();

        assert!( response.is_ok() );

        let permission = response.unwrap();
        assert_eq!(permission.role, updated_permission.role);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn update_invalid_test() {
        let response = get_invalid_resource().update("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
