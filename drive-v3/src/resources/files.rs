use std::fs;
use std::io::Read;
use std::path::PathBuf;
use std::collections::HashMap;
use reqwest::{Url, Method, header};
use reqwest::blocking::{Client, multipart, RequestBuilder};
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::utils::upload::FileUploader;
use crate::{Credentials, Error, ErrorKind, objects};

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/copy",
    returns=objects::File,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to create a copy of a file and apply any requested
/// updates with patch semantics.
pub struct CopyRequest {
    /// Whether to ignore the domain's default visibility settings for the
    /// created file.
    ///
    /// Domain administrators can choose to make all uploaded files visible
    /// to the domain by default; this parameter bypasses that behavior for
    /// the request. Permissions are still inherited from parent folders.
    #[drive_v3(parameter)]
    ignore_default_visibility: Option<bool>,

    /// Whether to set the `keepForever` field in the new head revision.
    ///
    /// This is only applicable to files with binary content in a Drive.
    ///
    /// Only 200 revisions for the file can be kept forever, if the limit is
    /// reached, try deleting pinned revisions.
    #[drive_v3(parameter)]
    keep_revision_forever: Option<bool>,

    /// A language hint for OCR processing during image import
    /// (ISO 639-1 code).
    #[drive_v3(parameter)]
    ocr_language: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,

    /// Sets the metadata that the copied file will have.
    #[drive_v3(body)]
    metadata: Option<objects::File>,
}

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/upload/drive/v3/files",
)]
#[derive(DriveRequestBuilder)]
/// A request builder to create a new file in a Drive.
pub struct CreateRequest {
    /// The type of upload request to the /upload URI.
    #[drive_v3(parameter)]
    upload_type: Option<objects::UploadType>,

    /// Whether to ignore the domain's default visibility settings for the
    /// created file.
    ///
    /// Domain administrators can choose to make all uploaded files visible
    /// to the domain by default; this parameter bypasses that behavior for
    /// the request. Permissions are still inherited from parent folders.
    #[drive_v3(parameter)]
    ignore_default_visibility: Option<bool>,

    /// Whether to set the `keepForever` field in the new head revision.
    ///
    /// This is only applicable to files with binary content in a Drive.
    ///
    /// Only 200 revisions for the file can be kept forever, if the limit is
    /// reached, try deleting pinned revisions.
    #[drive_v3(parameter)]
    keep_revision_forever: Option<bool>,

    /// A language hint for OCR processing during image import
    /// (ISO 639-1 code).
    #[drive_v3(parameter)]
    ocr_language: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Whether to use the uploaded content as indexable text.
    #[drive_v3(parameter)]
    use_content_as_indexable_text: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,

    /// Sets the metadata that the created file will have in Google Drive.
    #[drive_v3(body)]
    metadata: Option<objects::File>,

    /// The path of the source file to be uploaded.
    content_source: Option<PathBuf>,

    /// Use this string as the content of the created file.
    content_string: Option<String>,

    /// A callback to receive updates on a resumable upload.
    #[drive_v3(callback)]
    callback: Option<fn(usize, usize)>,
}

impl CreateRequest {
    /// Gets a media request.
    fn get_media_request( &self ) -> crate::Result<RequestBuilder> {
        let mut content_bytes = Vec::new();

        if self.content_source.is_some() && self.content_string.is_some() {
            return Err( Error::new(
                ErrorKind::Request,
                "a create request can only use one of 'content_source' or 'content_string'"
            ) )
        }

        if let Some(source) = &self.content_source {
            let mut file = fs::File::open(source)?;
            file.read_to_end(&mut content_bytes)?;
        }

        if let Some(string) = &self.content_string {
            content_bytes = string.as_bytes().to_vec();
        }

        let mut request = self.build()?    
            .header( "Content-Length", content_bytes.len().to_string() )
            .body(content_bytes);

        let metadata = self.metadata.clone().unwrap_or_default();
        if let Some(mime_type) = metadata.mime_type {
            request = request.header("Content-Type", mime_type);
        }

        Ok(request)
    }

    fn get_metadata_form_part( &self ) -> crate::Result<multipart::Part> {
        let metadata_string = serde_json::to_string(&self.metadata)?;

        let mut metadata_headers = header::HeaderMap::new();

        metadata_headers.insert(
            header::CONTENT_TYPE, "application/json; charset=UTF-8".parse()? 
        );

        metadata_headers.insert(
            header::CONTENT_DISPOSITION, "form-data; name=\"metadata\"".parse()?
        );

        Ok( multipart::Part::text(metadata_string)
            .headers(metadata_headers) )
    }

    fn get_file_form_part( &self ) -> crate::Result<multipart::Part> {
        let metadata = self.metadata.clone().unwrap_or_default();
        let content_mime_type = metadata.mime_type.unwrap_or( "*/*".into() );

        let mut file_headers = reqwest::header::HeaderMap::new();

        file_headers.insert(
            header::CONTENT_TYPE, content_mime_type.parse()?
        );

        file_headers.insert(
            header::CONTENT_DISPOSITION, "form-data; name=\"file\"".parse()?
        );

        let mut file_part = multipart::Part::text("");

        if let Some(source) = &self.content_source {
            file_part = multipart::Part::file(source)?;
        }

        if let Some(string) = &self.content_string {
            file_part = multipart::Part::text(string.clone());
        }

        Ok( file_part.headers(file_headers) )
    }

    /// Gets a multipart request.
    fn get_multipart_request( &self ) -> crate::Result<RequestBuilder> {
        let metadata_part = self.get_metadata_form_part()?;
        let file_part = self.get_file_form_part()?;

        let form = multipart::Form::new()
            .part("metadata", metadata_part)
            .part("file", file_part);

        Ok( self.build()?
            .multipart(form) )
    }

    /// Performs a resumable upload.
    fn perform_resumable_upload( &self ) -> crate::Result<objects::File> {
        let metadata = self.metadata.clone().unwrap_or_default();
        let metadata_string = serde_json::to_string(&metadata)?;
        let metadata_size = metadata_string.as_bytes().len();
        let content_mime_type = metadata.mime_type.unwrap_or( "*/*".into() );

        if self.content_string.is_some() {
            return Err( Error::new(
                ErrorKind::Request,
                "A resumable upload cannot be created from a string, it must be a file",
            ) )
        }

        let mut file = match &self.content_source {
            Some(source) => fs::File::open(source)?,
            None => {
                return Err( Error::new(
                    ErrorKind::Request,
                    "A resumable request must include a source file"
                ) )
            }
        };

        let file_size = file.metadata()?.len();

        let request = self.build()?
            .header( "X-Upload-Content-Type", &content_mime_type )
            .header( "X-Upload-Content-Length", &file_size.to_string() )
            .header( "Content-Type", "application/json; charset=UTF-8" )
            .header( "Content-Length", &metadata_size.to_string() )
            .body(metadata_string);

        let response = request.send()?;

        if !response.status().is_success() {
            return Err( response.into() );
        }

        let upload_uri = match response.headers().get("location") {
            Some(header) => header.to_str()?,
            #[cfg(not(tarpaulin_include))]
            None => {
                return Err( Error::new(
                    ErrorKind::Request,
                    "unable to get the resumable upload location",
                ) )
            }
        };

        let mut file_uploader = FileUploader::from_uri(upload_uri);

        if let Some(callback) = self.callback {
            file_uploader = file_uploader.with_callback(callback);
        }

        file_uploader.upload_file(&mut file)
    }

    /// Executes this request.
    ///
    /// # Errors
    ///
    /// - an [`IO`](crate::ErrorKind::IO) error, if the source file does not exist.
    /// - a [`UrlParsing`](crate::ErrorKind::UrlParsing) error, if the creation of the request URL failed.
    /// - a [`Json`](crate::ErrorKind::Json) error, if unable to parse the destination file to JSON.
    /// - a [`Request`](crate::ErrorKind::Request) error, if unable to send the request or get a body from the response.
    /// - a [`Response`](crate::ErrorKind::Response) error, if the request returned an error response.
    pub fn execute( &self ) -> crate::Result<objects::File> {
        let upload_type = self.upload_type.unwrap_or_default();

        let request = match upload_type {
            objects::UploadType::Media => self.get_media_request()?,
            objects::UploadType::Multipart => self.get_multipart_request()?,
            objects::UploadType::Resumable => {
                return self.perform_resumable_upload()
            },
        };

        let response = request.send()?;

        if !response.status().is_success() {
            return Err( response.into() );
        }

        Ok( serde_json::from_str( &response.text()? )? )
    }
}

#[request(
    method=Method::DELETE,
    url="https://www.googleapis.com/drive/v3/files/{file_id}",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
/// A request builder to permanently delete a file without moving it into
/// the trash.
pub struct DeleteRequest {
    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,
}

#[request(
    method=Method::DELETE,
    url="https://www.googleapis.com/drive/v3/files/trash",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
/// A request builder to permanently delete all of the user's trashed files.
pub struct EmptyTrashRequest {
    /// If set, empties the trash of the provided shared drive.
    drive_id: Option<String>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/export",
    returns=Vec::<u8>,
    returns_bytes=true,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to export a Google Workspace document.
pub struct ExportRequest {
    /// The MIME type of the format requested for this export.
    #[drive_v3(parameter)]
    mime_type: Option<String>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/generateIds",
    returns=objects::GeneratedIDs,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to generate IDs.
pub struct GenerateIDsRequest {
    /// The number of IDs to return.
    #[drive_v3(parameter)]
    count: Option<i64>,

    /// The space in which the IDs can be used to create new files.
    #[drive_v3(parameter)]
    space: Option<objects::Space>,

    /// The type of items which the IDs can be used for.
    #[drive_v3(parameter)]
    kind: Option<objects::IDKind>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}",
    returns=objects::File,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a file’s metadata by ID.
pub struct GetRequest {
    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}",
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a file’s content by ID.
pub struct GetMediaRequest {
    /// Whether the user is acknowledging the risk of downloading known
    /// malware or other abusive files.
    #[drive_v3(parameter)]
    acknowledge_abuse: Option<bool>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,

    /// Path to save the contents to.
    save_to: Option<PathBuf>,
}

impl GetMediaRequest {
    /// Executes this request.
    ///
    /// # Errors
    ///
    /// - a [`UrlParsing`](ErrorKind::UrlParsing) error, if the creation of the request URL failed.
    /// - a [`Json`](ErrorKind::Json) error, if unable to parse the destination file to JSON.
    /// - a [`Request`](ErrorKind::Request) error, if unable to send the request or get a body from the response.
    /// - a [`Response`](ErrorKind::Response) error, if the request returned an error response.
    pub fn execute( &self ) -> crate::Result< Vec<u8> > {
        let mut parameters = self.get_parameters();
        parameters.push(( "alt".into(), objects::Alt::Media.to_string() ));

        let url = Url::parse_with_params(&self.url, parameters)?;
        let request = Client::new()
            .request( self.method.clone(), url )
            .bearer_auth( self.credentials.get_access_token() );

        let response = request.send()?;

        if !response.status().is_success() {
            return Err( response.into() );
        }

        let file_bytes: Vec<u8> = response.bytes()?.into();

        if let Some(path) = &self.save_to {
            use std::io::Write;

            let mut file = fs::File::create(path)?;
            file.write_all(&file_bytes)?;
        }

        Ok(file_bytes)
    }
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files",
    returns=objects::FileList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list the user's files.
pub struct ListRequest {
    /// Bodies of items (files/documents) to which the query applies.
    ///
    /// Supported bodies are `user`, `domain`, `drive`, and `allDrives`.
    ///
    /// Prefer `user` or `drive` to `allDrives` for efficiency. By default,
    /// corpora is set to `user`. However, this can change depending on the
    /// filter set through the [`q`](ListRequest::q) parameter.
    #[drive_v3(parameter)]
    corpora: Option<String>,

    /// ID of the shared drive to search.
    #[drive_v3(parameter)]
    drive_id: Option<String>,

    /// Whether both My Drive and shared drive items should be included in
    /// results.
    #[drive_v3(parameter)]
    include_items_from_all_drives: Option<bool>,

    /// A comma-separated list of sort keys.
    ///
    /// Valid keys are `createdTime`, `folder`, `modifiedByMeTime`,
    /// `modifiedTime`, `name`, `name_natural`, `quotaBytesUsed`,
    /// `recency`, `sharedWithMeTime`, `starred`, and `viewedByMeTime`.
    ///
    /// Each key sorts ascending by default, but can be reversed by adding
    /// the `desc` to the end of the key.
    #[drive_v3(parameter)]
    order_by: Option<String>,

    /// The maximum number of files to return per page.
    ///
    /// Partial or empty result pages are possible even before the end of
    /// the files list has been reached.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`nest_page_token`](objects::FileList::next_page_token) from the
    /// previous response.
    #[drive_v3(parameter)]
    page_token: Option<String>,

    /// A query for filtering the file results.
    ///
    /// For more information, see Google's
    /// [Search for files & folders](https://developers.google.com/drive/api/guides/search-files)
    /// guide.
    #[drive_v3(parameter)]
    q: Option<String>,

    /// A comma-separated list of spaces to query within the corpora.
    ///
    /// Supported values are `drive` and `appDataFolder`.
    #[drive_v3(parameter)]
    spaces: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/listLabels",
    returns=objects::LabelList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list the labels in a file.
pub struct ListLabelsRequest {
    /// The maximum number of labels to return per page.
    ///
    /// When not set, defaults to 100.
    #[drive_v3(parameter)]
    max_results: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`next_page_token`](objects::LabelList::next_page_token) from the
    /// previous response.
    #[drive_v3(parameter)]
    page_token: Option<String>,
}

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/modifyLabels",
)]
#[derive(DriveRequestBuilder)]
/// A request builder to modify the labels in a file.
pub struct ModifyLabelsRequest {
    /// Sets the metadata that the updated file will have in Google Drive.
    #[drive_v3(body)]
    modifications: Option< Vec<objects::LabelModification> >,
}

#[cfg(not(tarpaulin_include))] // Requires a business account
impl ModifyLabelsRequest {
    /// Executes this request.
    ///
    /// # Errors
    ///
    /// - a [`UrlParsing`](ErrorKind::UrlParsing) error, if the creation of the
    /// request URL failed.
    /// - a [`Json`](ErrorKind::Json) error, if unable to parse the destination
    /// file to JSON.
    /// - a [`Request`](ErrorKind::Request) error, if unable to send the request
    /// or get a body from the response.
    /// - a [`Response`](ErrorKind::Response) error, if the request returned an
    /// error response.
    pub fn execute( &self ) -> crate::Result< Vec<objects::Label> > {
        let mut modify_request = objects::ModifyLabelsRequest::new();

        if let Some(modifications) = &self.modifications {
            modify_request.label_modifications = Some( modifications.to_vec() );
        }

        let request = self.build()?
            .body( serde_json::to_string(&modify_request)? );

        let response = request.send()?;

        if !response.status().is_success() {
            return Err( response.into() )
        }

        let response_text = response.text()?;
        let parsed_json = serde_json::from_str::<HashMap<&str, serde_json::Value>> (&response_text)?;

        match parsed_json.get("modifiedLabels") {
            Some(labels) => Ok( serde_json::from_value(labels.clone())? ),
            None => Err( Error::new(
                ErrorKind::Json,
                "the response did not contain the modified labels",
            ) )
        }
    }
}

#[request(
    method=Method::PATCH,
    url="https://www.googleapis.com/upload/drive/v3/files/{file_id}",
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list the labels in a file.
pub struct UpdateRequest {
    /// The type of upload request to the /upload URI.
    #[drive_v3(parameter)]
    upload_type: Option<objects::UploadType>,

    /// A comma-separated list of parent IDs to add.
    #[drive_v3(parameter)]
    add_parents: Option<String>,

    /// Whether to set the `keepForever` field in the new head revision.
    ///
    /// This is only applicable to files with binary content in a Drive.
    ///
    /// Only 200 revisions for the file can be kept forever, if the limit is
    /// reached, try deleting pinned revisions.
    #[drive_v3(parameter)]
    keep_revision_forever: Option<bool>,

    /// A language hint for OCR processing during image import
    /// (ISO 639-1 code).
    #[drive_v3(parameter)]
    ocr_language: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Whether to use the uploaded content as indexable text.
    #[drive_v3(parameter)]
    use_content_as_indexable_text: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only 'published' is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,

    /// Sets the metadata that the updated file will have in Google Drive.
    #[drive_v3(body)]
    metadata: Option<objects::File>,

    /// The path of the source file to be uploaded.
    content_source: Option<PathBuf>,

    /// Use this string as the content of the created file.
    content_string: Option<String>,

    /// A callback to receive updates on a resumable upload.
    #[drive_v3(callback)]
    callback: Option<fn(usize, usize)>,
}

impl UpdateRequest {
    /// Gets a media request.
    fn get_media_request( &self ) -> crate::Result<RequestBuilder> {
        let mut content_bytes = Vec::new();

        if self.content_source.is_some() && self.content_string.is_some() {
            return Err( Error::new(
                ErrorKind::Request,
                "an update request can only use one of 'content_source' or 'content_string'"
            ) )
        }

        if let Some(source) = &self.content_source {
            let mut file = fs::File::open(source)?;
            file.read_to_end(&mut content_bytes)?;
        }

        if let Some(string) = &self.content_string {
            content_bytes = string.as_bytes().to_vec();
        }

        let mut request = self.build()?
            .header( "Content-Length", content_bytes.len().to_string() )
            .body(content_bytes);

        let metadata = self.metadata.clone().unwrap_or_default();
        if let Some(mime_type) = metadata.mime_type {
            request = request.header("Content-Type", mime_type);
        }

        Ok(request)
    }

    fn get_metadata_form_part( &self ) -> crate::Result<multipart::Part> {
        let metadata_string = serde_json::to_string(&self.metadata)?;

        let mut metadata_headers = header::HeaderMap::new();

        metadata_headers.insert(
            header::CONTENT_TYPE, "application/json; charset=UTF-8".parse()? 
        );

        metadata_headers.insert(
            header::CONTENT_DISPOSITION, "form-data; name=\"metadata\"".parse()?
        );

        Ok( multipart::Part::text(metadata_string)
            .headers(metadata_headers) )
    }

    fn get_file_form_part( &self ) -> crate::Result<multipart::Part> {
        let metadata = self.metadata.clone().unwrap_or_default();
        let content_mime_type = metadata.mime_type.unwrap_or( "*/*".into() );

        let mut file_headers = reqwest::header::HeaderMap::new();

        file_headers.insert(
            header::CONTENT_TYPE, content_mime_type.parse()?
        );

        file_headers.insert(
            header::CONTENT_DISPOSITION, "form-data; name=\"file\"".parse()?
        );

        let mut file_part = multipart::Part::text("");

        if let Some(source) = &self.content_source {
            file_part = multipart::Part::file(source)?;
        }

        if let Some(string) = &self.content_string {
            file_part = multipart::Part::text(string.clone());
        }

        Ok( file_part.headers(file_headers) )
    }

    /// Gets a multipart request.
    fn get_multipart_request( &self ) -> crate::Result<RequestBuilder> {
        let metadata_part = self.get_metadata_form_part()?;
        let file_part = self.get_file_form_part()?;

        let form = multipart::Form::new()
            .part("metadata", metadata_part)
            .part("file", file_part);

        Ok( self.build()?
            .multipart(form) )
    }

    /// Performs a resumable upload.
    fn perform_resumable_upload( &self ) -> crate::Result<objects::File> {
        let metadata = self.metadata.clone().unwrap_or_default();
        let metadata_string = serde_json::to_string(&metadata)?;
        let metadata_size = metadata_string.as_bytes().len();
        let content_mime_type = metadata.mime_type.unwrap_or( "*/*".into() );

        if self.content_string.is_some() {
            return Err( Error::new(
                ErrorKind::Request,
                "A resumable upload cannot be created from a string, it must be a file",
            ) )
        }

        let mut file = match &self.content_source {
            Some(source) => fs::File::open(source)?,
            None => {
                return Err( Error::new(
                    ErrorKind::Request,
                    "A resumable request must include a source file",
                ) )
            }
        };

        let file_size = file.metadata()?.len();

        let request = self.build()?
            .header( "X-Upload-Content-Type", &content_mime_type )
            .header( "X-Upload-Content-Length", &file_size.to_string() )
            .header( "Content-Type", "application/json; charset=UTF-8" )
            .header( "Content-Length", &metadata_size.to_string() )
            .body(metadata_string);

        let response = request.send()?;

        if !response.status().is_success() {
            return Err( response.into() );
        }

        let upload_uri = match response.headers().get("location") {
            Some(header) => header.to_str()?,
            #[cfg(not(tarpaulin_include))]
            None => {
                return Err( Error::new(
                    ErrorKind::Request,
                    "unable to get the resumable upload location",
                ) )
            }
        };

        let mut file_uploader = FileUploader::from_uri(upload_uri);

        if let Some(callback) = self.callback {
            file_uploader = file_uploader.with_callback(callback);
        }

        file_uploader.upload_file(&mut file)
    }

    /// Executes this request.
    ///
    /// # Errors
    ///
    /// - an [`IO`](crate::ErrorKind::IO) error, if the source file does not exist.
    /// - a [`UrlParsing`](crate::ErrorKind::UrlParsing) error, if the creation of the request URL failed.
    /// - a [`Json`](crate::ErrorKind::Json) error, if unable to parse the destination file to JSON.
    /// - a [`Request`](crate::ErrorKind::Request) error, if unable to send the request or get a body from the response.
    /// - a [`Response`](crate::ErrorKind::Response) error, if the request returned an error response.
    pub fn execute( &self ) -> crate::Result<objects::File> {
        let upload_type = self.upload_type.unwrap_or_default();

        let request = match upload_type {
            objects::UploadType::Media => self.get_media_request()?,
            objects::UploadType::Multipart => self.get_multipart_request()?,
            objects::UploadType::Resumable => {
                return self.perform_resumable_upload()
            },
        };

        let response = request.send()?;

        if !response.status().is_success() {
            return Err( response.into() );
        }

        Ok( serde_json::from_str( &response.text()? )? )
    }
}

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/watch",
    returns=objects::Channel,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to modify the labels in a file.
pub struct WatchRequest {
    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Whether the user is acknowledging the risk of downloading known
    /// malware or other abusive files.
    #[drive_v3(parameter)]
    acknowledge_abuse: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,

    /// Sets the metadata that the channel will have.
    #[drive_v3(body)]
    channel: Option<objects::Channel>,
}

/// Information related to a user's files.
///
/// # Examples:
///
/// List the files in a drive
///
/// ```no_run
/// # use drive_v3::Error;
/// use drive_v3::{Credentials, Drive};
///
/// let credentials_path = "my_credentials.json";
/// let scopes = ["https://www.googleapis.com/auth/drive.metadata.readonly"];
///
/// let credentials = Credentials::from_file(&credentials_path, &scopes)?;
/// let drive = Drive::new(&credentials);
///
/// let file_list = drive.files.list()
///     .fields("files(name, id, mimeType)") // Set what fields will be returned
///     .q("name = 'file_im_looking_for' and not trashed") // search for specific files
///     .execute()?;
///
/// if let Some(files) = file_list.files {
///     for file in &files {
///         println!("{}", file);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Files {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Files {
    /// Creates a new [`Files`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self {
            credentials: credentials.clone(),
        }
    }

    /// Creates a copy of a file and applies any requested updates with patch
    /// semantics.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/files/copy)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::File;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// // Set the metadata that the copied file will have
    /// let metadata = File {
    ///     name: Some( "my-copy.txt".to_string() ),
    ///     description: Some( "I copied this using drive_v3!".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// // Set the ID of the file you want to copy,
    /// // you can get this using files.list()
    /// let source_file_id = "some-file-id";
    ///
    /// let copied_file = drive.files.copy(&source_file_id)
    ///     .metadata(&metadata)
    ///     .execute()?;
    ///
    /// assert_eq!(copied_file.name, metadata.name);
    /// assert_eq!(copied_file.description, metadata.description);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn copy<T: AsRef<str>> ( &self, file_id: T ) -> CopyRequest {
        CopyRequest::new(&self.credentials, &file_id)
    }

    /// Creates a new file.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/files/create)
    /// for more information.
    ///
    /// # Note:
    ///
    /// You can use any of the [`UploadTypes`](objects::UploadType), but for
    /// most uploads I recommend using the
    /// [`Resumable`](objects::UploadType::Resumable) type, which will allow you
    /// to set a callback to monitor the upload progress.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// Perform a simple upload to create a small media file (5 MB or less)
    /// without supplying metadata:
    ///
    /// ```no_run
    /// use drive_v3::objects::{File, UploadType};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// // A simple upload does not support metadata, however you can use it in
    /// // this request to set the MIME type of your new file, any other fields
    /// // you set will be ignored.
    /// let metadata = File {
    ///     mime_type: Some( "text/plain".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let my_new_file = drive.files.create()
    ///     .upload_type(UploadType::Media)
    ///     .metadata(&metadata)
    ///     .content_string("This is the content of my new file!")
    ///     // .content_source("path/to/file.txt") // You can also load a file from the system
    ///     .execute()?;
    ///
    /// assert_eq!(my_new_file.mime_type, metadata.mime_type);
    /// # Ok::<(), Error>(())
    /// ```
    ///
    /// Perform a multipart upload to create a small media file (5 MB or less)
    /// along with metadata that describes the file, in a single request:
    ///
    /// ```no_run
    /// use drive_v3::objects::{File, UploadType};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// // Set what information the uploaded file wil have
    /// let metadata = File {
    ///     name: Some( "my-new-file.txt".to_string() ),
    ///     mime_type: Some( "text/plain".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let my_new_file = drive.files.create()
    ///     .upload_type(UploadType::Multipart)
    ///     .metadata(&metadata)
    ///     .content_source("path/to/file.txt")
    ///     // .content_string("This is the content of my new file!")  // You can use a string
    ///     .execute()?;
    ///
    /// assert_eq!(my_new_file.name, metadata.name);
    /// assert_eq!(my_new_file.mime_type, metadata.mime_type);
    /// # Ok::<(), Error>(())
    /// ```
    ///
    /// Perform a resumable upload to create a large file (greater than 5 MB):
    ///
    /// ```no_run
    /// use drive_v3::objects::{File, UploadType};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// // Set what information the uploaded file wil have
    /// let metadata = File {
    ///     name: Some( "my-new-file.txt".to_string() ),
    ///     mime_type: Some( "text/plain".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// // You can set a callback that will be called when a resumable upload
    /// // progresses
    /// fn progress_callback( total_bytes: usize, uploaded_bytes: usize ) {
    ///     println!("Uploaded {} bytes, out of a total of {}.", uploaded_bytes, total_bytes);
    /// }
    ///
    /// let my_new_file = drive.files.create()
    ///     .upload_type(UploadType::Resumable)
    ///     .callback(progress_callback)
    ///     .metadata(&metadata)
    ///     .content_source("path/to/file.txt")
    ///     .execute()?;
    ///
    /// assert_eq!(my_new_file.name, metadata.name);
    /// assert_eq!(my_new_file.mime_type, metadata.mime_type);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn create( &self ) -> CreateRequest {
        CreateRequest::new(&self.credentials)
    }

    /// Permanently deletes a file owned by the user without moving it to the
    /// trash.
    ///
    /// If the file belongs to a shared drive, the user must be an organizer on
    /// the parent folder. If the target is a folder, all descendants owned by
    /// the user are also deleted.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let my_file_id = "example-id";
    ///
    /// drive.files.delete(&my_file_id).execute()?;
    ///
    /// # Ok::<(), Error>(())
    /// ```
    pub fn delete<T: AsRef<str>> ( &self, file_id: T ) -> DeleteRequest {
        DeleteRequest::new(&self.credentials, &file_id)
    }

    /// Permanently deletes all of the user's trashed files.
    ///
    /// # Note:
    ///
    /// The emptying of the trash may take some time to be reflected in a user's
    /// Drive.
    ///
    /// # Requires the following OAuth scope:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive"],
    /// # )? );
    /// #
    /// drive.files.empty_trash()
    ///     // .drive_id("my-drive-id") // You can specify which drive to empty the trash from
    ///     .execute()?;
    ///
    /// # Ok::<(), Error>(())
    /// ```
    #[cfg(not(tarpaulin_include))] // Requires higher permissions
    pub fn empty_trash( &self ) -> EmptyTrashRequest {
        EmptyTrashRequest::new(&self.credentials)
    }

    /// Exports a Google Workspace document to the requested MIME type and
    /// returns exported byte content (limited to 10MB).
    ///
    /// For more information on the supported export MIME types, check Google's
    /// [documentation](https://developers.google.com/drive/api/guides/ref-export-formats).
    ///
    /// # Note:
    ///
    /// This request requires you to set the
    /// [`mime_type`](ExportRequest::mime_type) of the file to export.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use std::fs;
    /// use std::io::Write;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let my_file_id = "file-id";
    /// let my_exported_mime_type = "application/pdf";
    ///
    /// let exported_bytes = drive.files.export(&my_file_id)
    ///     .mime_type(my_exported_mime_type)
    ///     .execute()?;
    ///
    /// // Write the bytes to a file
    /// let mut file = fs::File::create("exported-file.pdf")?;
    /// file.write_all(&exported_bytes)?;
    ///
    /// # Ok::<(), Error>(())
    /// ```
    #[cfg(not(tarpaulin_include))] // Requires higher permissions
    pub fn export<T: AsRef<str>> ( &self, file_id: T ) -> ExportRequest {
        ExportRequest::new(&self.credentials, &file_id)
    }

    /// Generates a set of file IDs which can be provided in create or copy
    /// requests.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::{Space, IDKind};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let my_generated_ids = drive.files.generate_ids()
    ///     .count(5) // How many IDs to generate
    ///     .space(Space::Drive) // Set the space in which the IDs can be used
    ///     .kind(IDKind::Files) // Set the type of the IDs
    ///     .execute()?;
    ///
    /// for id in &my_generated_ids.ids {
    ///     println!("Generated this ID: {}", id);
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn generate_ids( &self ) -> GenerateIDsRequest {
        GenerateIDsRequest::new(&self.credentials)
    }

    /// Gets a file's metadata by ID.
    ///
    /// # Note:
    ///
    /// To get the content of a file you can use [`get_media`](Files::get_media)
    /// (only works if the file is stored in Drive).
    ///
    /// To download Google Docs, Sheets, and Slides use
    /// [`export`](Files::export) instead.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let my_file_id = "file-id";
    ///
    /// let file_metadata = drive.files.get(&my_file_id).execute()?;
    ///
    /// println!("Look at this metadata:\n{}", file_metadata);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get<T: AsRef<str>> ( &self, file_id: T ) -> GetRequest {
        GetRequest::new(&self.credentials, &file_id)
    }

    /// Gets a file's content by ID.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let my_text_file_id = "file-id";
    ///
    /// let file_bytes = drive.files.get_media(&my_text_file_id)
    ///     // .save_to("my_downloaded_file.txt") // Save the contents to a path
    ///     .execute()?;
    ///
    /// let content = String::from_utf8_lossy(&file_bytes);
    ///
    /// println!("content: {}", content);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get_media<T: AsRef<str>> ( &self, file_id: T ) -> GetMediaRequest {
        GetMediaRequest::new(&self.credentials, &file_id)
    }

    /// Lists the user's files.
    ///
    /// This method accepts the [`q`](ListRequest::q) parameter, which is a
    /// search query combining one or more search terms.
    ///
    /// For more information, see Google's
    /// [Search for files & folders](https://developers.google.com/drive/api/guides/search-files)
    /// guide.
    ///
    /// # Note
    ///
    /// This method returns all files by default, including trashed files. If
    /// you don't want trashed files to appear in the list, use the
    /// `trashed=false` or `not trashed` in the [`q`](ListRequest::q) parameter
    /// to remove trashed files from the results.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_list = drive.files.list()
    ///     .fields("files(name, id, mimeType)") // Set what fields will be returned
    ///     .q("name = 'file_im_looking_for' and not trashed") // search for specific files
    ///     .execute()?;
    ///
    /// if let Some(files) = file_list.files {
    ///     for file in &files {
    ///         println!("{}", file);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list( &self ) -> ListRequest {
        ListRequest::new(&self.credentials)
    }

    /// Lists the labels on a file.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let my_file_id = "file-id";
    ///
    /// let label_list = drive.files.list_labels(&my_file_id)
    ///     .max_results(10)
    ///     .execute()?;
    ///
    /// if let Some(labels) = label_list.labels {
    ///     for label in &labels {
    ///         println!("{}", label);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list_labels<T: AsRef<str>> ( &self, file_id: T ) -> ListLabelsRequest {
        ListLabelsRequest::new(&self.credentials, &file_id)
    }

    /// Modifies the set of labels applied to a file.
    ///
    /// Returns a list of the labels that were added or modified.
    ///
    /// # Note
    ///
    /// As of now, labels on files are not supported on personal (free) Google
    /// Drive accounts.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::{LabelModification, FieldModification};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let label_modifications = vec![
    ///     LabelModification::from(
    ///         "label-id",
    ///         &vec![
    ///             FieldModification {
    ///                 set_text_values: Some( vec!["text".into(), "other_text".into()] ),
    ///                 ..Default::default()
    ///             }
    ///         ]
    ///     )
    /// ];
    ///
    /// let my_file_id = "file-id";
    ///
    /// let modified_labels = drive.files.modify_labels(&my_file_id)
    ///     .modifications(label_modifications)
    ///     .execute()?;
    ///
    /// for label in &modified_labels {
    ///     println!("this label was modified:\n{}", label);
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    #[cfg(not(tarpaulin_include))] // Requires a business account
    pub fn modify_labels<T: AsRef<str>> ( &self, file_id: T ) -> ModifyLabelsRequest {
        ModifyLabelsRequest::new(&self.credentials, &file_id)
    }

    /// Updates a file's metadata and/or content.
    ///
    /// When calling this method, only populate fields in the request that you
    /// want to modify. When updating fields, some fields might be changed
    /// automatically, such as [`modified_time`](objects::File::modified_time).
    ///
    /// This method supports patch semantics.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// Perform a simple upload to create a small media file (5 MB or less)
    /// without supplying metadata:
    ///
    /// ```no_run
    /// use drive_v3::objects::{File, UploadType};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// // A simple upload does not support metadata, however you can use it in
    /// // this request to set the MIME type of your new file, any other fields
    /// // you set will be ignored.
    /// let metadata = File {
    ///     mime_type: Some( "text/plain".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let my_file_id = "file-id";
    ///
    /// let my_new_file = drive.files.update(&my_file_id)
    ///     .upload_type(UploadType::Media)
    ///     .metadata(&metadata)
    ///     .content_string("This is the content of my new file!")
    ///     // .content_source("path/to/file.txt") // You can also load a file from the system
    ///     .execute()?;
    ///
    /// # Ok::<(), Error>(())
    /// ```
    ///
    /// Perform a multipart upload to create a small media file (5 MB or less)
    /// along with metadata that describes the file, in a single request:
    ///
    /// ```no_run
    /// use drive_v3::objects::{File, UploadType};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// // Set what information the uploaded file wil have
    /// let metadata = File {
    ///     name: Some( "my-new-file.txt".to_string() ),
    ///     mime_type: Some( "text/plain".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let my_file_id = "file-id";
    ///
    /// let my_new_file = drive.files.update(&my_file_id)
    ///     .upload_type(UploadType::Multipart)
    ///     .metadata(&metadata)
    ///     .content_source("path/to/file.txt")
    ///     // .content_string("This is the content of my new file!")  // You can use a string
    ///     .execute()?;
    ///
    /// # Ok::<(), Error>(())
    /// ```
    ///
    /// Perform a resumable upload to create a large file (greater than 5 MB):
    ///
    /// ```no_run
    /// use drive_v3::objects::{File, UploadType};
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// // Set what information the uploaded file wil have
    /// let metadata = File {
    ///     name: Some( "my-new-file.txt".to_string() ),
    ///     mime_type: Some( "text/plain".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// // You can set a callback that will be called when a resumable upload
    /// // progress, that way you can monitor and display how far along your
    /// // file upload is
    /// fn progress_callback( total_bytes: usize, uploaded_bytes: usize ) {
    ///     println!("Uploaded {} bytes, out of a total of {}.", uploaded_bytes, total_bytes);
    /// }
    ///
    /// let my_file_id = "file-id";
    ///
    /// let my_new_file = drive.files.update(&my_file_id)
    ///     .upload_type(UploadType::Resumable)
    ///     .callback(progress_callback)
    ///     .metadata(&metadata)
    ///     .content_source("path/to/file.txt")
    ///     .execute()?;
    ///
    /// # Ok::<(), Error>(())
    /// ```
    pub fn update<T: AsRef<str>> ( &self, file_id: T ) -> UpdateRequest {
        UpdateRequest::new(&self.credentials, &file_id)
    }

    /// Subscribes to changes to a file.
    ///
    /// # Note
    ///
    /// In order to subscribe to a file's changes, you must provide a
    /// [`Channel`](objects::Channel) with an `id` and an `address` which is the
    /// one that will receive the notifications. This can be done by creating a
    /// channel using [`from`](objects::Channel::from).
    ///
    /// For more information on channels, see Google's
    /// [documentation](https://developers.google.com/drive/api/guides/push).
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Channel;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let channel_id = "my-channel-id";
    /// let channel_address = "https://mydomain.com/channel-notifications";
    /// let channel = Channel::from(&channel_id, &channel_address);
    ///
    /// let my_file_id = "file-id";
    ///
    /// let created_channel = drive.files.watch(&my_file_id)
    ///     .channel(&channel)
    ///     .execute()?;
    ///
    /// println!("this is the created channel:\n{}", created_channel);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn watch<T: AsRef<str>> ( &self, file_id: T ) -> WatchRequest {
        WatchRequest::new(&self.credentials, &file_id)
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use super::Files;
    use std::io::Write;
    use std::path::PathBuf;
    use crate::{objects, ErrorKind};
    use crate::utils::test::{INVALID_CREDENTIALS, LOCAL_STORAGE_IN_USE, VALID_CREDENTIALS};

    fn get_resource() -> Files {
        Files::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Files {
        Files::new(&INVALID_CREDENTIALS)
    }

    fn delete_file( file: &objects::File ) -> crate::Result<()> {
        get_resource().delete( file.clone().id.unwrap() ).execute()
    }

    fn get_test_metadata() -> objects::File {
        objects::File {
            name: Some( "test.txt".to_string() ),
            description: Some( "a test file".to_string() ),
            mime_type: Some( "text/plain".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_file() -> (fs::File, PathBuf) {
        let path = PathBuf::from("test-file.txt");

        let mut test_file = fs::File::create(&path).unwrap();
        test_file.write_all( "content".as_bytes() ).unwrap();

        (test_file, path)
    }

    fn get_test_drive_file() -> crate::Result<objects::File> {
        let metadata = get_test_metadata();

        get_resource().create()
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn copy_test() {
        let metadata = get_test_metadata();
        let test_drive_file = get_test_drive_file().unwrap();

        let response = get_resource().copy( &test_drive_file.clone().id.unwrap() )
            .fields("*")
            .metadata(&metadata)
            .execute().unwrap();

        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        delete_file(&response).expect("Failed to cleanup a created file");
    }

    #[test]
    fn copy_invalid_response_test() {
        let response = get_resource().copy("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn create_media_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let metadata = get_test_metadata();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().create()
            .upload_type(objects::UploadType::Media)
            .metadata(&metadata)
            .content_source(&test_file_path)
            .execute()
            .unwrap();

        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&response).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn create_multipart_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let metadata = get_test_metadata();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().create()
            .fields("*")
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()
            .unwrap();

        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&response).expect("Failed to cleanup a created file");

        let response = get_resource().create()
            .fields("*")
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_source(&test_file_path)
            .execute()
            .unwrap();

        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&response).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn create_resumable_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        fn callback( _: usize, _: usize ) {}

        let metadata = get_test_metadata();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().create()
            .fields("*")
            .upload_type(objects::UploadType::Resumable)
            .metadata(&metadata)
            .content_source(&test_file_path)
            .callback(callback)
            .execute()
            .unwrap();

        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&response).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn create_resumable_invalid_response_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let (_, test_file_path) = get_test_file();

        let response = get_invalid_resource().create()
            .upload_type(objects::UploadType::Resumable)
            .content_source(&test_file_path)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );

        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn create_multiple_sources_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let (_, test_file_path) = get_test_file();

        let response = get_resource().create()
            .upload_type(objects::UploadType::Media)
            .content_string("content")
            .content_source(&test_file_path)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Request );

        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn create_invalid_resumable_test() {
        let response = get_resource().create()
            .upload_type(objects::UploadType::Resumable)
            .content_string("content")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Request );
    }

    #[test]
    fn create_no_source_resumable_test() {
        let response = get_resource().create()
            .upload_type(objects::UploadType::Resumable)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Request );
    }

    #[test]
    fn create_invalid_response_test() {
        let response = get_invalid_resource().create()
            .content_string("content")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn generate_ids_test() {
        let response = get_resource().generate_ids()
            .count(8)
            .space(objects::Space::Drive)
            .kind(objects::IDKind::Files)
            .execute().unwrap();

        assert_eq!( response.ids.len(), 8 );
        assert_eq!( response.space, objects::Space::Drive );
    }

    #[test]
    fn get_test() {
        let metadata = get_test_metadata();
        let test_drive_file = get_test_drive_file().unwrap();

        let response = get_resource().get( test_drive_file.clone().id.unwrap() )
            .fields("*")
            .execute().unwrap();

        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
    }

    #[test]
    fn get_media_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let test_drive_file = get_test_drive_file().unwrap();
        let save_path = PathBuf::from("saved.txt");

        let response = get_resource().get_media( test_drive_file.clone().id.unwrap() )
            .save_to(&save_path)
            .execute().unwrap();

        let content = String::from_utf8(response).unwrap();
        let saved_content = fs::read_to_string(&save_path).unwrap();

        assert_eq!(&content, "content");
        assert_eq!(&saved_content, "content");

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        fs::remove_file(&save_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn get_media_invalid_response_test() {
        let response = get_invalid_resource().get_media("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_test() {
        let response = get_resource().list()
            .execute();

        assert!( response.is_ok() );
    }

    #[test]
    fn list_labels_test() {
        let test_drive_file = get_test_drive_file().unwrap();

        let response = get_resource().list_labels( test_drive_file.clone().id.unwrap() )
            .execute();

        assert!( response.is_ok() );

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let metadata = get_test_metadata();
        let test_drive_file = get_test_drive_file().unwrap();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .upload_type(objects::UploadType::Media)
            .metadata(&metadata)
            .content_source(&test_file_path)
            .execute()
            .unwrap();

        assert_eq!(response.id, test_drive_file.id);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_string_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let metadata = get_test_metadata();
        let test_drive_file = get_test_drive_file().unwrap();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .upload_type(objects::UploadType::Media)
            .metadata(&metadata)
            .content_string("new content")
            .execute()
            .unwrap();

        assert_eq!(response.id, test_drive_file.id);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_multipart_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let metadata = get_test_metadata();
        let test_drive_file = get_test_drive_file().unwrap();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .fields("*")
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()
            .unwrap();

        assert_eq!(response.id, test_drive_file.id);
        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .fields("*")
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_source(&test_file_path)
            .execute()
            .unwrap();

        assert_eq!(response.id, test_drive_file.id);
        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_resumable_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        fn callback( _: usize, _: usize ) {}

        let metadata = get_test_metadata();
        let test_drive_file = get_test_drive_file().unwrap();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .fields("*")
            .upload_type(objects::UploadType::Resumable)
            .metadata(&metadata)
            .content_source(&test_file_path)
            .callback(callback)
            .execute()
            .unwrap();

        assert_eq!(response.id, test_drive_file.id);
        assert_eq!(response.name, metadata.name);
        assert_eq!(response.description, metadata.description);
        assert_eq!(response.mime_type, metadata.mime_type);

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_invalid_id_test() {
        let response = get_resource().update("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn update_resumable_invalid_response_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let (_, test_file_path) = get_test_file();

        let response = get_invalid_resource().update("invalid-id")
            .upload_type(objects::UploadType::Resumable)
            .content_source(&test_file_path)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );

        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_invalid_response_test() {
        let response = get_invalid_resource().update("invalid-id")
            .content_string("content")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn update_multiple_sources_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let test_drive_file = get_test_drive_file().unwrap();
        let (_, test_file_path) = get_test_file();

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .upload_type(objects::UploadType::Media)
            .content_string("content")
            .content_source(&test_file_path)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Request );

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        fs::remove_file(&test_file_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_invalid_resumable_test() {
        let test_drive_file = get_test_drive_file().unwrap();

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .upload_type(objects::UploadType::Resumable)
            .content_string("content")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Request );

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
    }

    #[test]
    fn update_no_source_resumable_test() {
        let test_drive_file = get_test_drive_file().unwrap();

        let response = get_resource().update( test_drive_file.clone().id.unwrap() )
            .upload_type(objects::UploadType::Resumable)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Request );

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
    }

    #[test]
    fn watch_test() {
        let test_drive_file = get_test_drive_file().unwrap();

        let channel_id = "channel_id".to_string();
        let channel_address = "https://wwwgoogle.com".to_string();

        let channel = objects::Channel::from(&channel_id, &channel_address);

        let response = get_resource().watch( test_drive_file.clone().id.unwrap() )
            .channel(&channel)
            .execute();

        assert!( response.is_ok() );
        assert_eq!( response.unwrap().id, Some(channel_id) );

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
    }

    #[test]
    fn watch_invalid_response_test() {
        let response = get_invalid_resource().watch("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
