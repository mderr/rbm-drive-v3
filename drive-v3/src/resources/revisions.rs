use std::{fs, path::PathBuf};
use reqwest::{blocking::Client, Method, Url};
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{objects, Credentials};

#[request(
    method=Method::DELETE,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/revisions/{revision_id}",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
/// A request builder to permanently delete a file version.
pub struct DeleteRequest {}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/revisions/{revision_id}",
    returns=objects::Revision,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a revision's metadata by ID.
pub struct GetRequest {}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/revisions/{revision_id}",
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a revision's content by ID.
pub struct GetMediaRequest {
    /// Whether the user is acknowledging the risk of downloading known
    /// malware or other abusive files.
    #[drive_v3(parameter)]
    acknowledge_abuse: Option<bool>,

    /// Path to save the contents to.
    save_to: Option<PathBuf>,
}

impl GetMediaRequest {
    /// Executes this request.
    ///
    /// # Errors:
    ///
    /// - a [`UrlParsing`](crate::ErrorKind::UrlParsing) error, if the creation
    /// of the request's URL failed.
    /// - a [`Request`](crate::ErrorKind::Request) error, if unable to send the
    /// request or get a body from the response.
    /// - a [`Response`](crate::ErrorKind::Response) error, if the request
    /// returned an error response.
    /// - a [`Json`](crate::ErrorKind::Json) error, if unable to parse the
    /// response's body.
    pub fn execute( &self ) -> crate::Result< Vec<u8> > {
        let mut parameters = self.get_parameters();
        parameters.push(( "alt".into(), objects::Alt::Media.to_string() ));

        let url = Url::parse_with_params(&self.url, parameters)?;
        let request = Client::new()
            .request( self.method.clone(), url )
            .bearer_auth( self.credentials.get_access_token() );

        let response = request.send()?;

        if !response.status().is_success() {
            return Err( response.into() );
        }

        let file_bytes: Vec<u8> = response.bytes()?.into();

        if let Some(path) = &self.save_to {
            use std::io::Write;

            let mut file = fs::File::create(path)?;
            file.write_all(&file_bytes)?;
        }

        Ok(file_bytes)
    }
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/revisions",
    returns=objects::RevisionList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list a file's revisions.
pub struct ListRequest {
    /// The maximum number of permissions to return per page.
    ///
    /// When not set for files in a shared drive, at most 100 results will
    /// be returned. When not set for files that are not in a shared drive,
    /// the entire list will be returned.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`next_page_token`](objects::RevisionList::next_page_token) from
    /// the previous response.
    #[drive_v3(parameter)]
    page_token: Option<String>,
}

#[request(
    method=Method::PATCH,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/revisions/{revision_id}",
    returns=objects::Revision,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to update a revision with patch semantics.
pub struct UpdateRequest {
    /// The updated revision.
    #[drive_v3(body)]
    revision: Option<objects::Revision>,
}

/// The metadata for a revision to a file.
///
/// Some resource methods (such as [`revisions.update`](Revisions::update))
/// require a `permission_id`. Use the [`revisions.list`](Revisions::list)
/// method to retrieve the ID for a file, folder, or shared drive.
///
/// # Examples:
///
/// List the revisions in a file
///
/// ```no_run
/// # use drive_v3::{Error, Credentials, Drive};
/// #
/// # let drive = Drive::new( &Credentials::from_file(
/// #     "../.secure-files/google_drive_credentials.json",
/// #     &["https://www.googleapis.com/auth/drive.file"],
/// # )? );
/// #
/// let file_id = "some-file-id";
///
/// let revision_list = drive.revisions.list(&file_id)
///     .execute()?;
///
/// if let Some(revisions) = revision_list.revisions {
///     for revision in revisions {
///         println!("{}", revision);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Revisions {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Revisions {
    /// Creates a new [`Revisions`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Permanently deletes a file version.
    ///
    /// You can only delete revisions for files with binary content in Google
    /// Drive, like images or videos. Revisions for other files, like Google
    /// Docs or Sheets, and the last remaining file version can't be deleted.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/revisions/delete)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let revision_id = "some-revision-id";
    ///
    /// let response = drive.revisions.delete(&file_id, &revision_id).execute();
    ///
    /// assert!( response.is_ok() );
    /// # Ok::<(), Error>(())
    /// ```
    pub fn delete<T, U> ( &self, file_id: T, revision_id: U ) -> DeleteRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
    {
        DeleteRequest::new(&self.credentials, file_id, revision_id)
    }

    /// Gets a revision's metadata by ID.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/revisions/get)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let revision_id = "some-revision-id";
    ///
    /// let revision = drive.revisions.get(&file_id, &revision_id)
    ///     .fields("*")
    ///     .execute()?;
    ///
    /// println!("This is the file's revision metadata:\n{}", revision);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get<T, U> ( &self, file_id: T, revision_id: U ) -> GetRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
    {
        GetRequest::new(&self.credentials, file_id, revision_id)
    }

    /// Gets a revision's content by ID.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/revisions/get)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let revision_id = "some-revision-id";
    ///
    /// let file_bytes = drive.revisions.get_media(&file_id, &revision_id)
    ///     // .save_to("my_downloaded_file.txt") // Save the contents to a path
    ///     .execute()?;
    ///
    /// let content = String::from_utf8_lossy(&file_bytes);
    ///
    /// println!("content: {}", content);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get_media<T, U> ( &self, file_id: T, revision_id: U ) -> GetMediaRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
    {
        GetMediaRequest::new(&self.credentials, file_id, revision_id)
    }

    /// Lists a file's revisions.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/revisions/list)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    ///
    /// let revision_list = drive.revisions.list(&file_id)
    ///     .execute()?;
    ///
    /// if let Some(revisions) = revision_list.revisions {
    ///     for revision in revisions {
    ///         println!("{}", revision);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list<T: AsRef<str>> ( &self, file_id: T ) -> ListRequest {
        ListRequest::new(&self.credentials, file_id)
    }

    /// Updates a reply with patch semantics.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/replies/update)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the [`fields`](UpdateRequest::fields)
    /// parameter.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Revision;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let updated_revision = Revision {
    ///     published: Some(false),
    ///     ..Default::default()
    /// };
    ///
    /// let file_id = "some-file-id";
    /// let revision_id = "some-revision-id";
    ///
    /// let revision = drive.revisions.update(&file_id, &revision_id)
    ///     .fields("*")
    ///     .revision(&updated_revision)
    ///     .execute()?;
    ///
    /// assert_eq!(revision.published, updated_revision.published);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn update<T, U> ( &self, file_id: T, revision_id: U ) -> UpdateRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
    {
        UpdateRequest::new(&self.credentials, file_id, revision_id)
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use super::Revisions;
    use std::path::PathBuf;
    use crate::{ErrorKind, objects, resources};
    use crate::utils::test::{INVALID_CREDENTIALS, LOCAL_STORAGE_IN_USE, VALID_CREDENTIALS};

    fn get_resource() -> Revisions {
        Revisions::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Revisions {
        Revisions::new(&INVALID_CREDENTIALS)
    }

    fn get_files_resource() -> resources::Files {
        resources::Files::new(&VALID_CREDENTIALS)
    }

    fn delete_file( file: &objects::File ) -> crate::Result<()> {
        get_files_resource().delete( file.clone().id.unwrap() ).execute()
    }

    fn get_test_file_metadata() -> objects::File {
        objects::File {
            name: Some( "test.txt".to_string() ),
            description: Some( "a test file".to_string() ),
            mime_type: Some( "text/plain".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_drive_file() -> crate::Result<objects::File> {
        let metadata = get_test_file_metadata();

        let file = get_files_resource().create()
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()?;


        get_files_resource().update( &file.clone().id.unwrap() )
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()
    }

    fn get_oldest_file_revision( file: &objects::File ) -> crate::Result<objects::Revision> {
        let revision_list = get_resource().list( &file.clone().id.unwrap() )
            .fields("*")
            .execute()?;

        Ok( revision_list.revisions.unwrap().last().unwrap().clone() )
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn delete_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let oldest_file_revision = get_oldest_file_revision(&test_drive_file).unwrap();

        let response = get_resource().delete(
            &test_drive_file.clone().id.unwrap(),
            &oldest_file_revision.clone().id.unwrap(),
            )
            .execute();

        assert!( response.is_ok() );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn delete_invalid_test() {
        let response = get_invalid_resource().delete("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn get_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let oldest_file_revision = get_oldest_file_revision(&test_drive_file).unwrap();

        let response = get_resource().get(
            &test_drive_file.clone().id.unwrap(),
            &oldest_file_revision.clone().id.unwrap(),
            )
            .fields("*")
            .execute();

        assert!( response.is_ok() );

        let revision = response.unwrap();
        assert_eq!(revision, oldest_file_revision);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn get_invalid_test() {
        let response = get_invalid_resource().get("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn get_media_test() {
        // Only run if no other tests are using the local storage
        let _unused = LOCAL_STORAGE_IN_USE.lock().unwrap();

        let test_drive_file = get_test_drive_file().unwrap();
        let oldest_file_revision = get_oldest_file_revision(&test_drive_file).unwrap();

        let save_path = PathBuf::from("saved.txt");

        let response = get_resource().get_media(
            &test_drive_file.clone().id.unwrap(),
            &oldest_file_revision.clone().id.unwrap(),
            )
            .save_to(&save_path)
            .execute();

        assert!( response.is_ok() );

        let content = String::from_utf8( response.unwrap() ).unwrap();
        let saved_content = fs::read_to_string(&save_path).unwrap();

        assert_eq!(&content, "content");
        assert_eq!(&saved_content, "content");

        delete_file(&test_drive_file).expect("Failed to cleanup a created file");
        fs::remove_file(&save_path).expect("Failed to cleanup a created file");
    }

    #[test]
    fn get_media_invalid_test() {
        let response = get_invalid_resource().get_media("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_test() {
        let test_drive_file = get_test_drive_file().unwrap();

        let response = get_resource().list( &test_drive_file.clone().id.unwrap() )
            .execute();

        assert!( response.is_ok() );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn list_invalid_test() {
        let response = get_invalid_resource().list("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn update_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let oldest_file_revision = get_oldest_file_revision(&test_drive_file).unwrap();

        let updated_revision = objects::Revision {
            published: Some(false),
            ..Default::default()
        };

        let response = get_resource().update(
                &test_drive_file.clone().id.unwrap(),
                &oldest_file_revision.clone().id.unwrap(),
            )
            .fields("*")
            .revision(&updated_revision)
            .execute();

        assert!( response.is_ok() );

        let revision = response.unwrap();
        assert_eq!(revision.published, updated_revision.published);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn update_invalid_test() {
        let response = get_invalid_resource().update("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
