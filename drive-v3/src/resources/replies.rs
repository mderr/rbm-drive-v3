use reqwest::Method;
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{objects, Credentials};

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}/replies",
    returns=objects::Reply,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to create a reply to a comment.
pub struct CreateRequest {
    /// The reply which will be created.
    #[drive_v3(body)]
    reply: Option<objects::Reply>,
}

#[request(
    method=Method::DELETE,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}/replies/{reply_id}",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
/// A request builder to delete a reply.
pub struct DeleteRequest {}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}/replies/{reply_id}",
    returns=objects::Reply,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a reply by ID.
pub struct GetRequest {
    /// Whether to return deleted replies.
    ///
    /// Deleted replies will not include their original content.
    #[drive_v3(parameter)]
    include_deleted: Option<bool>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}/replies",
    returns=objects::ReplyList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list a comments's replies.
pub struct ListRequest {
    /// Whether to return deleted replies.
    ///
    /// Deleted replies will not include their original content.
    #[drive_v3(parameter)]
    include_deleted: Option<bool>,

    /// The maximum number of permissions to return per page.
    ///
    /// When not set for files in a shared drive, at most 100 results will
    /// be returned. When not set for files that are not in a shared drive,
    /// the entire list will be returned.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`next_page_token`](objects::ReplyList::next_page_token) from
    /// the previous response.
    #[drive_v3(parameter)]
    page_token: Option<String>,
}

#[request(
    method=Method::PATCH,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}/replies/{reply_id}",
    returns=objects::Reply,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to update a permission for a file or shared drive.
pub struct UpdateRequest {
    /// Whether to remove the expiration date.
    #[drive_v3(parameter)]
    remove_expiration: Option<bool>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Whether to transfer ownership to the specified user and downgrade
    /// the current owner to a writer.
    ///
    /// This parameter is required as an acknowledgement of the side effect.
    #[drive_v3(parameter)]
    transfer_ownership: Option<bool>,

    /// Issue the request as a domain administrator.
    ///
    /// if set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,

    /// The updated reply.
    #[drive_v3(body)]
    reply: Option<objects::Reply>,
}

/// A reply to a comment on a file.
///
/// Some resource methods (such as [`replies.update`](Replies::update))
/// require a `permission_id`. Use the [`replies.list`](Replies::list)
/// method to retrieve the ID for a file, folder, or shared drive.
///
/// # Examples:
///
/// List the permission in a file
///
/// ```no_run
/// # use drive_v3::{Error, Credentials, Drive};
/// #
/// # let drive = Drive::new( &Credentials::from_file(
/// #     "../.secure-files/google_drive_credentials.json",
/// #     &["https://www.googleapis.com/auth/drive.file"],
/// # )? );
/// #
/// let file_id = "some-file-id";
/// let permission_list = drive.permissions.list(&file_id).execute()?;
///
/// if let Some(permissions) = permission_list.permissions {
///     for permission in permissions {
///         println!("{}", permission);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Replies {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Replies {
    /// Creates a new [`Replies`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Creates a reply to a comment.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/replies/create)
    /// for more information.
    ///
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Reply;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let reply = Reply {
    ///     content: Some( "my reply content".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    ///
    /// let created_reply = drive.replies.create(&file_id, &comment_id)
    ///     .reply(&reply)
    ///     .execute()?;
    ///
    /// assert_eq!(created_reply.content, reply.content);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn create<T, U> ( &self, file_id: T, comment_id: U ) -> CreateRequest
        where
            T: AsRef<str>,
            U: AsRef<str>
    {
        CreateRequest::new(&self.credentials, file_id, comment_id)
    }

    /// Deletes a reply.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/replies/delete)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    /// let reply_id = "some-reply-id";
    ///
    /// let response = drive.replies.delete(&file_id, &comment_id, &reply_id).execute();
    ///
    /// assert!( response.is_ok() );
    /// # Ok::<(), Error>(())
    /// ```
    pub fn delete<T, U, V> ( &self, file_id: T, comment_id: U, reply_id: V ) -> DeleteRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
            V: AsRef<str>,
    {
        DeleteRequest::new(&self.credentials, file_id, comment_id, reply_id)
    }

    /// Gets a reply by ID.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/replies/get)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the [`fields`](GetRequest::fields)
    /// parameter.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    /// let reply_id = "some-reply-id";
    ///
    /// let reply = drive.replies.get(&file_id, &comment_id, &reply_id)
    ///     .fields("*")
    ///     .execute()?;
    ///
    /// println!("This is the comment's reply:\n{}", reply);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get<T, U, V> ( &self, file_id: T, comment_id: U, reply_id: V ) -> GetRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
            V: AsRef<str>,
    {
        GetRequest::new(&self.credentials, file_id, comment_id, reply_id)
    }

    /// Lists a comment's replies.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/replies/list)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the [`fields`](ListRequest::fields)
    /// parameter.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    ///
    /// let reply_list = drive.replies.list(&file_id, &comment_id)
    ///     .fields("*")
    ///     .execute()?;
    ///
    /// if let Some(replies) = reply_list.replies {
    ///     for reply in replies {
    ///         println!("{}", reply);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list<T, U> ( &self, file_id: T, comment_id: U ) -> ListRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
    {
        ListRequest::new(&self.credentials, file_id, comment_id)
    }

    /// Updates a reply with patch semantics.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/replies/update)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the [`fields`](UpdateRequest::fields)
    /// parameter.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Reply;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let updated_reply = Reply {
    ///     content: Some( "updated content".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    /// let reply_id = "some-reply-id";
    ///
    /// let reply = drive.replies.update(&file_id, &comment_id, &reply_id)
    ///     .fields("*")
    ///     .reply(&updated_reply)
    ///     .execute()?;
    ///
    /// assert_eq!(reply.content, updated_reply.content);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn update<T, U, V> ( &self, file_id: T, comment_id: U, reply_id: V ) -> UpdateRequest
        where
            T: AsRef<str>,
            U: AsRef<str>,
            V: AsRef<str>,
    {
        UpdateRequest::new(&self.credentials, file_id, comment_id, reply_id)
    }
}

#[cfg(test)]
mod tests {
    use super::Replies;
    use crate::{ErrorKind, objects, resources};
    use crate::utils::test::{INVALID_CREDENTIALS, VALID_CREDENTIALS};

    fn get_resource() -> Replies {
        Replies::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Replies {
        Replies::new(&INVALID_CREDENTIALS)
    }

    fn get_files_resource() -> resources::Files {
        resources::Files::new(&VALID_CREDENTIALS)
    }

    fn get_comments_resource() -> resources::Comments {
        resources::Comments::new(&VALID_CREDENTIALS)
    }

    fn delete_file( file: &objects::File ) -> crate::Result<()> {
        get_files_resource().delete( file.clone().id.unwrap() ).execute()
    }

    fn get_test_file_metadata() -> objects::File {
        objects::File {
            name: Some( "test.txt".to_string() ),
            description: Some( "a test file".to_string() ),
            mime_type: Some( "text/plain".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_reply() -> objects::Reply {
        objects::Reply {
            content: Some( "test reply".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_comment() -> objects::Comment {
        objects::Comment {
            content: Some( "test comment".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_drive_file() -> crate::Result<objects::File> {
        let metadata = get_test_file_metadata();

        get_files_resource().create()
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()
    }

    fn get_test_drive_comment( file: &objects::File ) -> crate::Result<objects::Comment> {
        let comment = get_test_comment();

        get_comments_resource().create( &file.clone().id.unwrap() )
            .fields("*")
            .comment(&comment)
            .execute()
    }

    fn get_test_drive_reply( file: &objects::File, comment: &objects::Comment ) -> crate::Result<objects::Reply> {
        let test_reply = get_test_reply();

        get_resource().create(
                &file.clone().id.unwrap(),
                &comment.clone().id.unwrap(),
            )
            .fields("*")
            .reply(&test_reply)
            .execute()
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn create_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();
        let test_reply = get_test_reply();

        let response = get_resource().create(
                &test_drive_file.clone().id.unwrap(),
                &test_drive_comment.clone().id.unwrap(),
            )
            .fields("content")
            .reply(&test_reply)
            .execute();

        assert!( response.is_ok() );

        let reply = response.unwrap();
        assert_eq!(reply.content, test_reply.content);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn create_invalid_test() {
        let response = get_invalid_resource().create("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn delete_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();
        let test_drive_reply = get_test_drive_reply(&test_drive_file, &test_drive_comment).unwrap();

        let response = get_resource().delete(
            &test_drive_file.clone().id.unwrap(),
            &test_drive_comment.clone().id.unwrap(),
            &test_drive_reply.clone().id.unwrap(),
            )
            .execute();

        assert!( response.is_ok() );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn delete_invalid_test() {
        let response = get_invalid_resource()
            .delete("invalid-id", "invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn get_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();
        let test_drive_reply = get_test_drive_reply(&test_drive_file, &test_drive_comment).unwrap();

        let response = get_resource().get(
            &test_drive_file.clone().id.unwrap(),
            &test_drive_comment.clone().id.unwrap(),
            &test_drive_reply.clone().id.unwrap(),
            )
            .fields("*")
            .execute();

        assert!( response.is_ok() );

        let reply = response.unwrap();
        assert_eq!(reply, test_drive_reply);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn get_invalid_test() {
        let response = get_invalid_resource()
            .get("invalid-id", "invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();
        let test_drive_reply = get_test_drive_reply(&test_drive_file, &test_drive_comment).unwrap();

        let response = get_resource().list(
                &test_drive_file.clone().id.unwrap(),
                &test_drive_comment.clone().id.unwrap(),
            )
            .fields("*")
            .execute();

        assert!( response.is_ok() );

        let replies = response.unwrap().replies.unwrap();
        assert!( replies.contains(&test_drive_reply) );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn list_invalid_test() {
        let response = get_invalid_resource().list("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn update_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();
        let test_drive_reply = get_test_drive_reply(&test_drive_file, &test_drive_comment).unwrap();

        let updated_reply = objects::Reply {
            content: Some( "updated reply".to_string() ),
            ..Default::default()
        };

        let response = get_resource().update(
                &test_drive_file.clone().id.unwrap(),
                &test_drive_comment.clone().id.unwrap(),
                &test_drive_reply.clone().id.unwrap(),
            )
            .fields("*")
            .reply(&updated_reply)
            .execute();

        assert!( response.is_ok() );

        let reply = response.unwrap();
        assert_eq!(reply.content, updated_reply.content);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn update_invalid_test() {
        let response = get_invalid_resource()
            .update("invalid-id", "invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
