use reqwest::Method;
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{objects, Credentials};

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/apps/{app_id}",
    returns=objects::App
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a specific app.
pub struct GetRequest {}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/apps",
    returns=objects::AppList
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list a user's installed apps.
pub struct ListRequest {
    /// A comma-separated list of file extensions to limit returned results.
    ///
    /// All results within the given app query scope which can open any of
    /// the given file extensions are included in the response. If
    /// [`app_filter_mime_types`](ListRequest::app_filter_mime_types) are
    /// provided as well, the result is a union of the two resulting app
    /// lists.
    #[drive_v3(parameter)]
    app_filter_extensions: Option<String>,

    /// A comma-separated list of file extensions to limit returned results.
    ///
    /// All results within the given app query scope which can open any of
    /// the given MIME types will be included in the response. If
    /// [`app_filter_extensions`](ListRequest::app_filter_extensions) are
    /// provided as well, the result is a union of the two resulting app
    /// lists.
    #[drive_v3(parameter)]
    app_filter_mime_types: Option<String>,

    /// A language or locale code, as defined by BCP 47, with some
    /// extensions from Unicode's LDML format.
    #[drive_v3(parameter)]
    language_code: Option<String>,
}

/// Provides a list of apps that a user has installed.
///
/// Contains information about each app's supported MIME types, file,
/// extensions, and other details.
///
/// Some resource methods (such as [`apps.get`](Apps::get)) require an `app_id`.
/// Use the [`apps.list`](Apps::list) method to retrieve the ID for an installed
/// application.
///
/// # Examples:
///
/// List the apps for a user
///
/// ```no_run
/// # use drive_v3::{Error, Credentials, Drive};
/// #
/// # let drive = Drive::new( &Credentials::from_file(
/// #     "../.secure-files/google_drive_credentials.json",
/// #     &["https://www.googleapis.com/auth/drive.apps.readonly"],
/// # )? );
/// #
/// let app_list = drive.apps.list().execute()?;
///
/// if let Some(apps) = app_list.items {
///     for app in apps {
///         println!("{}", app);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Apps {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Apps {
    /// Creates a new [`Apps`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Gets a specific app.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/apps/get)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/docs`
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.apps.readonly`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let app_id = "some-app-id";
    ///
    /// let app = drive.apps.get(&app_id).execute()?;
    ///
    /// println!("This is the info of the app:\n{}", app);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get<T: AsRef<str>> ( &self, app_id: T ) -> GetRequest {
        GetRequest::new(&self.credentials, app_id)
    }

    /// Lists a user's installed apps.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/apps/list)
    /// for more information.
    ///
    /// # Requires the following OAuth scope:
    ///
    /// - `https://www.googleapis.com/auth/drive.apps.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.apps.readonly"],
    /// # )? );
    /// #
    /// let app_list = drive.apps.list().execute()?;
    ///
    /// if let Some(apps) = app_list.items {
    ///     for app in apps {
    ///         println!("{}", app);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list( &self ) -> ListRequest {
        ListRequest::new(&self.credentials)
    }
}

#[cfg(test)]
mod tests {
    use super::Apps;
    use crate::ErrorKind;
    use crate::utils::test::{INVALID_CREDENTIALS, VALID_CREDENTIALS};

    fn get_resource() -> Apps {
        Apps::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Apps {
        Apps::new(&INVALID_CREDENTIALS)
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn get_test() {
        let google_docs_id = "619683526622";
        let response = get_resource().get(&google_docs_id)
            .execute();

        assert!( response.is_ok() );

        let app = response.unwrap();

        assert_eq!( &app.id.unwrap(), google_docs_id );
        assert_eq!( &app.name.unwrap(), "Google Docs" );
        assert_eq!( &app.kind.unwrap(), "drive#app" );
    }

    #[test]
    fn get_invalid_test() {
        let google_docs_id = "619683526622";
        let response = get_invalid_resource().get(&google_docs_id)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_test() {
        let response = get_resource().list().execute();

        assert!( response.is_ok() );
        assert!( response.unwrap().items.is_some() );
    }

    #[test]
    fn list_invalid_test() {
        let response = get_invalid_resource().list()
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
