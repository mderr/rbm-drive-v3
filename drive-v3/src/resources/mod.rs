/// Items related to Google Drive's
/// [`about`](https://developers.google.com/drive/api/reference/rest/v3/about)
/// endpoint.
pub mod about;

/// Items related to Google Drive's
/// [`apps`](https://developers.google.com/drive/api/reference/rest/v3/apps)
/// endpoint.
pub mod apps;

/// Items related to Google Drive's
/// [`changes`](https://developers.google.com/drive/api/reference/rest/v3/changes)
/// endpoint.
#[cfg(not(tarpaulin_include))] // Requires paid Google Workspace features
pub mod changes;

/// Items related to Google Drive's
/// [`channels`](https://developers.google.com/drive/api/reference/rest/v3/channels)
/// endpoint.
pub mod channels;

/// Items related to Google Drive's
/// [`comments`](https://developers.google.com/drive/api/reference/rest/v3/comments)
/// endpoint.
pub mod comments;

/// Items related to Google Drive's
/// [`drives`](https://developers.google.com/drive/api/reference/rest/v3/drives)
/// endpoint.
#[cfg(not(tarpaulin_include))] // Requires paid Google Workspace features
pub mod drives;

/// Items related to Google Drive's
/// [`files`](https://developers.google.com/drive/api/reference/rest/v3/files)
/// endpoint.
pub mod files;

/// Items related to Google Drive's
/// [`permissions`](https://developers.google.com/drive/api/reference/rest/v3/permissions)
/// endpoint.
pub mod permissions;

/// Items related to Google Drive's
/// [`replies`](https://developers.google.com/drive/api/reference/rest/v3/replies)
/// endpoint.
pub mod replies;

/// Items related to Google Drive's
/// [`revisions`](https://developers.google.com/drive/api/reference/rest/v3/revisions)
/// endpoint.
pub mod revisions;

#[doc(inline)]
pub use about::About;

#[doc(inline)]
pub use apps::Apps;

#[doc(inline)]
pub use changes::Changes;

#[doc(inline)]
pub use channels::Channels;

#[doc(inline)]
pub use comments::Comments;

#[doc(inline)]
pub use drives::Drives;

#[doc(inline)]
pub use files::Files;

#[doc(inline)]
pub use permissions::Permissions;

#[doc(inline)]
pub use replies::Replies;

#[doc(inline)]
pub use revisions::Revisions;

#[doc(hidden)]
pub trait DriveRequestBuilder {
    fn get_parameters( &self ) -> Vec<(String, String)>;
    fn build( &self ) -> crate::Result<reqwest::blocking::RequestBuilder>;
    fn send( &self ) -> crate::Result<reqwest::blocking::Response>;
}
