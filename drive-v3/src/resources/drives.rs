use reqwest::Method;
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{objects, Credentials};

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/drives",
    returns=objects::DriveInfo,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to create a shared drive.
pub struct CreateRequest {
    /// An ID, such as a random UUID, which uniquely identifies this user's
    /// request for idempotent creation of a shared drive.
    ///
    /// A repeated request by the same user and with the same request ID
    /// will avoid creating duplicates by attempting to create the same
    /// shared drive. If the shared drive already exists a `409` error will
    /// be returned.
    #[drive_v3(parameter)]
    request_id: Option<String>,

    /// The Drive to be created
    #[drive_v3(body)]
    drive_info: Option<objects::DriveInfo>
}

#[request(
    method=Method::DELETE,
    url="https://www.googleapis.com/drive/v3/drives/{drive_id}",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
/// A request builder to delete a shared drive.
pub struct DeleteRequest {
    /// Issue the request as a domain administrator.
    ///
    /// If set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,

    /// Whether any items inside the shared drive should also be deleted.
    ///
    /// This option is only supported when
    /// [`use_domain_admin_access`](DeleteRequest::use_domain_admin_access)
    /// is also set to `true`.
    #[drive_v3(parameter)]
    allow_item_deletion: Option<bool>,
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/drives/{drive_id}",
    returns=objects::DriveInfo,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a shared drive.
pub struct GetRequest {
    /// Issue the request as a domain administrator.
    ///
    /// If set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,
}

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/drives/{drive_id}/hide",
    returns=objects::DriveInfo,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to hide a shared drive from the default view.
pub struct HideRequest {}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/drives",
    returns=objects::DriveInfoList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list the user's shared drives.
pub struct ListRequest {
    /// Maximum number of shared drives to return per page.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// Page token for shared drives.
    #[drive_v3(parameter)]
    page_token: Option<String>,

    /// Query string for searching shared drives.
    #[drive_v3(parameter)]
    q: Option<String>,

    /// Issue the request as a domain administrator.
    ///
    /// If set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,
}

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/drives/{drive_id}/unhide",
    returns=objects::DriveInfo,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to restore a shared drive to the default view.
pub struct UnhideRequest {}

#[request(
    method=Method::PATCH,
    url="https://www.googleapis.com/drive/v3/drives/{drive_id}",
    returns=objects::DriveInfo,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to updates the metadata for a shared drive.
pub struct UpdateRequest {
    /// Issue the request as a domain administrator.
    ///
    /// if set to `true`, then the requester will be granted access if they
    /// are an administrator of the domain to which the shared drive
    /// belongs.
    #[drive_v3(parameter)]
    use_domain_admin_access: Option<bool>,

    /// The Drive to be created
    #[drive_v3(body)]
    drive_info: Option<objects::DriveInfo>,
}

/// Information of a shared drive.
///
/// Some resource methods (such as [`drives.update`](Drives::update)) require a
/// `drive_id`. Use the [`drives.list`](Drives::list) method to retrieve the ID
/// for a shared drive.
///
/// # Examples
///
/// List the shared Drives of a user
///
/// ```no_run
/// # use drive_v3::{Error, Credentials, Drive};
/// #
/// # let drive = Drive::new( &Credentials::from_file(
/// #     "../.secure-files/google_drive_credentials.json",
/// #     &["https://www.googleapis.com/auth/drive.file"],
/// # )? );
/// #
/// let drive_list = drive.drives.list()
///     .page_size(10)
///     .q("name = 'drive_im_looking_for'") // search for specific drives
///     .execute()?;
///
/// if let Some(drives) = drive_list.drives {
///     for drive in drives {
///         println!("{}", drive);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Drives {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Drives {
    /// Creates a new [`Drives`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Creates a shared drive.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/drives/create)
    /// for more information.
    ///
    /// # Requires the following OAuth scope:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::DriveInfo;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let drive_info = DriveInfo {
    ///     name: Some( "drive-name".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let created_drive = drive.drives.create()
    ///     .drive_info(&drive_info)
    ///     .execute()?;
    ///
    /// assert_eq!(created_drive.name, drive_info.name);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn create( &self ) -> CreateRequest {
        CreateRequest::new(&self.credentials)
    }

    /// Permanently deletes a shared drive for which the user is an organizer.
    ///
    /// The shared drive cannot contain any untrashed items.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/drives/delete)
    /// for more information.
    ///
    /// # Requires the following OAuth scope:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let drive_id = "some-drive-id";
    /// let response = drive.drives.delete(&drive_id).execute();
    ///
    /// assert!( response.is_ok() );
    /// # Ok::<(), Error>(())
    /// ```
    pub fn delete<T: AsRef<str>> ( &self, drive_id: T ) -> DeleteRequest {
        DeleteRequest::new(&self.credentials, drive_id)
    }

    /// Gets a shared drive's metadata by ID.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/drives/get)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let drive_id = "some-drive-id";
    /// let drive_info = drive.drives.get(&drive_id).execute()?;
    ///
    /// println!("This is the Drive's information:\n{}", drive_info);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get<T: AsRef<str>> ( &self, drive_id: T ) -> GetRequest {
        GetRequest::new(&self.credentials, drive_id)
    }

    /// Hides a shared drive from the default view.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/drives/hide)
    /// for more information.
    ///
    /// # Requires the following OAuth scope:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let drive_id = "some-drive-id";
    /// let drive_info = drive.drives.hide(&drive_id).execute()?;
    ///
    /// assert!( drive_info.hidden.unwrap() );
    /// # Ok::<(), Error>(())
    /// ```
    pub fn hide<T: AsRef<str>> ( &self, drive_id: T ) -> HideRequest {
        HideRequest::new(&self.credentials, drive_id)
    }

    /// Lists the user's shared drives.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/drives/list)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This method accepts the [`q`](ListRequest::q) parameter, which is a
    /// search query combining one or more search terms. For more information,
    /// see Google's
    /// [Search for shared drives](https://developers.google.com/drive/api/guides/search-shareddrives)
    /// guide.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let drive_list = drive.drives.list()
    ///     .page_size(10)
    ///     .q("name = 'drive_im_looking_for'") // search for specific drives
    ///     .execute()?;
    ///
    /// if let Some(drives) = drive_list.drives {
    ///     for drive in drives {
    ///         println!("{}", drive);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list( &self ) -> ListRequest {
        ListRequest::new(&self.credentials)
    }

    /// Restores a shared drive to the default view.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/drives/unhide)
    /// for more information.
    ///
    /// # Requires the following OAuth scope:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let drive_id = "some-drive-id";
    /// let drive_info = drive.drives.unhide(&drive_id).execute()?;
    ///
    /// assert!( !drive_info.hidden.unwrap() );
    /// # Ok::<(), Error>(())
    /// ```
    pub fn unhide<T: AsRef<str>> ( &self, drive_id: T ) -> UnhideRequest {
        UnhideRequest::new(&self.credentials, drive_id)
    }

    /// Updates the metadata for a shared drive.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/drives/update)
    /// for more information.
    ///
    /// # Requires the following OAuth scope:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::DriveInfo;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let updated_drive_info = DriveInfo {
    ///     name: Some( "updated-drive-name".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let drive_id = "some-drive-id";
    ///
    /// let drive_info = drive.drives.update(&drive_id)
    ///     .drive_info(&updated_drive_info)
    ///     .execute()?;
    ///
    /// assert_eq!(drive_info.name, updated_drive_info.name);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn update<T: AsRef<str>> ( &self, drive_id: T ) -> UpdateRequest {
        UpdateRequest::new(&self.credentials, drive_id)
    }
}

#[cfg(test)]
mod tests {
    use super::Drives;
    use crate::ErrorKind;
    use crate::utils::test::{INVALID_CREDENTIALS, VALID_CREDENTIALS};

    fn get_resource() -> Drives {
        Drives::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Drives {
        Drives::new(&INVALID_CREDENTIALS)
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn create_invalid_test() {
        let response = get_invalid_resource().create()
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn delete_invalid_test() {
        let response = get_invalid_resource().delete("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn get_invalid_test() {
        let response = get_invalid_resource().get("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn hide_invalid_test() {
        let response = get_invalid_resource().hide("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_test() {
        let response = get_resource().list()
            .execute();

        assert!( response.is_ok() );
    }

    #[test]
    fn list_invalid_test() {
        let response = get_invalid_resource().list()
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn unhide_invalid_test() {
        let response = get_invalid_resource().unhide("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn update_invalid_test() {
        let response = get_invalid_resource().update("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
