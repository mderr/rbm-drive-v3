use reqwest::Method;
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{Credentials, objects};

/// A request builder to get information about the user, the user's Drive,
/// and system capabilities.
#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/about",
    returns=objects::About
)]
#[derive(DriveRequestBuilder)]
pub struct GetRequest {}

/// Information about the user, the user's Drive, and system capabilities.
///
/// # Examples:
///
/// ```no_run
/// use drive_v3::{Credentials, Drive};
/// # use drive_v3::Error;
///
/// let credentials_path = "my_credentials.json";
/// let scopes = ["https://www.googleapis.com/auth/drive.metadata.readonly"];
///
/// let credentials = Credentials::from_file(&credentials_path, &scopes)?;
/// let drive = Drive::new(&credentials);
///
/// let my_information = drive.about.get()
///     .fields("storageQuota, canCreateDrives, appInstalled")
///     .execute()?;
///
/// println!("look at all this information:\n{}", my_information);
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct About {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl About {
    /// Creates a new [`About`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Gets information about the user, the user's Drive, and system capabilities.
    ///
    /// See Google's [documentation](https://developers.google.com/drive/api/reference/rest/v3/about/get) for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the [`fields`](GetRequest::fields) parameter.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::{Credentials, Drive};
    /// # use drive_v3::Error;
    ///
    /// let credentials_path = "my_credentials.json";
    /// let scopes = ["https://www.googleapis.com/auth/drive.metadata.readonly"];
    ///
    /// let credentials = Credentials::from_file(&credentials_path, &scopes)?;
    /// let drive = Drive::new(&credentials);
    ///
    /// let my_information = drive.about.get()
    ///     .fields("storageQuota, canCreateDrives, appInstalled")
    ///     .execute()?;
    ///
    /// println!("look at all this information:\n{}", my_information);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get( &self ) -> GetRequest {
        GetRequest::new(&self.credentials)
    }
}

#[cfg(test)]
mod tests {
    use reqwest::Method;

    use super::About;
    use crate::ErrorKind;
    use crate::utils::test::{VALID_CREDENTIALS, INVALID_CREDENTIALS};

    fn get_resource() -> About {
        About::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> About {
        About::new(&INVALID_CREDENTIALS)
    }

    #[test]
    fn new_test() {
        let credentials = VALID_CREDENTIALS.clone();
        let resource = About::new(&credentials);

        assert_eq!(resource.credentials, credentials);
    }

    #[test]
    fn get_test() {
        let about_request = get_resource().get();

        assert_eq!(about_request.method, Method::GET);
        assert_eq!(&about_request.url, "https://www.googleapis.com/drive/v3/about");
    }

    #[test]
    fn fields_test() {
        let about_request = get_resource().get()
            .fields("test-fields");

        assert_eq!( about_request.fields, Some("test-fields".into()) );
    }

    #[test]
    fn execute_test() {
        let response = get_resource().get()
            .fields("kind")
            .execute();

        assert!( response.is_ok() );
    }

    #[test]
    fn execute_invalid_credentials_test() {
        let response = get_invalid_resource().get()
            .fields("kind")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
