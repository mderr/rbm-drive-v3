use reqwest::Method;
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{objects, Credentials};

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments",
    returns=objects::Comment,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to create a comment on a file.
pub struct CreateRequest {
    /// The comment to be created
    #[drive_v3(body)]
    comment: Option<objects::Comment>
}

#[request(
    method=Method::DELETE,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
/// A request builder to delete a comment on a file.
pub struct DeleteRequest {}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}",
    returns=objects::Comment,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get a comment on a file.
pub struct GetRequest {
    /// Whether to return deleted comments.
    ///
    /// Deleted comments will not include their original content.
    #[drive_v3(parameter)]
    include_deleted: Option<bool>
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments",
    returns=objects::CommentList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list the comments on a file.
pub struct ListRequest {
    /// Whether to return deleted comments.
    ///
    /// Deleted comments will not include their original content.
    #[drive_v3(parameter)]
    include_deleted: Option<bool>,

    /// The maximum number of comments to return per page.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`next_page_token`](objects::CommentList::next_page_token) from the
    /// previous response.
    #[drive_v3(parameter)]
    page_token: Option<String>,

    /// The minimum value of `modifiedTime` for the result comments
    /// (RFC 3339 date-time).
    #[drive_v3(parameter)]
    start_modified_time: Option<String>,
}

#[request(
    method=Method::PATCH,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/comments/{comment_id}",
    returns=objects::Comment,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to update a comment on a file.
pub struct UpdateRequest {
    /// The updated comment
    #[drive_v3(body)]
    comment: Option<objects::Comment>
}

/// Comments on a file.
///
/// Some resource methods (such as [`comments.update`](Comments::update))
/// require a `comment_id`. Use the [`comments.list`](Comments::list) method
/// to retrieve the ID for a comment in a file.
///
/// # Examples:
///
/// List the comments in a file
///
/// ```no_run
/// # use drive_v3::{Error, Credentials, Drive};
/// #
/// # let drive = Drive::new( &Credentials::from_file(
/// #     "../.secure-files/google_drive_credentials.json",
/// #     &["https://www.googleapis.com/auth/drive.file"],
/// # )? );
/// #
/// let file_id = "some-file-id";
///
/// let comment_list = drive.comments.list(&file_id)
///     .fields("*") // You must set the fields
///     .page_size(10)
///     .execute()?;
///
/// if let Some(comments) = comment_list.comments {
///     for comment in comments {
///         println!("{}", comment);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Comments {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Comments {
    /// Creates a new [`Comments`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Creates a comment on a file.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/comments/create)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Comment;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let comment = Comment {
    ///     content: Some( "this is my comment".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let file_id = "some-file-id";
    ///
    /// let created_comment = drive.comments.create(&file_id)
    ///     .fields("*")
    ///     .comment(&comment)
    ///     .execute()?;
    ///
    /// assert_eq!(created_comment.content, comment.content);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn create<T: AsRef<str>> ( &self, file_id: T ) -> CreateRequest {
        CreateRequest::new(&self.credentials, file_id)
    }

    /// Deletes a comment.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/comments/delete)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    ///
    /// let response = drive.comments.delete(&file_id, &comment_id).execute();
    ///
    /// assert!( response.is_ok() );
    /// # Ok::<(), Error>(())
    /// ```
    pub fn delete<T, U> ( &self, file_id: T, comment_id: U ) -> DeleteRequest
        where
            T: AsRef<str>,
            U: AsRef<str>
    {
        DeleteRequest::new(&self.credentials, file_id, comment_id)
    }

    /// Gets a comment by ID.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/comments/get)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the [`fields`](GetRequest::fields)
    /// parameter.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    ///
    /// let created_comment = drive.comments.get(&file_id, &comment_id)
    ///     .fields("id, author, createdTime, content") // You must set the fields
    ///     .execute()?;
    ///
    /// println!("This is the comment:\n{}", created_comment);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get<T, U> ( &self, file_id: T, comment_id: U ) -> GetRequest
        where
            T: AsRef<str>,
            U: AsRef<str>
    {
        GetRequest::new(&self.credentials, file_id, comment_id)
    }

    /// Lists a file's comments.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/comments/list)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the [`fields`](ListRequest::fields)
    /// parameter.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let file_id = "some-file-id";
    ///
    /// let comment_list = drive.comments.list(&file_id)
    ///     .fields("*") // You must set the fields
    ///     .page_size(10)
    ///     .execute()?;
    ///
    /// if let Some(comments) = comment_list.comments {
    ///     for comment in comments {
    ///         println!("{}", comment);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list<T: AsRef<str>> ( &self, file_id: T ) -> ListRequest {
        ListRequest::new(&self.credentials, file_id)
    }

    /// Updates a comment with patch semantics.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/comments/update)
    /// for more information.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.file`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Comment;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let updated_comment = Comment {
    ///     content: Some( "this is the updated content of my comment".to_string() ),
    ///     ..Default::default()
    /// };
    ///
    /// let file_id = "some-file-id";
    /// let comment_id = "some-comment-id";
    ///
    /// let modified_comment = drive.comments.update(&file_id, &comment_id)
    ///     .fields("*")
    ///     .comment(&updated_comment)
    ///     .execute()?;
    ///
    /// assert_eq!(modified_comment.content, updated_comment.content);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn update<T, U> ( &self, file_id: T, comment_id: U ) -> UpdateRequest
        where
            T: AsRef<str>,
            U: AsRef<str>
    {
        UpdateRequest::new(&self.credentials, file_id, comment_id)
    }
}

#[cfg(test)]
mod tests {
    use super::Comments;
    use crate::{ErrorKind, objects, resources};
    use crate::utils::test::{INVALID_CREDENTIALS, VALID_CREDENTIALS};

    fn get_resource() -> Comments {
        Comments::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Comments {
        Comments::new(&INVALID_CREDENTIALS)
    }

    fn get_files_resource() -> resources::Files {
        resources::Files::new(&VALID_CREDENTIALS)
    }

    fn delete_file( file: &objects::File ) -> crate::Result<()> {
        get_files_resource().delete( file.clone().id.unwrap() ).execute()
    }

    fn get_test_file_metadata() -> objects::File {
        objects::File {
            name: Some( "test.txt".to_string() ),
            description: Some( "a test file".to_string() ),
            mime_type: Some( "text/plain".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_drive_file() -> crate::Result<objects::File> {
        let metadata = get_test_file_metadata();

        get_files_resource().create()
            .fields("*")
            .upload_type(objects::UploadType::Multipart)
            .metadata(&metadata)
            .content_string("content")
            .execute()
    }

    fn get_test_comment() -> objects::Comment {
        objects::Comment {
            content: Some( "test comment".to_string() ),
            ..Default::default()
        }
    }

    fn get_test_drive_comment( file: &objects::File ) -> crate::Result<objects::Comment> {
        let test_comment = get_test_comment();

        get_resource().create( &file.clone().id.unwrap() )
            .fields("*")
            .comment(&test_comment)
            .execute()
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn create_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_comment = get_test_comment();

        let response = get_resource().create( &test_drive_file.clone().id.unwrap() )
            .fields("*")
            .comment(&test_comment)
            .execute();

        assert!( response.is_ok() );

        let comment = response.unwrap();
        assert_eq!(comment.content, test_comment.content);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn create_invalid_test() {
        let response = get_invalid_resource().create("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn delete_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();

        let response = get_resource().delete(
                &test_drive_file.clone().id.unwrap(),
                &test_drive_comment.clone().id.unwrap(),
            )
            .execute();

        assert!( response.is_ok() );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn delete_invalid_test() {
        let response = get_invalid_resource().delete("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn get_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();

        let response = get_resource().get(
                &test_drive_file.clone().id.unwrap(),
                &test_drive_comment.clone().id.unwrap(),
            )
            .fields("*")
            .execute();

        assert!( response.is_ok() );

        let comment = response.unwrap();
        assert_eq!(comment, test_drive_comment);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn get_invalid_test() {
        let response = get_invalid_resource().get("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();

        let response = get_resource().list( &test_drive_file.clone().id.unwrap() )
            .fields("*")
            .execute();

        assert!( response.is_ok() );

        let comment_list = response.unwrap();
        assert_eq!( comment_list.comments, Some(vec![test_drive_comment]) );

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn list_invalid_test() {
        let response = get_invalid_resource().list("invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn update_test() {
        let test_drive_file = get_test_drive_file().unwrap();
        let test_drive_comment = get_test_drive_comment(&test_drive_file).unwrap();

        let mut updated_comment = test_drive_comment.clone();
        updated_comment.content = Some( "updated comment".to_string() );

        let response = get_resource().update(
                &test_drive_file.clone().id.unwrap(),
                &test_drive_comment.clone().id.unwrap(),
            )
            .fields("*")
            .comment(&updated_comment)
            .execute();

        assert!( response.is_ok() );

        let comment = response.unwrap();
        assert_eq!(comment.content, updated_comment.content);

        delete_file(&test_drive_file).expect("Failed to cleanup created file");
    }

    #[test]
    fn update_invalid_test() {
        let response = get_invalid_resource().update("invalid-id", "invalid-id")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
