use reqwest::Method;
use std::collections::HashMap;
use drive_v3_macros::{DriveRequestBuilder, request};

use super::DriveRequestBuilder;
use crate::{objects, Credentials, Error, ErrorKind};

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/changes/startPageToken",
)]
#[derive(DriveRequestBuilder)]
/// A request builder to get the starting `pageToken` for listing future
/// changes.
pub struct GetStartPageTokenRequest {
    /// The ID of the shared drive for which the starting pageToken for
    /// listing future changes from that shared drive will be returned.
    #[drive_v3(parameter)]
    drive_id: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,
}

impl GetStartPageTokenRequest {
    /// Executes this request.
    ///
    /// # Errors:
    ///
    /// - a [`UrlParsing`](crate::ErrorKind::UrlParsing) error, if the creation
    /// of the request's URL failed.
    /// - a [`Request`](crate::ErrorKind::Request) error, if unable to send the
    /// request or get a body from the response.
    /// - a [`Response`](crate::ErrorKind::Response) error, if the request
    /// returned an error response.
    /// - a [`Json`](crate::ErrorKind::Json) error, if unable to parse the
    /// response's body.
    pub fn execute( &self ) -> crate::Result<String> {
        let response = self.send()?;
        let response_text = response.text()?;
        let parsed_json = serde_json::from_str::<HashMap<&str, String>> (&response_text)?;

        match parsed_json.get("startPageToken") {
            Some(token) => Ok( token.clone() ),
            #[cfg(not(tarpaulin_include))]
            None => Err( Error::new(
                ErrorKind::Json,
                "the response did not contain the 'startPageToken' field",
            ) )
        }
    }
}

#[request(
    method=Method::GET,
    url="https://www.googleapis.com/drive/v3/changes",
    returns=objects::ChangeList,
)]
#[derive(DriveRequestBuilder)]
/// A request builder to list the changes for a user or drive.
pub struct ListRequest {
    /// The shared drive from which changes will be returned.
    ///
    /// If specified the change IDs will be reflective of the shared drive;
    /// use the combined drive ID and change ID as an identifier.
    #[drive_v3(parameter)]
    drive_id: Option<String>,

    /// Whether changes should include the file resource if the file is
    /// still accessible by the user at the time of the request, even when
    /// a file was removed from the list of changes and there will be no
    /// further change entries for this file.
    #[drive_v3(parameter)]
    include_corpus_removals: Option<bool>,

    /// Whether both My Drive and shared drive items should be included in
    /// results.
    #[drive_v3(parameter)]
    include_items_from_all_drives: Option<bool>,

    /// Whether to include changes indicating that items have been removed
    /// from the list of changes, for example by deletion or loss of access.
    #[drive_v3(parameter)]
    include_removed: Option<bool>,

    /// The maximum number of changes to return per page.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`next_page_token`](objects::ChangeList::next_page_token) from the
    /// previous response or to the response from the
    /// [`get_start_page_token`](Changes::get_start_page_token) method.
    #[drive_v3(parameter)]
    page_token: Option<String>,

    /// Whether to restrict the results to changes inside the My Drive
    /// hierarchy.
    ///
    /// This omits changes to files such as those in the Application Data
    /// folder or shared files which have not been added to My Drive.
    #[drive_v3(parameter)]
    restrict_to_my_drive: Option<bool>,

    /// A comma-separated list of spaces to query within the corpora.
    ///
    /// Supported values are `drive` and `appDataFolder`.
    #[drive_v3(parameter)]
    spaces: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,
}

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/changes/watch",
    returns=objects::Channel,
)]
#[derive(DriveRequestBuilder)]
/// A request builder for subscribing to changes for a user.
pub struct WatchRequest {
    /// The shared drive from which changes will be returned.
    ///
    /// If specified the change IDs will be reflective of the shared drive;
    /// use the combined drive ID and change ID as an identifier.
    #[drive_v3(parameter)]
    drive_id: Option<String>,

    /// Whether changes should include the file resource if the file is
    /// still accessible by the user at the time of the request, even when
    /// a file was removed from the list of changes and there will be no
    /// further change entries for this file.
    #[drive_v3(parameter)]
    include_corpus_removals: Option<bool>,

    /// Whether both My Drive and shared drive items should be included in
    /// results.
    #[drive_v3(parameter)]
    include_items_from_all_drives: Option<bool>,

    /// Whether to include changes indicating that items have been removed
    /// from the list of changes, for example by deletion or loss of access.
    #[drive_v3(parameter)]
    include_removed: Option<bool>,

    /// The maximum number of changes to return per page.
    #[drive_v3(parameter)]
    page_size: Option<i64>,

    /// The token for continuing a previous list request on the next page.
    ///
    /// This should be set to the value of
    /// [`next_page_token`](objects::ChangeList::next_page_token) from the
    /// previous response or to the response from the
    /// [`get_start_page_token`](Changes::get_start_page_token) method.
    #[drive_v3(parameter)]
    page_token: Option<String>,

    /// Whether to restrict the results to changes inside the My Drive
    /// hierarchy.
    ///
    /// This omits changes to files such as those in the Application Data
    /// folder or shared files which have not been added to My Drive.
    #[drive_v3(parameter)]
    restrict_to_my_drive: Option<bool>,

    /// A comma-separated list of spaces to query within the corpora.
    ///
    /// Supported values are `drive` and `appDataFolder`.
    #[drive_v3(parameter)]
    spaces: Option<String>,

    /// Whether the requesting application supports both My Drives and
    /// shared drives.
    #[drive_v3(parameter)]
    supports_all_drives: Option<bool>,

    /// Specifies which additional view's permissions to include in the
    /// response.
    ///
    /// Only `published` is supported.
    #[drive_v3(parameter)]
    include_permissions_for_view: Option<String>,

    /// A comma-separated list of IDs of labels to include in the
    /// [`label_info`](objects::File::label_info) part of the response.
    #[drive_v3(parameter)]
    include_labels: Option<String>,

    /// Sets the metadata that the channel will have.
    #[drive_v3(body)]
    channel: Option<objects::Channel>
}

/// A change to a file or shared drive.
///
/// # Examples:
///
/// List changes in a drive
///
/// ```no_run
/// # use drive_v3::{Error, Credentials, Drive};
/// #
/// # let drive = Drive::new( &Credentials::from_file(
/// #     "../.secure-files/google_drive_credentials.json",
/// #     &["https://www.googleapis.com/auth/drive.file"],
/// # )? );
/// #
/// let drive_id = "some-drive-id";
/// let page_token = "some-page-token";
///
/// let change_list = drive.changes.list()
///     .page_size(10)
///     .drive_id(drive_id)
///     .page_token(page_token)
///     .execute()?;
///
/// if let Some(changes) = change_list.changes {
///     for change in changes {
///         println!("{}", change);
///     }
/// }
/// # Ok::<(), Error>(())
/// ```
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Changes {
    /// Credentials used to authenticate a user's access to this resource.
    credentials: Credentials,
}

impl Changes {
    /// Creates a new [`Changes`] resource with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self { credentials: credentials.clone() }
    }

    /// Gets the starting `pageToken` for listing future changes.
    ///
    /// The starting page token is used for listing future changes. The page
    /// token doesn't expire.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/changes/getStartPageToken)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the
    /// [`drive_id`](GetStartPageTokenRequest::drive_id) and
    /// [`supports_all_drives`](GetStartPageTokenRequest::supports_all_drives)
    /// parameters.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let drive_id = "some-drive-id";
    ///
    /// let start_page_token = drive.changes.get_start_page_token()
    ///     .drive_id(drive_id)
    ///     .supports_all_drives(true)
    ///     .execute()?;
    ///
    /// println!("Your start page token is: {}", start_page_token);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn get_start_page_token( &self ) -> GetStartPageTokenRequest {
        GetStartPageTokenRequest::new(&self.credentials)
    }

    /// Lists the changes for a user or shared drive.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/changes/list)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the
    /// [`drive_id`](ListRequest::drive_id) and
    /// [`page_token`](ListRequest::page_token)
    /// parameters.
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    /// #
    /// let drive_id = "some-drive-id";
    /// let page_token = "some-page-token";
    ///
    /// let change_list = drive.changes.list()
    ///     .page_size(10)
    ///     .drive_id(drive_id)
    ///     .page_token(page_token)
    ///     .execute()?;
    ///
    /// if let Some(changes) = change_list.changes {
    ///     for change in changes {
    ///         println!("{}", change);
    ///     }
    /// }
    /// # Ok::<(), Error>(())
    /// ```
    pub fn list( &self ) -> ListRequest {
        ListRequest::new(&self.credentials)
    }

    /// Subscribes to changes for a user.
    ///
    /// See Google's
    /// [documentation](https://developers.google.com/drive/api/reference/rest/v3/changes/watch)
    /// for more information.
    ///
    /// # Note:
    ///
    /// This request requires you to set the
    /// [`drive_id`](ListRequest::drive_id) and
    /// [`page_token`](ListRequest::page_token)
    /// parameters.
    ///
    /// # Note
    ///
    /// In order to subscribe to changes, you must provide a
    /// [`Channel`](objects::Channel) with an `id` and an `address` which is the
    /// one that will receive the notifications. This can be done by creating a
    /// channel using [`from`](objects::Channel::from).
    ///
    /// For more informChangesation on channels, see Google's
    /// [documentation](https://developers.google.com/drive/api/guides/push).
    ///
    /// # Requires one of the following OAuth scopes:
    ///
    /// - `https://www.googleapis.com/auth/drive`
    /// - `https://www.googleapis.com/auth/drive.appdata`
    /// - `https://www.googleapis.com/auth/drive.file`
    /// - `https://www.googleapis.com/auth/drive.metadata`
    /// - `https://www.googleapis.com/auth/drive.metadata.readonly`
    /// - `https://www.googleapis.com/auth/drive.photos.readonly`
    /// - `https://www.googleapis.com/auth/drive.readonly`
    ///
    /// # Examples:
    ///
    /// ```no_run
    /// use drive_v3::objects::Channel;
    /// # use drive_v3::{Error, Credentials, Drive};
    /// #
    /// # let drive = Drive::new( &Credentials::from_file(
    /// #     "../.secure-files/google_drive_credentials.json",
    /// #     &["https://www.googleapis.com/auth/drive.file"],
    /// # )? );
    ///
    /// let channel_id = "my-channel-id";
    /// let channel_address = "https://mydomain.com/channel-notifications";
    /// let channel = Channel::from(&channel_id, &channel_address);
    ///
    /// let drive_id = "some-drive-id";
    /// let page_token = "some-page-token";
    ///
    /// let created_channel = drive.changes.watch()
    ///     .drive_id(drive_id)
    ///     .page_token(page_token)
    ///     .channel(&channel)
    ///     .execute()?;
    ///
    /// println!("this is the created channel:\n{}", created_channel);
    /// # Ok::<(), Error>(())
    /// ```
    pub fn watch( &self ) -> WatchRequest {
        WatchRequest::new(&self.credentials)
    }
}

#[cfg(test)]
mod tests {
    use super::Changes;
    use crate::{objects, ErrorKind};
    use crate::utils::test::{INVALID_CREDENTIALS, VALID_CREDENTIALS};

    fn get_resource() -> Changes {
        Changes::new(&VALID_CREDENTIALS)
    }

    fn get_invalid_resource() -> Changes {
        Changes::new(&INVALID_CREDENTIALS)
    }

    #[test]
    fn new_test() {
        let valid_resource = get_resource();
        let invalid_resource = get_invalid_resource();

        assert_eq!( valid_resource.credentials, VALID_CREDENTIALS.clone() );
        assert_eq!( invalid_resource.credentials, INVALID_CREDENTIALS.clone() );
    }

    #[test]
    fn get_start_page_token_invalid_response_test() {
        let response = get_invalid_resource().get_start_page_token()
            .drive_id("test-drive-id")
            .supports_all_drives(true)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn list_invalid_response_test() {
        let response = get_invalid_resource().list()
            .drive_id("test-drive-id")
            .page_token("test-page-token")
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }

    #[test]
    fn watch_invalid_response_test() {
        let channel = objects::Channel::from("test-channel-id", "test-address");

        let response = get_invalid_resource().watch()
            .drive_id("test-drive-id")
            .page_token("test-page-token")
            .channel(&channel)
            .execute();

        assert!( response.is_err() );
        assert_eq!( response.unwrap_err().kind, ErrorKind::Response );
    }
}
