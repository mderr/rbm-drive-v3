#![warn(missing_docs)]
#![doc = include_str!("../README.md")]

/// Objects and types returned by Google Drive's API.
#[cfg(not(tarpaulin_include))]
pub mod objects;

/// Resources that the user can call to get information from Google Drive's API.
pub mod resources;

/// Resources that the user can call to get information from Google Drive's API.
#[doc(hidden)]
pub(crate) mod utils;

/// Credentials for a authorizing requests to the Google Drive API.
mod credentials;

/// Custom errors that can arise during [`drive_v3`](crate) usage.
#[cfg(not(tarpaulin_include))]
mod error;

/// Files that contain authorization info for your API.
mod secrets;

#[doc(hidden)]
/// Utility functions for added security during authorization and authentication.
pub(crate) mod security;

/// Server that runs locally to receive authorization from the Google Drive API.
mod server;

/// Tokens used to authenticate and provide authorization information to Google APIs.
mod token;

/// Drive struct.
mod drive;

#[doc(hidden)]
/// Custom result that wraps the default on with the [`crate`]'s Error.
mod result;

pub use drive::Drive;
pub use server::LocalServer;
pub use secrets::ClientSecrets;
pub use credentials::Credentials;
pub use error::{Error, ErrorKind};
pub use token::{AccessToken, RefreshToken};
pub use result::Result;
