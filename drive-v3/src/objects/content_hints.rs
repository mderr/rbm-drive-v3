use std::fmt;
use serde::{Serialize, Deserialize};

use super::Thumbnail;

/// Additional information about the content of the file.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ContentHints {
    /// Text to be indexed for the file to improve fullText queries. This is limited to 128KB in length and may contain HTML
    /// elements.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub indexable_text: Option<String>,

    /// A thumbnail for the file. This will only be used if Google Drive cannot generate a standard thumbnail.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub thumbnail: Option<Thumbnail>,
}

impl fmt::Display for ContentHints {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl ContentHints {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
