use std::fmt;
use serde::{Serialize, Deserialize};

/// The type of upload request to the `/upload` URI.
///
/// If you are uploading data with an `/upload` URI, this field is required.
///
/// If you are creating a metadata-only file, this field is not required.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum UploadType {
    /// A [simple upload](https://developers.google.com/drive/api/guides/manage-uploads#simple) which allows you to upload a file
    /// without supplying its metadata.
    ///
    /// When you perform a simple upload, basic metadata is created and some attributes are inferred from the file, such as the
    /// [`mime_type`](super::File::mime_type) or [`modified_time`](super::File::modified_time). You can use a simple upload in
    /// cases where you have small files and file metadata isn't important.
    #[default]
    Media,

    /// A [multipart upload](https://developers.google.com/drive/api/guides/manage-uploads#multipart) which allows to upload
    /// metadata and data in the same request.
    ///
    /// Use this option if the data you send is small enough to upload again, in its entirety, if the connection fails.
    Multipart,

    /// A [resumable upload](https://developers.google.com/drive/api/guides/manage-uploads#resumable) which allows you to resume
    /// an upload operation after a communication failure interrupts the flow of data.
    ///
    /// Because you don't have to restart large file uploads from the start, resumable uploads can also reduce your bandwidth
    /// usage if there's a network failure.
    Resumable,
}

impl fmt::Display for UploadType {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let string = match self {
            Self::Media     => "media",
            Self::Multipart => "multipart",
            Self::Resumable => "resumable",
        };

        write!(f, "{}", string)
    }
}

/// Geographic location information stored in the image.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Location {
    /// The latitude stored in the image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub latitude: Option<f64>,

    /// The longitude stored in the image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub longitude: Option<f64>,

    /// The altitude stored in the image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub altitude: Option<f64>,
}

impl fmt::Display for Location {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Location {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// Additional metadata about image media, if available.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ImageMediaMetadata {
    /// Whether a flash was used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub flash_used: Option<bool>,

    /// The metering mode used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub metering_mode: Option<String>,

    /// The type of sensor used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sensor: Option<String>,

    /// The exposure mode used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exposure_mode: Option<String>,

    /// The color space of the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub color_space: Option<String>,

    /// The white balance mode used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub white_balance: Option<String>,

    /// The width of the image in pixels.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub width: Option<i64>,

    /// The height of the image in pixels.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub height: Option<i64>,

    /// Geographic location information stored in the image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location: Option<Location>,

    /// The number of clockwise 90 degree rotations applied from the image's original orientation.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rotation: Option<i64>,

    /// The date and time the photo was taken (EXIF DateTime).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub time: Option<String>,

    /// The make of the camera used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub camera_make: Option<String>,

    /// The model of the camera used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub camera_model: Option<String>,

    /// The length of the exposure, in seconds.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exposure_time: Option<f64>,

    /// The aperture used to create the photo (f-number).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub aperture: Option<f64>,

    /// The focal length used to create the photo, in millimeters.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub focal_length: Option<f64>,

    /// The ISO speed used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub iso_speed: Option<i64>,

    /// The exposure bias of the photo (APEX value).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub exposure_bias: Option<f64>,

    /// The smallest f-number of the lens at the focal length used to create the photo (APEX value).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_aperture_value: Option<f64>,

    /// The distance to the subject of the photo, in meters.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subject_distance: Option<i64>,

    /// The lens used to create the photo.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lens: Option<String>,
}

impl fmt::Display for ImageMediaMetadata {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl ImageMediaMetadata {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// Additional metadata about video media.
///
/// This may not be available immediately upon upload.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct VideoMediaMetadata {
    /// The width of the video in pixels.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub width: Option<i64>,

    /// The height of the video in pixels.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub height: Option<i64>,

    /// The duration of the video in milliseconds.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub duration_millis: Option<String>,
}

impl fmt::Display for VideoMediaMetadata {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl VideoMediaMetadata {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
