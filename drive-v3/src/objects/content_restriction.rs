use std::fmt;
use serde::{Serialize, Deserialize};

use super::User;

/// A restriction for accessing the content of the file.
///
/// # Warning:
///
/// The field `type` is renamed to `restriction_type` as the word type is a reserved keyword in Rust.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ContentRestriction {
    /// Whether the content of the file is read-only.
    ///
    /// If a file is read-only, a new revision of the file may not be added, comments may not be added or modified, and the title
    /// of the file may not be modified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub read_only: Option<bool>,

    /// Reason for why the content of the file is restricted.
    ///
    /// This is only mutable on requests that also set readOnly=true.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub reason: Option<String>,

    /// The type of the content restriction. Currently the only possible value is `globalContentRestriction`.
    #[serde(rename = "type")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub restriction_type: Option<String>,

    /// The user who set the content restriction.
    ///
    /// Only populated if [`read_only`](ContentRestriction::read_only) is true.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub restricting_user: Option<User>,

    /// The time at which the content restriction was set (formatted RFC 3339 timestamp).
    ///
    /// Only populated if [`read_only`](ContentRestriction::read_only) is true.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub restriction_time: Option<String>,

    /// Whether the content restriction can only be modified or removed by a user who owns the file.
    ///
    /// For files in shared drives, any user with organizer capabilities can modify or remove this content restriction.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner_restricted: Option<bool>,

    /// Whether the content restriction was applied by the system, for example due to an esignature.
    ///
    /// Users cannot modify or remove system restricted content restrictions.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub system_restricted: Option<bool>,
}

impl fmt::Display for ContentRestriction {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl ContentRestriction {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
