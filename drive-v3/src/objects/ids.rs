use std::fmt;
use serde::{Serialize, Deserialize};

/// The type of items which an ID can be used for.
///
/// Default: [`Files`](IDKind::Files).
///
/// # Note
///
/// [`Shortcuts`](IDKind::Shortcuts) are only supported in the [`Drive`](super::Space::Drive) space.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum IDKind {
    /// Files
    #[default]
    Files,

    /// Shortcuts
    Shortcuts
}

impl fmt::Display for IDKind {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let string = match self {
            Self::Files     => "files",
            Self::Shortcuts => "shortcuts",
        };

        write!(f, "{}", string)
    }
}

/// A list of themes that are supported for shared drives.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GeneratedIDs {
    /// The IDs generated for the requesting user in the specified space.
    pub ids: Vec<String>,

    /// The type of file that can be created with these IDs.
    pub space: super::Space,

    /// Identifies what kind of resource this is.
    ///
    /// Value is always `drive#generatedIds`.
    pub kind: String,
}

impl fmt::Display for GeneratedIDs {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}
