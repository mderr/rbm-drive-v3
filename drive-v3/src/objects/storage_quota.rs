use std::fmt;
use serde::{Serialize, Deserialize};

/// The user's storage quota limits and usage. All fields are measured in bytes.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StorageQuota {
    /// The usage limit, if applicable. This will not be present if the user has unlimited storage.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<String>,

    /// The usage by all files in Google Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub usage_in_drive: Option<String>,

    /// The usage by trashed files in Google Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub usage_in_drive_trash: Option<String>,

    /// The total usage across all services.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub usage: Option<String>,
}

impl fmt::Display for StorageQuota {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl StorageQuota {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
