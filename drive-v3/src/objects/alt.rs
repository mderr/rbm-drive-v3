use std::fmt;
use serde::{Serialize, Deserialize};

/// Alternative response formats.
///
/// Default: [`Json`](Alt::Json).
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Alt {
    /// JSON
    #[default]
    Json,

    /// Media
    Media
}

impl fmt::Display for Alt {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let string = match self {
            Self::Json  => "json",
            Self::Media => "media",
        };

        write!(f, "{}", string)
    }
}
