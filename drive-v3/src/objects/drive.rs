use std::fmt;
use serde::{Serialize, Deserialize};

use super::DriveCapabilities;

/// Background image for a shared drive.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BackgroundImageFile {
    /// The ID of an image file in Google Drive to use for the background image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// The X coordinate of the upper left corner of the cropping area in the
    /// background image.
    ///
    /// This is a value in the closed range of 0 to 1. This value represents the
    /// horizontal distance from the left side of the entire image to the left
    /// side of the cropping area divided by the width of the entire image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub x_coordinate: Option<f32>,

    /// The Y coordinate of the upper left corner of the cropping area in the
    /// background image.
    ///
    /// This is a value in the closed range of 0 to 1. This value represents the
    /// vertical distance from the top side of the entire image to the top side
    /// of the cropping area divided by the height of the entire image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub y_coordinate: Option<f32>,

    /// The width of the cropped image in the closed range of 0 to 1.
    ///
    /// This value represents the width of the cropped image divided by the
    /// width of the entire image. The height is computed by applying a width to
    /// height aspect ratio of 80 to 9. The resulting image must be at least
    /// 1280 pixels wide and 144 pixels high.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub width: Option<f32>,
}

impl fmt::Display for BackgroundImageFile {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl BackgroundImageFile {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A set of restrictions that apply to a shared drive or items inside a shared
/// drive.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DriveRestrictions {
    /// Whether the options to copy, print, or download files inside this shared
    /// drive, should be disabled for readers and commenters.
    ///
    /// When this restriction is set to `true`, it will override the similarly
    /// named field to `true` for any file inside this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub copy_requires_writer_permission: Option<bool>,

    /// Whether access to this shared drive and items inside this shared drive
    /// is restricted to users of the domain to which this shared drive belongs.
    ///
    /// This restriction may be overridden by other sharing policies controlled
    /// outside of this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain_users_only: Option<bool>,

    /// Whether access to items inside this shared drive is restricted to its
    /// members.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub drive_members_only: Option<bool>,

    /// Whether administrative privileges on this shared drive are required to
    /// modify restrictions.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub admin_managed_restrictions: Option<bool>,

    /// If `true`, only users with the organizer role can share folders. If
    /// `false`, users with either the organizer role or the file organizer role
    /// can share folders.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sharing_folders_requires_organizer_permission: Option<bool>,
}

impl fmt::Display for DriveRestrictions {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl DriveRestrictions {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// Representation of a shared drive.
///
/// Some resource methods (such as
/// [`drives.update`](crate::resources::Drives::update)) require a driveId.
/// Use the [`drives.list`](crate::resources::Drives::list) method to retrieve
/// the ID for a shared drive.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DriveInfo {
    /// The ID of this shared drive which is also the ID of the top level folder
    /// of this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// The name of this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    /// The color of this shared drive as an RGB hex string.
    ///
    /// It can only be set on a
    /// [`drives.update`](crate::resources::Drives::update) request that does
    /// not set `themeId`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub color_rgb: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#drive`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// A short-lived link to this shared drive's background image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub background_image_link: Option<String>,

    /// Capabilities the current user has on this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub capabilities: Option<DriveCapabilities>,

    /// The ID of the theme from which the background image and color will be
    /// set.
    ///
    /// The set of possible `driveThemes` can be retrieved from a
    /// [`about.get`](crate::resources::About::get) response. When not specified
    /// on a [`drives.create`](crate::resources::Drives::create) request, a
    /// random theme is chosen from which the background image and color are
    /// set. This is a write-only field; it can only be set on requests that
    /// don't set [`color_rgb`](DriveInfo::color_rgb) or
    /// [`background_image_file`](DriveInfo::background_image_file).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub theme_id: Option<String>,

    /// An image file and cropping parameters from which a background image for
    /// this shared drive is set.
    ///
    /// This is a write only field; it can only be set on
    /// [`drives.update`](crate::resources::Drives::update) requests that don't
    /// set [`theme_id`](DriveInfo::theme_id). When specified, all fields of the
    /// [`BackgroundImageFile`](BackgroundImageFile) must be set.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub background_image_file: Option<BackgroundImageFile>,

    /// The time at which the shared drive was created (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,

    /// Whether the shared drive is hidden from default view.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hidden: Option<bool>,

    /// A set of restrictions that apply to this shared drive or items inside
    /// this shared drive.
    ///
    /// Note that restrictions can't be set when creating a shared drive. To add
    /// a restriction, first create a shared drive and then use
    /// [`drives.update`](crate::resources::Drives::update) to add restrictions.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub restrictions: Option<DriveRestrictions>,

    /// The organizational unit of this shared drive.
    ///
    /// This field is only populated on
    /// [`drives.list`](crate::resources::Drives::list) responses when the
    /// `useDomainAdminAccess` parameter is set to `true`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub org_unit_id: Option<String>,
}

#[doc(hidden)]
impl From<&Self> for DriveInfo {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl fmt::Display for DriveInfo {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl DriveInfo {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A list of shared drives.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DriveInfoList {
    /// The page token for the next page of shared drives.
    ///
    /// This will be absent if the end of the list has been reached. If the
    /// token is rejected for any reason, it should be discarded, and
    /// pagination should be restarted from the first page of results. The page
    /// token is typically valid for several hours. However, if new items are
    /// added or removed, your expected results might differ.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#driveList`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The list of shared drives.
    ///
    /// If [`next_page_token`](DriveInfoList::next_page_token) is populated,
    /// then this list may be incomplete and an additional page of results
    /// should be fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub drives: Option<Vec<DriveInfo>>,
}

impl fmt::Display for DriveInfoList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl DriveInfoList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
