use std::fmt;
use serde::{Serialize, Deserialize};

use super::{Reply, User};

/// The file content to which the comment refers, typically within the anchor
/// region.
///
/// For a text file, for example, this would be the text at the location of the
/// comment.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct QuotedFileContent {
    /// The MIME type of the quoted content.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,

    /// The quoted content itself.
    ///
    /// This is interpreted as plain text if set through the API.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value: Option<String>,
}

impl fmt::Display for QuotedFileContent {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl QuotedFileContent {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A comment on a file.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Comment {
    /// The ID of the comment.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#comment`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The time at which the comment was created (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,

    /// The last time the comment or any of its replies was modified
    /// (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modified_time: Option<String>,

    /// Whether the comment has been resolved by one of its replies.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resolved: Option<bool>,

    /// A region of the document represented as a JSON string.
    ///
    /// For details on defining anchor properties, refer to Google's
    /// [Manage comments and replies](https://developers.google.com/drive/api/v3/manage-comments)
    /// documentation.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub anchor: Option<String>,

    /// The full list of replies to the comment in chronological order.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub replies: Option<Vec<Reply>>,

    /// The author of the reply.
    ///
    /// The author's email address and permission ID will not be populated.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<User>,

    /// Whether the reply has been deleted.
    ///
    /// A deleted reply has no content.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub deleted: Option<bool>,

    /// The content of the reply with HTML formatting.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub html_content: Option<String>,

    /// The plain text content of the reply.
    ///
    /// This field is used for setting the content, while
    /// [`html_content`](Reply::html_content) should be displayed. This is
    /// required on creates if no `action` is specified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,

    /// The file content to which the comment refers, typically within the
    /// anchor region.
    ///
    /// For a text file, for example, this would be the text at the location of
    /// the comment.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub quoted_file_content: Option<QuotedFileContent>,
}

#[doc(hidden)]
impl From<&Self> for Comment {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl fmt::Display for Comment {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Comment {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A list of comments on a file.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CommentList {
    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#commentList`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The list of comments.
    ///
    /// If [`next_page_token`](CommentList::next_page_token) is populated, then
    /// this list may be incomplete and an additional page of results should be
    /// fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comments: Option<Vec<Comment>>,

    /// The page token for the next page of comments.
    ///
    /// This will be absent if the end of the comments list has been reached. If
    /// the token is rejected for any reason, it should be discarded, and
    /// pagination should be restarted from the first page of results. The page
    /// token is typically valid for several hours. However, if new items are
    /// added or removed, your expected results might differ.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,
}

impl fmt::Display for CommentList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl CommentList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
