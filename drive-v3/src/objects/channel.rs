use std::fmt;
use serde::{Serialize, Deserialize};

/// A notification channel used to watch for resource changes.
///
/// # Warning:
///
/// The field `type` is renamed to `channel_type` as the word type is a reserved keyword in Rust.
#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Channel {
    /// A Boolean value to indicate whether payload is wanted.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub payload: Option<bool>,

    /// A UUID or similar unique string that identifies this channel.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// An opaque ID that identifies the resource being watched on this channel.
    ///
    /// Stable across different API versions.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resource_id: Option<String>,

    /// A version-specific identifier for the watched resource.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resource_uri: Option<String>,

    /// An arbitrary string delivered to the target address with each
    /// notification delivered over this channel.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub token: Option<String>,

    /// Date and time of notification channel expiration, expressed as a Unix
    /// timestamp, in milliseconds.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expiration: Option<String>,

    /// The type of delivery mechanism used for this channel.
    ///
    /// Valid values are `web_hook` or `webhook`.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub channel_type: Option<String>,

    /// The address where notifications are delivered for this channel.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub address: Option<String>,

    /// Additional parameters controlling delivery channel behavior.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub params: Option<serde_json::Map<String, serde_json::Value>>,

    /// Identifies this as a notification channel, which is `api#channel`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
}

impl fmt::Debug for Channel {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        f.debug_struct("Channel")
            .field("payload", &self.payload)
            .field("id", &self.id)
            .field("resource_id", &self.resource_id)
            .field("resource_uri", &self.resource_uri)
            .field("token", &format_args!("[hidden for security]"))
            .field("expiration", &self.expiration)
            .field("channel_type", &self.channel_type)
            .field("address", &self.address)
            .field("params", &self.params)
            .field("kind", &self.kind)
            .finish()
    }
}


impl fmt::Display for Channel {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Default for Channel {
    fn default() -> Self {
        Self {
            payload: Default::default(),
            id: Default::default(),
            resource_id: Default::default(),
            resource_uri: Default::default(),
            token: Default::default(),
            expiration: Default::default(),
            channel_type: Some( "web_hook".to_string() ),
            address: Default::default(),
            params: Default::default(),
            kind: Default::default(),
        }
    }
}

#[doc(hidden)]
impl From<&Self> for Channel {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl Channel {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }

    /// Creates a new instance of this struct with the given address.
    pub fn from<T, U> ( channel_id: T, address: U ) -> Self
        where
            T: AsRef<str>,
            U: AsRef<str>,
    {
        Self {
            id: Some( channel_id.as_ref().to_string() ),
            address: Some( address.as_ref().to_string() ),
            ..Default::default()
        }
    }
}
