use std::fmt;
use serde::{Serialize, Deserialize};

/// The Google Drive Spaces
///
/// Default: [`Drive`](Space::Drive).
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Space {
    /// A user's Drive
    #[default]
    Drive,

    /// The app data folder
    AppDataFolder
}

impl fmt::Display for Space {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let string = match self {
            Self::Drive         => "drive",
            Self::AppDataFolder => "appDataFolder",
        };

        write!(f, "{}", string)
    }
}
