use std::fmt;
use serde::{Serialize, Deserialize};

use super::{ContentHints, User, Permission, FileCapabilities, ImageMediaMetadata,
    VideoMediaMetadata, ShortcutDetails, ContentRestriction, LabelInfo};

/// Contains details about the link URLs that clients are using to refer to this item.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LinkShareMetadata {
    /// Whether the file is eligible for security update.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub security_update_eligible: Option<bool>,

    /// Whether the security update is enabled for this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub security_update_enabled: Option<bool>,
}

impl fmt::Display for LinkShareMetadata {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

#[doc(hidden)]
impl From<&Self> for LinkShareMetadata {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl LinkShareMetadata {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// List of the user's files.
///
/// # Note
///
/// All of the fields of [`FileList`] are an [`Option<T>`] since the values that
/// Google's API will return are dependent on the
/// [`fields`](crate::resources::files::ListRequest::fields) requested, by
/// default all fields will be requested but if you change this value only the
/// specified fields will be [`Some<T>`], the rest will be [`None`].
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FileList {
    /// The page token for the next page of files.
    ///
    /// This will be absent if the end of the files list has been reached. If the token is rejected for any reason, it should be
    /// discarded, and pagination should be restarted from the first page of results. The page token is typically valid for
    /// several hours. However, if new items are added or removed, your expected results might differ.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,

    /// Identifies what kind of resource this is. Value: the fixed string "drive#fileList".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// Whether the search process was incomplete.
    ///
    /// If `true`, then some search results might be missing, since all documents were not searched. This can occur when
    /// searching multiple drives with the `allDrives` corpora, but all corpora couldn't be searched. When this happens, it's
    /// suggested that clients narrow their query by choosing a different corpus such as `user` or `drive`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub incomplete_search: Option<bool>,

    /// The list of files.
    ///
    /// If [`next_page_token`](FileList::next_page_token) is populated, then this list may be incomplete and an additional page
    /// of results should be fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub files: Option<Vec<File>>,
}

impl fmt::Display for FileList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

#[doc(hidden)]
impl From<&Self> for FileList {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl FileList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// The metadata for a file.
///
/// Some resource methods (such as files.update) require a `fileId`. Use the `files.list` method to retrieve the ID for a file.
///
/// See Google's [documentation](https://developers.google.com/drive/api/reference/rest/v3/files) for a full list and more
/// detailed information.
///
/// # Warning
///
/// Fields like `teamDriveId` are not included in this struct, since they are marked as deprecated
/// in Google's [documentation](https://developers.google.com/drive/api/reference/rest/v3/about).
///
/// # Note
///
/// All of the fields of [`File`] are an [`Option<T>`] since the values that
/// Google's API will return are dependent on the
/// [`fields`](crate::resources::files::GetRequest::fields) requested, by
/// default all fields will be requested but if you change this value only the
/// specified fields will be [`Some<T>`], the rest will be [`None`].
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct File {
    /// Identifies what kind of resource this is.
    ///
    /// Value: the fixed string "drive#file".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// ID of the shared drive the file resides in.
    ///
    /// Only populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub drive_id: Option<String>,

    /// The final component of fullFileExtension.
    ///
    /// This is only available for files with binary content in Google Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub file_extension: Option<String>,

    /// Whether the options to copy, print, or download this file, should be disabled for readers and commenters.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub copy_requires_writer_permission: Option<bool>,

    /// The MD5 checksum for the content of the file.
    ///
    /// This is only applicable to files with binary content in Google Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub md5_checksum: Option<String>,

    /// Additional information about the content of the file.
    ///
    /// These fields are never populated in responses.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_hints: Option<ContentHints>,

    /// Whether users with only writer permission can modify the file's permissions.
    ///
    /// Not populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub writers_can_share: Option<bool>,

    /// Whether the file has been viewed by this user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub viewed_by_me: Option<bool>,

    /// The MIME type of the file.
    ///
    /// Google Drive attempts to automatically detect an appropriate value from uploaded content, if no value is provided. The
    /// value cannot be changed unless a new revision is uploaded.
    ///
    /// If a file is created with a Google Doc MIME type, the uploaded content is imported, if possible. The supported import
    /// formats are published in the About resource.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,

    /// Links for exporting Docs Editors files to specific formats.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub export_links: Option<serde_json::Map<String, serde_json::Value>>,

    /// The IDs of the parent folders which contain the file.
    ///
    /// If not specified as part of a create request, a `file` is placed directly in the user's My Drive folder. If not specified
    /// as part of a copy request, a `file` inherits any discoverable parents of the source file. `files.update` requests must
    /// use the `addParents` and `removeParents` parameters to modify the parents list.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub parents: Option<Vec<String>>,

    /// A short-lived link to the file's thumbnail, if available.
    ///
    /// Typically lasts on the order of hours. Only populated when the requesting app can access the file's content. If the file
    /// isn't shared publicly, the URL returned in Files.thumbnailLink must be fetched using a credentialed request.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub thumbnail_link: Option<String>,

    /// A static, unauthenticated link to the file's icon.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon_link: Option<String>,

    /// Whether the file has been shared.
    ///
    /// Not populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shared: Option<bool>,

    /// The last user to modify the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_modifying_user: Option<User>,

    /// The owner of this file.
    ///
    /// Only certain legacy files may have more than one owner. This field isn't populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owners: Option<Vec<User>>,

    /// The ID of the file's head revision.
    ///
    /// This is currently only available for files with binary content in Google Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub head_revision_id: Option<String>,

    /// The user who shared the file with the requesting user, if applicable.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sharing_user: Option<User>,

    /// A link for opening the file in a relevant Google editor or viewer in a browser.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub web_view_link: Option<String>,

    /// A link for downloading the content of the file in a browser.
    ///
    /// This is only available for files with binary content in Google Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub web_content_link: Option<String>,

    /// Size in bytes of blobs and first party editor files.
    ///
    /// Won't be populated for files that have no size, like `shortcuts` and `folders`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub size: Option<String>,

    /// The full list of permissions for the file.
    ///
    /// This is only available if the requesting user can share the file. Not populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permissions: Option<Vec<Permission>>,

    /// Whether this file has a thumbnail.
    ///
    /// This does not indicate whether the requesting app has access to the thumbnail. To check access, look for the presence of
    /// the [`thumbnail_link`](File::thumbnail_link) field.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub has_thumbnail: Option<bool>,

    /// The list of spaces which contain the file.
    ///
    /// The currently supported values are `drive`, `appDataFolder` and `photos`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub spaces: Option<Vec<String>>,

    /// The color for a folder or a shortcut to a folder as an RGB hex string.
    ///
    /// The supported colors are published in the [`folder_color_palette`](super::About::folder_color_palette) field of the
    /// [`About`](super::About) resource.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub folder_color_rgb: Option<String>,

    /// The ID of the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// The name of the file.
    ///
    /// This is not necessarily unique within a folder. Note that for immutable items such as the top level folders of shared
    /// drives, My Drive root folder, and Application Data folder the name is constant.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    /// A short description of the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,

    /// Whether the user has starred the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub starred: Option<bool>,

    /// Whether the file has been trashed, either explicitly or from a trashed parent folder.
    ///
    /// Only the owner may trash a file, and other users cannot see files in the owner's trash.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trashed: Option<bool>,

    /// Whether the file has been explicitly trashed, as opposed to recursively trashed from a parent folder.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub explicitly_trashed: Option<bool>,

    /// The time at which the file was created (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,

    /// The last time the file was modified by anyone (RFC 3339 date-time).
    ///
    /// Note that setting `modified_time` also updates `modified_by_me_time` for the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modified_time: Option<String>,

    /// The last time the file was modified by the user (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modified_by_me_time: Option<String>,

    /// The last time the file was viewed by the user (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub viewed_by_me_time: Option<String>,

    /// The time at which the file was shared with the user, if applicable (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shared_with_me_time: Option<String>,

    /// The number of storage quota bytes used by the file. This includes the head revision as well as previous revisions with
    /// `keepForever` enabled.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub quota_bytes_used: Option<String>,

    /// A monotonically increasing version number for the file. This reflects every change made to the file on the server, even
    /// those not visible to the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub version: Option<String>,

    /// The original filename of the uploaded content if available, or else the original value of the
    /// [`name`](File::name) field.
    ///
    /// This is only available for files with binary content in Google Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub original_filename: Option<String>,

    /// Whether the user owns the file. Not populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owned_by_me: Option<bool>,

    /// The full file extension extracted from the [`name`](File::name) field.
    ///
    /// May contain multiple concatenated extensions, such as "`tar.gz`". This is only available for files with binary content in
    /// Google Drive.
    ///
    /// This is automatically updated when the [`name`](File::name) field changes, however it is not cleared if the new
    /// [`name`](File::name) does not contain a valid extension.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub full_file_extension: Option<String>,

    /// A collection of arbitrary key-value pairs which are visible to all apps.
    ///
    /// Entries with null values are cleared in update and copy requests.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<serde_json::Map<String, serde_json::Value>>,

    /// A collection of arbitrary key-value pairs which are private to the requesting app.
    ///
    /// Entries with null values are cleared in update and copy requests.
    ///
    /// These properties can only be retrieved using an authenticated request. An authenticated request uses an access token
    /// obtained with an OAuth 2 client ID. You cannot use an API key to retrieve private properties.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub app_properties: Option<serde_json::Map<String, serde_json::Value>>,

    /// Whether the file was created or opened by the requesting app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_app_authorized: Option<bool>,

    /// Capabilities the current user has on this file.
    ///
    /// Each capability corresponds to a fine-grained action that a user may take.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub capabilities: Option<FileCapabilities>,

    /// Whether there are permissions directly on this file.
    ///
    /// This field is only populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub has_augmented_permissions: Option<bool>,

    /// If the file has been explicitly trashed, the user who trashed it.
    ///
    /// Only populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trashing_user: Option<User>,

    /// The thumbnail version for use in thumbnail cache invalidation.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub thumbnail_version: Option<String>,

    /// The time that the item was trashed (RFC 3339 date-time).
    ///
    /// Only populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trashed_time: Option<String>,

    /// Whether the file has been modified by this user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modified_by_me: Option<bool>,

    /// files.list of permission IDs for users with access to this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permission_ids: Option<Vec<String>>,

    /// Additional metadata about image media, if available.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub image_media_metadata: Option<ImageMediaMetadata>,

    /// Additional metadata about video media.
    ///
    /// This may not be available immediately upon upload.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub video_media_metadata: Option<VideoMediaMetadata>,

    /// Shortcut file details.
    ///
    /// Only populated for shortcut files, which have the [`mime_type`](File::mime_type) field set to
    /// `application/vnd.google-apps.shortcut`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub shortcut_details: Option<ShortcutDetails>,

    /// Restrictions for accessing the content of the file.
    ///
    /// Only populated if such a restriction exists.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_restrictions: Option<Vec<ContentRestriction>>,

    /// A key needed to access the item via a shared link.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub resource_key: Option<String>,

    /// Contains details about the link URLs that clients are using to refer to this item.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub link_share_metadata: Option<LinkShareMetadata>,

    /// An overview of the labels on the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label_info: Option<LabelInfo>,

    /// The SHA1 checksum associated with this file, if available.
    ///
    /// This field is only populated for files with content stored in Google Drive; it is not populated for Docs Editors or
    /// shortcut files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sha1_checksum: Option<String>,

    /// The SHA256 checksum associated with this file, if available.
    ///
    /// This field is only populated for files with content stored in Google Drive; it is not populated for Docs Editors or
    /// shortcut files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sha256_checksum: Option<String>,
}

impl fmt::Display for File {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

#[doc(hidden)]
impl From<&Self> for File {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl File {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
