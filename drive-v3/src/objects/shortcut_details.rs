use std::fmt;
use serde::{Serialize, Deserialize};

/// Shortcut file details.
///
/// Only populated for shortcut files, which have the [`mime_type`](super::File::mime_type) field set to
/// `application/vnd.google-apps.shortcut`.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ShortcutDetails {
    /// The ID of the file that this shortcut points to.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_id: Option<String>,

    /// The MIME type of the file that this shortcut points to.
    ///
    /// The value of this field is a snapshot of the target's MIME type, captured when the shortcut is created.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_mime_type: Option<String>,

    /// The [`resource_key`](super::File::resource_key) for the target file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub target_resource_key: Option<String>,
}

impl fmt::Display for ShortcutDetails {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl ShortcutDetails {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
