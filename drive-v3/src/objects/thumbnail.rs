use std::fmt;

use serde::{Serialize, Deserialize};

/// A thumbnail for the file.
///
/// This will only be used if Google Drive cannot generate a standard thumbnail.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Thumbnail {
    /// The thumbnail data encoded with URL-safe Base64 (RFC 4648 section 5).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub image: Option<String>,

    /// The MIME type of the thumbnail.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
}

impl fmt::Display for Thumbnail {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Thumbnail {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
