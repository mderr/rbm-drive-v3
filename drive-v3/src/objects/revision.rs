use std::fmt;
use serde::{Serialize, Deserialize};

use super::User;

/// The metadata for a revision to a file.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Revision {
    /// The ID of the revision.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// The MIME type of the revision.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#revision`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// Whether this revision is published.
    ///
    /// This is only applicable to Docs Editors files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub published: Option<bool>,

    /// Links for exporting Docs Editors files to specific formats.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub export_links: Option< serde_json::Map<String, serde_json::Value> >,

    /// Whether to keep this revision forever, even if it is no longer the head
    /// revision.
    ///
    /// If not set, the revision will be automatically purged 30 days after
    /// newer content is uploaded. This can be set on a maximum of 200 revisions
    /// for a file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub keep_forever: Option<bool>,

    /// The MD5 checksum of the revision's content.
    ///
    /// This is only applicable to files with binary content in Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub md5_checksum: Option<String>,

    /// The last time the revision was modified (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modified_time: Option<String>,

    /// Whether subsequent revisions will be automatically republished.
    ///
    /// This is only applicable to Docs Editors files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub publish_auto: Option<bool>,

    /// Whether this revision is published outside the domain.
    ///
    /// This is only applicable to Docs Editors files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub published_outside_domain: Option<bool>,

    /// A link to the published revision.
    ///
    /// This is only populated for Google Sites files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub published_link: Option<String>,

    /// The size of the revision's content in bytes.
    ///
    /// This is only applicable to files with binary content in Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub size: Option<String>,

    /// The original filename used to create this revision.
    ///
    /// This is only applicable to files with binary content in Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub original_filename: Option<String>,

    /// The last user to modify this revision.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub last_modifying_user: Option<User>,
}

#[doc(hidden)]
impl From<&Self> for Revision {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl fmt::Display for Revision {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Revision {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A list of revisions to a file.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RevisionList {
    /// The page token for the next page of replies.
    ///
    /// This will be absent if the end of the list has been reached. If the
    /// token is rejected for any reason, it should be discarded, and
    /// pagination should be restarted from the first page of results. The page
    /// token is typically valid for several hours. However, if new items are
    /// added or removed, your expected results might differ.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#revisionList`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The list of replies.
    ///
    /// If [`next_page_token`](RevisionList::next_page_token) is populated,
    /// then this list may be incomplete and an additional page of results
    /// should be fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub revisions: Option<Vec<Revision>>,
}

impl fmt::Display for RevisionList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl RevisionList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
