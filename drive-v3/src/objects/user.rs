use std::fmt;
use serde::{Serialize, Deserialize};

/// Information about a Drive user.
///
/// See Google's [documentation](https://developers.google.com/drive/api/reference/rest/v3/User) for a full list and more
/// detailed information.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct User {
    /// A plain text displayable name for this user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,

    /// Identifies what kind of resource this is. Value: the fixed string "drive#user".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// Whether this user is the requesting user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub me: Option<bool>,

    /// The user's ID as visible in Permission resources.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permission_id: Option<String>,

    /// The email address of the user. This may not be present in certain contexts if the user has not made their email address
    /// visible to the requester.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email_address: Option<String>,

    /// A link to the user's profile photo, if available.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub photo_link: Option<String>,
}

impl fmt::Display for User {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl User {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
