use std::fmt;
use serde::{Serialize, Deserialize};

/// An icon for an app.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppIcon {
    /// Size of the icon.
    ///
    /// Represented as the maximum of the width and height.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub size: Option<i64>,

    /// Category of the icon.
    ///
    /// Allowed values are:
    /// - `application`: The icon for the application.
    /// - `document`: The icon for a file associated with the app.
    /// - `documentShared`: The icon for a shared file associated with the app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub category: Option<String>,

    /// URL for the icon.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icon_url: Option<String>,
}

impl fmt::Display for AppIcon {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl AppIcon {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A list of apps which the user has installed.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppList {
    /// The list of app IDs that the user has specified to use by default.
    ///
    /// The list is in reverse-priority order (lowest to highest).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub default_app_ids: Option<Vec<String>>,

    /// Identifies what kind of resource this is.
    ///
    /// This always is `drive#appList`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// A link back to this list.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub self_link: Option<String>,

    /// The list of apps.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<App>>,
}

impl fmt::Display for AppList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl AppList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// An app that a user has installed.
///
/// Contains information about its supported MIME types, file extensions, and 
/// other details.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct App {
    /// The name of the app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,

    /// The type of object this app creates such as a Chart.
    ///
    /// If empty, the app name should be used instead.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub object_type: Option<String>,

    /// Whether this app supports creating objects.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub supports_create: Option<bool>,

    /// A link to the product listing for this app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub product_url: Option<String>,

    /// The list of primary MIME types.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub primary_mime_types: Option<Vec<String>>,

    /// The list of secondary MIME types.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub secondary_mime_types: Option<Vec<String>>,

    /// The list of primary file extensions.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub primary_file_extensions: Option<Vec<String>>,

    /// The list of secondary file extensions.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub secondary_file_extensions: Option<Vec<String>>,

    /// The ID of the app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// Whether this app supports importing from Google Docs.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub supports_import: Option<bool>,

    /// Whether the app is installed.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub installed: Option<bool>,

    /// Whether the app is authorized to access data on the user's Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub authorized: Option<bool>,

    /// The various icons for the app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub icons: Option<Vec<AppIcon>>,

    /// Whether the app is selected as the default handler for the types it supports.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub use_by_default: Option<bool>,

    /// Identifies what kind of resource this is.
    ///
    /// This always is `drive#app`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// A short description of the app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub short_description: Option<String>,

    /// A long description of the app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub long_description: Option<String>,

    /// Whether this app supports opening more than one file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub supports_multi_open: Option<bool>,

    /// The ID of the product listing for this app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub product_id: Option<String>,

    /// The template URL for opening files with this app.
    ///
    /// The template contains `{ids}` or `{exportIds}` to be replaced by the
    /// actual file IDs.
    ///
    /// For more information, see Google's
    /// [Open Files](https://developers.google.com/drive/api/guides/integrate-open)
    /// documentation.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub open_url_template: Option<String>,

    /// The URL to create a file with this app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub create_url: Option<String>,

    /// The template URL to create a file with this app in a given folder.
    ///
    /// The template contains the `{folderId}` to be replaced by the folder ID
    /// house the new file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub create_in_folder_template: Option<String>,

    /// Whether this app supports creating files when offline.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub supports_offline_create: Option<bool>,

    /// Whether the app has Drive-wide scope.
    ///
    /// An app with Drive-wide scope can access all files in the user's Drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub has_drive_wide_scope: Option<bool>,
}

impl fmt::Display for App {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl App {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
