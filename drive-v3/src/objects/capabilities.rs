use std::fmt;
use serde::{Serialize, Deserialize};

/// Capabilities the current user has on a file.
///
/// Each capability corresponds to a fine-grained action that a user may take.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FileCapabilities {
    /// Whether the current user can move children of this folder outside of the
    /// shared drive.
    ///
    /// This is false when the item is not a folder. Only populated for items
    /// in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_move_children_out_of_drive: Option<bool>,

    /// Whether the current user can read the shared drive to which this file
    /// belongs.
    ///
    /// Only populated for items in shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_read_drive: Option<bool>,

    /// Whether the current user can edit this file.
    ///
    /// Other factors may limit the type of changes a user can make to a file.
    /// For example, see
    /// [`can_change_copy_requires_writer_permission`](FileCapabilities::can_change_copy_requires_writer_permission)
    /// or [`can_modify_content`](FileCapabilities::can_modify_content).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_edit: Option<bool>,

    /// Whether the current user can copy this file.
    ///
    /// For an item in a shared drive, whether the current user can copy
    /// non-folder descendants of this item, or this item itself if it is not a
    /// folder.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_copy: Option<bool>,

    /// Whether the current user can comment on this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_comment: Option<bool>,

    /// Whether the current user can add children to this folder.
    ///
    /// This is always false when the item is not a folder.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_add_children: Option<bool>,

    /// Whether the current user can delete this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_delete: Option<bool>,

    /// Whether the current user can download this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_download: Option<bool>,

    /// Whether the current user can list the children of this folder.
    ///
    /// This is always false when the item is not a folder.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_list_children: Option<bool>,

    /// Whether the current user can remove children from this folder.
    ///
    /// This is always false when the item is not a folder. For a folder in a
    /// shared drive, use
    /// [`can_delete_children`](FileCapabilities::can_delete_children) or
    /// [`can_trash_children`](FileCapabilities::can_trash_children) instead.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_remove_children: Option<bool>,

    /// Whether the current user can rename this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_rename: Option<bool>,

    /// Whether the current user can move this file to trash.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_trash: Option<bool>,

    /// Whether the current user can read the revisions resource of this file.
    ///
    /// For a shared drive item, whether revisions of non-folder descendants of
    /// this item, or this item itself if it is not a folder, can be read.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_read_revisions: Option<bool>,

    /// Whether the current user can change the
    /// [`copy_requires_writer_permission`](super::File::copy_requires_writer_permission)
    /// restriction of this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_change_copy_requires_writer_permission: Option<bool>,

    /// Whether the current user can restore this file from trash.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_untrash: Option<bool>,

    /// Whether the current user can modify the content of this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_modify_content: Option<bool>,

    /// Whether the current user can delete children of this folder.
    ///
    /// This is false when the item is not a folder. Only populated for items in
    /// shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_delete_children: Option<bool>,

    /// Whether the current user can trash children of this folder.
    ///
    /// This is false when the item is not a folder. Only populated for items in
    /// shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_trash_children: Option<bool>,

    /// Whether the current user can move this item outside of this drive by
    /// changing its parent.
    ///
    /// Note that a request to change the parent of the item may still fail
    /// depending on the new parent that is being added.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_move_item_out_of_drive: Option<bool>,

    /// Whether the current user can add a parent for the item without removing
    /// an existing parent in the same request.
    ///
    /// Not populated for shared drive files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_add_my_drive_parent: Option<bool>,

    /// Whether the current user can move this item within this drive.
    ///
    /// Note that a request to change the parent of the item may still fail
    /// depending on the new parent that is being added and the parent that is
    /// being removed.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_move_item_within_drive: Option<bool>,

    /// Whether the current user can modify the sharing settings for this file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_share: Option<bool>,

    /// Whether the current user can move children of this folder within this
    /// drive.
    ///
    /// This is false when the item is not a folder. Note that a request to move
    /// the child may still fail depending on the current user's access to the
    /// child and to the destination folder.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_move_children_within_drive: Option<bool>,

    /// Whether the current user can add a folder from another drive (different
    /// shared drive or My Drive) to this folder.
    ///
    /// This is false when the item is not a folder. Only populated for items in
    /// shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_add_folder_from_another_drive: Option<bool>,

    /// Whether the current user can change the
    /// [`security_update_enabled`](super::LinkShareMetadata::security_update_enabled)
    /// field on link share metadata.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_change_security_update_enabled: Option<bool>,

    /// Whether the current user is the pending owner of the file.
    ///
    /// Not populated for shared drive files.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_accept_ownership: Option<bool>,

    /// Whether the current user can read the labels on the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_read_labels: Option<bool>,

    /// Whether the current user can modify the labels on the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_modify_labels: Option<bool>,

    /// Whether the current user can add or modify content restrictions on the
    /// file which are editor restricted.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_modify_editor_content_restriction: Option<bool>,

    /// Whether the current user can add or modify content restrictions which
    /// are owner restricted.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_modify_owner_content_restriction: Option<bool>,

    /// Whether there is a content restriction on the file that can be removed
    /// by the current user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_remove_content_restriction: Option<bool>,
}

impl fmt::Display for FileCapabilities {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl FileCapabilities {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// Capabilities the current user has on a shared drive.
///
/// Each capability corresponds to a fine-grained action that a user may take.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DriveCapabilities {
    /// Whether the current user can add children to folders in this shared
    /// drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_add_children: Option<bool>,

    /// Whether the current user can comment on files in this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_comment: Option<bool>,

    /// Whether the current user can copy files in this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_copy: Option<bool>,

    /// Whether the current user can delete this shared drive.
    ///
    /// Attempting to delete the shared drive may still fail if there are
    /// untrashed items inside the shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_delete_drive: Option<bool>,

    /// Whether the current user can download files in this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_download: Option<bool>,

    /// Whether the current user can edit files in this shared drive
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_edit: Option<bool>,

    /// Whether the current user can list the children of folders in this shared
    /// drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_list_children: Option<bool>,

    /// Whether the current user can add members to this shared drive or remove
    /// them or change their role.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_manage_members: Option<bool>,

    /// Whether the current user can read the revisions resource of files in
    /// this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_read_revisions: Option<bool>,

    /// Whether the current user can rename files or folders in this shared
    /// drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_rename: Option<bool>,

    /// Whether the current user can rename this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_rename_drive: Option<bool>,

    /// Whether the current user can change the background of this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_change_drive_background: Option<bool>,

    /// Whether the current user can share files or folders in this shared
    /// drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_share: Option<bool>,

    /// Whether the current user can change the `copyRequiresWriterPermission`
    /// restriction of this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_change_copy_requires_writer_permission_restriction: Option<bool>,

    /// Whether the current user can change the `domainUsersOnly` restriction
    /// of this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_change_domain_users_only_restriction: Option<bool>,

    /// Whether the current user can change the driveMembersOnly restriction of
    /// this shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_change_drive_members_only_restriction: Option<bool>,

    /// Whether the current user can change the
    /// `sharingFoldersRequiresOrganizerPermission` restriction of this shared
    /// drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_change_sharing_folders_requires_organizer_permission_restriction: Option<bool>,

    /// Whether the current user can reset the shared drive restrictions to
    /// defaults.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_reset_drive_restrictions: Option<bool>,

    /// Whether the current user can delete children from folders in this shared
    /// drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_delete_children: Option<bool>,

    /// Whether the current user can trash children from folders in this shared
    /// drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_trash_children: Option<bool>,
}

impl fmt::Display for DriveCapabilities {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl DriveCapabilities {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
