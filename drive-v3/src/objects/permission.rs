use std::fmt;
use serde::{Serialize, Deserialize};

/// Details of whether the permissions on this shared drive item are inherited or directly on this item.
///
/// This is an output-only
/// field which is present only for shared drive items.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PermissionDetails {
    /// The permission type for this user.
    ///
    /// While new values may be added in future, the following are currently
    /// possible:
    /// - `file`
    /// - `member`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permission_type: Option<String>,

    /// The ID of the item from which this permission is inherited.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inherited_from: Option<String>,

    /// The primary role for this user.
    ///
    /// While new values may be added in the future, the following are currently
    /// possible:
    /// - `organizer`
    /// - `fileOrganizer`
    /// - `writer`
    /// - `commenter`
    /// - `reader`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub role: Option<String>,

    /// Whether this permission is inherited.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inherited: Option<bool>,
}

impl fmt::Display for PermissionDetails {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl PermissionDetails {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A permission for a file.
///
/// A permission grants a user, group, domain, or the world access to a file or
/// a folder hierarchy.
///
/// # Warning:
///
/// The field `type` is renamed to `permission_type` as the word type is a reserved keyword in Rust.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Permission {
    /// The ID of this permission. This is a unique identifier for the grantee, and is published in User resources as
    /// permissionId. IDs should be treated as opaque values.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// The "pretty" name of the value of the permission.
    ///
    /// The following is a list of examples for each type of permission:
    /// - `user`: User's full name, as defined for their Google account, such as "Joe Smith."
    /// - `group`: Name of the Google Group, such as "The Company Administrators."
    /// - `domain`: String domain name, such as "company.com."
    /// - `anyone`: No displayName is present.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,

    /// The type of the grantee.
    ///
    /// Valid values are:
    /// - `user`
    /// - `group`
    /// - `domain`
    /// - `anyone`
    ///
    /// When creating a permission, if type is `user` or `group`, you must provide an `emailAddress` for the user or group. When
    /// type is `domain`, you must provide a domain. There isn't extra information required for an anyone type.
    /// # Warning:
    ///
    /// This field is renamed from `type` as the word type is a reserved keyword in Rust.
    #[serde(rename = "type")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permission_type: Option<String>,

    /// Identifies what kind of resource this is. Value: the fixed string "drive#permission".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// Details of whether the permissions on this shared drive item are inherited or directly on this item. This is an
    /// output-only field which is present only for shared drive items.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permission_details: Option<Vec<PermissionDetails>>,

    /// A link to the user's profile photo, if available.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub photo_link: Option<String>,

    /// The email address of the user or group to which this permission refers.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email_address: Option<String>,

    /// The role granted by this permission.
    ///
    /// While new values may be supported in the future, the following are currently allowed:
    /// - `owner`
    /// - `organizer`
    /// - `fileOrganizer`
    /// - `writer`
    /// - `commenter`
    /// - `reader`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub role: Option<String>,

    /// Whether the permission allows the file to be discovered through search. This is only applicable for permissions of type
    /// `domain` or `anyone`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub allow_file_discovery: Option<bool>,

    /// The domain to which this permission refers.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,

    /// The time at which this permission will expire (RFC 3339 date-time).
    ///
    /// Expiration times have the following restrictions:
    /// - They can only be set on `user` and `group` permissions.
    /// - The time must be in the future.
    /// - The time cannot be more than a year in the future.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expiration_time: Option<String>,

    /// Whether the account associated with this permission has been deleted.
    ///
    /// This field only pertains to `user` and `group` permissions.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub deleted: Option<bool>,

    /// Indicates the view for this permission.
    ///
    /// Only populated for permissions that belong to a view. `published` is the only supported value.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub view: Option<String>,

    /// Whether the account associated with this permission is a pending owner.
    ///
    /// Only populated for user type permissions for files that are not in a shared drive.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pending_owner: Option<bool>,
}

#[doc(hidden)]
impl From<&Self> for Permission {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl fmt::Display for Permission {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Permission {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A list of permissions for a file.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PermissionList {
    /// The page token for the next page of permissions.
    ///
    /// This will be absent if the end of the list has been reached. If the
    /// token is rejected for any reason, it should be discarded, and
    /// pagination should be restarted from the first page of results. The page
    /// token is typically valid for several hours. However, if new items are
    /// added or removed, your expected results might differ.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#permissionList`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The list of shared drives.
    ///
    /// If [`next_page_token`](PermissionList::next_page_token) is populated,
    /// then this list may be incomplete and an additional page of results
    /// should be fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub permissions: Option<Vec<Permission>>,
}

impl fmt::Display for PermissionList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl PermissionList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
