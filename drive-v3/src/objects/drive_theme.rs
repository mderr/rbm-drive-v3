use std::fmt;
use serde::{Serialize, Deserialize};

/// A list of themes that are supported for shared drives.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DriveTheme {
    /// The ID of the theme.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// A link to this theme's background image.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub background_image_link: Option<String>,

    /// The color of this theme as an RGB hex string.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub color_rgb: Option<String>,
}

impl fmt::Display for DriveTheme {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl DriveTheme {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
