use std::fmt;
use serde::{Serialize, Deserialize};

use super::{StorageQuota, DriveTheme, User};

/// Information about the user, the user's Drive, and system capabilities.
///
/// See Google's
/// [documentation](https://developers.google.com/drive/api/reference/rest/v3/about)
/// for a full list and more detailed information.
///
/// # Warning
///
/// Fields like `teamDriveThemes` or `canCreateTeamDrives` are not included in
/// this struct, since they are marked as deprecated in Google's
/// [documentation](https://developers.google.com/drive/api/reference/rest/v3/about).
///
/// # Note
///
/// All of the fields of [`About`] are an [`Option`] since the values that
/// Google's API will return are dependent on the
/// [`fields`](crate::resources::about::GetRequest::fields) requested.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct About {
    /// Identifies what kind of resource this is. Value: the fixed string "drive#about".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The user's storage quota limits and usage. All fields are measured in bytes.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub storage_quota: Option<StorageQuota>,

    /// A list of themes that are supported for shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub drive_themes: Option<Vec<DriveTheme>>,

    /// Whether the user can create shared drives.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub can_create_drives: Option<bool>,

    /// A map of source MIME type to possible targets for all supported imports.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub import_formats: Option< serde_json::Map<String, serde_json::Value> >,

    /// A map of source MIME type to possible targets for all supported exports.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub export_formats: Option< serde_json::Map<String, serde_json::Value> >,

    /// Whether the user has installed the requesting app.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub app_installed: Option<bool>,

    /// The authenticated user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<User>,

    /// The currently supported folder colors as RGB hex strings.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub folder_color_palette: Option<Vec<String>>,

    /// A map of maximum import sizes by MIME type, in bytes.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_import_sizes: Option< serde_json::Map<String, serde_json::Value> >,

    /// The maximum upload size in bytes.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub max_upload_size: Option<String>,
}

impl fmt::Display for About {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl About {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
