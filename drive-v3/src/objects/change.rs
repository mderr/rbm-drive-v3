use std::fmt;
use serde::{Serialize, Deserialize};

use super::{File, DriveInfo};

/// A list of changes for a user.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChangeList {
    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#changeList`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The page token for the next page of changes.
    ///
    /// This will be absent if the end of the changes list has been reached.
    /// The page token doesn't expire.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,

    /// The starting page token for future changes.
    ///
    /// This will be present only if the end of the current changes list has
    /// been reached. The page token doesn't expire.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub new_start_page_token: Option<String>,

    /// The list of changes.
    ///
    /// If [`next_page_token`](ChangeList::next_page_token) is populated, then
    /// this list may be incomplete and an additional page of results should be
    /// fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub changes: Option<Vec<Change>>,
}

impl fmt::Display for ChangeList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl ChangeList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A change to a file or shared drive.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Change {
    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#change`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// Whether the file or shared drive has been removed from this list of
    /// changes, for example by deletion or loss of access.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub removed: Option<bool>,

    /// The updated state of the file.
    ///
    /// Present if the type is file and the file has not been removed from this
    /// list of changes.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub file: Option<File>,

    /// The ID of the file which has changed.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub file_id: Option<String>,

    /// The time of this change (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub time: Option<String>,

    /// The ID of the shared drive associated with this change.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub drive_id: Option<String>,

    /// The type of the change.
    ///
    /// Possible values are `file` and `drive`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub change_type: Option<String>,

    /// The updated state of the shared drive.
    ///
    /// Present if the [`change_type`](Change::change_type) is `drive`, the user
    /// is still a member of the shared drive, and the shared drive has not been
    /// deleted.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub drive: Option<DriveInfo>,
}

impl fmt::Display for Change {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Change {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
