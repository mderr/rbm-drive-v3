use std::fmt;
use serde::{Serialize, Deserialize};

use super::User;

/// A reply to a comment on a file.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Reply {
    /// The ID of the reply.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#reply`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The time at which the comment was created (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,

    /// The last time the comment or any of its replies was modified
    /// (RFC 3339 date-time).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modified_time: Option<String>,

    /// The action the reply performed to the parent comment.
    ///
    /// Valid values are:
    /// - `resolve`
    /// - `reopen`
    #[serde(skip_serializing_if = "Option::is_none")]
    pub action: Option<String>,

    /// The author of the reply.
    ///
    /// The author's email address and permission ID will not be populated.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<User>,

    /// Whether the reply has been deleted.
    ///
    /// A deleted reply has no content.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub deleted: Option<bool>,

    /// The content of the reply with HTML formatting.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub html_content: Option<String>,

    /// The plain text content of the reply.
    ///
    /// This field is used for setting the content, while
    /// [`html_content`](Reply::html_content) should be displayed. This is
    /// required on creates if no `action` is specified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,
}

#[doc(hidden)]
impl From<&Self> for Reply {
    fn from( reference: &Self ) -> Self {
        reference.clone()
    }
}

impl fmt::Display for Reply {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Reply {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// A list of replies to a comment.
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ReplyList {
    /// The page token for the next page of replies.
    ///
    /// This will be absent if the end of the list has been reached. If the
    /// token is rejected for any reason, it should be discarded, and
    /// pagination should be restarted from the first page of results. The page
    /// token is typically valid for several hours. However, if new items are
    /// added or removed, your expected results might differ.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,

    /// Identifies what kind of resource this is.
    ///
    /// This is always `drive#replyList`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The list of replies.
    ///
    /// If [`next_page_token`](ReplyList::next_page_token) is populated,
    /// then this list may be incomplete and an additional page of results
    /// should be fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub replies: Option<Vec<Reply>>,
}

impl fmt::Display for ReplyList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl ReplyList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
