use std::fmt;
use serde::{Serialize, Deserialize};

/// List of a file's labels.
///
/// # Note
///
/// All of the fields of [`LabelList`] are an [`Option<T>`] since the values
/// that Google's API will return are dependent on the
/// [`fields`](crate::resources::files::ListLabelsRequest::fields) requested, by
/// default all fields will be requested but if you change this value only the
/// specified fields will be [`Some<T>`], the rest will be [`None`].
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LabelList {
    /// The page token for the next page of files.
    ///
    /// This will be absent if the end of the files list has been reached. If the token is rejected for any reason, it should be
    /// discarded, and pagination should be restarted from the first page of results. The page token is typically valid for
    /// several hours. However, if new items are added or removed, your expected results might differ.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub next_page_token: Option<String>,

    /// Identifies what kind of resource this is. Value: the fixed string "drive#labelList".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// The list of labels.
    ///
    /// If [`next_page_token`](LabelList::next_page_token) is populated, then this list may be incomplete and an additional page
    /// of results should be fetched.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub labels: Option<Vec<Label>>,
}

impl fmt::Display for LabelList {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl LabelList {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// An overview of the labels on the file.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LabelInfo {
    /// The set of labels on the file as requested by the label IDs in the includeLabels parameter.
    ///
    /// By default, no labels are returned.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub labels: Option<Vec<Label>>,
}

impl fmt::Display for LabelInfo {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl LabelInfo {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// Representation of label and label fields.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Label {
    /// The ID of the label.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    /// The revision ID of the label.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub revision_id: Option<String>,

    /// This is always `drive#label`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// A map of the fields on the label, keyed by the field's ID.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<serde_json::Map<String, serde_json::Value>>,
}

impl fmt::Display for Label {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl Label {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}

/// existing label on a file, or remove a label from a file.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[doc(hidden)]
pub struct ModifyLabelsRequest {
    /// The list of modifications to this label's fields.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label_modifications: Option<Vec<LabelModification>>,

    /// This is always `drive#modifyLabelsRequest`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
}

impl ModifyLabelsRequest {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }

    /// Creates an instance of this struct from the given
    /// [`field_modifications`](FieldModification).
    pub fn from( label_modifications: &[LabelModification] ) -> Self {
        Self {
            label_modifications: Some( label_modifications.to_vec() ),
            ..Default::default()
        }
    }
}

/// A modification to a label on a file.
///
/// A [`LabelModification`] can be used to apply a label to a file, update an
/// existing label on a file, or remove a label from a file.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LabelModification {
    /// The ID of the label to modify.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label_id: Option<String>,

    /// The list of modifications to this label's fields.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub field_modifications: Option<Vec<FieldModification>>,

    /// If true, the label will be removed from the file.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub remove_label: Option<bool>,

    /// This is always `drive#labelModification`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
}

impl fmt::Display for LabelModification {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl LabelModification {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }

    /// Creates an instance of this struct from the given
    /// [`field_modifications`](FieldModification).
    pub fn from<T: AsRef<str>> ( id: T, field_modifications: &[FieldModification] ) -> Self {
        Self {
            label_id: Some( id.as_ref().to_string() ),
            field_modifications: Some( field_modifications.to_vec() ),
            ..Default::default()
        }
    }
}

/// A modification to a [`Label`](Label)'s field.
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FieldModification {
    /// The ID of the field to be modified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub field_id: Option<String>,

    /// This is always `drive#labelFieldModification`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,

    /// Replaces the value of a date field with these new values.
    ///
    /// The string must be in the RFC 3339 full-date format: YYYY-MM-DD.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub set_date_values: Option<Vec<String>>,

    /// Sets the value of a `text` field.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub set_text_values: Option<Vec<String>>,

    /// Replaces a `selection` field with these new values.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub set_selection_values: Option<Vec<String>>,

    /// Replaces the value of an `integer` field with these new values.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub set_integer_values: Option<Vec<String>>,

    /// Replaces a `user` field with these new values.
    ///
    /// The values must be valid email addresses.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub set_user_values: Option<Vec<String>>,

    /// Unsets the values for this field.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub unset_values: Option<bool>,
}

impl fmt::Display for FieldModification {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        let json = serde_json::to_string_pretty(&self)
            .unwrap_or( format!("unable to parse JSON, this is the debug view:\n{:#?}", self) );

        write!(f, "{}", json)
    }
}

impl FieldModification {
    /// Creates a new, empty instance of this struct.
    pub fn new() -> Self {
        Self { ..Default::default() }
    }
}
