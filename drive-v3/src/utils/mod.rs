#[doc(hidden)]
pub mod upload;

#[cfg(test)]
pub mod test {
    use once_cell::sync::Lazy;
    use std::{sync::Mutex, process::Command, io};

    use crate::{server::LocalServer, ClientSecrets, Credentials};

    pub static VALID_SECRETS: Lazy<ClientSecrets> = Lazy::new( || {
        println!("[DEBUG]: initializing valid client secrets");

        let secrets_path = "../.secure-files/google_drive_secrets.json";

        ClientSecrets::from_file(secrets_path).unwrap()
    } );

    pub static VALID_CREDENTIALS: Lazy<Credentials> = Lazy::new( || {
        println!("[DEBUG]: initializing valid credentials");

        let credentials_path = "../.secure-files/google_drive_credentials.json";
        let scopes = ["https://www.googleapis.com/auth/drive.metadata.readonly"];

        let mut credentials = Credentials::from_file(credentials_path, &scopes).unwrap();

        if !credentials.are_valid() {
            println!("[DEBUG]: had to refresh credentials");

            credentials.refresh().unwrap();
            credentials.store(credentials_path).unwrap();
        }

        credentials
    } );

    pub static INVALID_CREDENTIALS: Lazy<Credentials> = Lazy::new( || {
        println!("[DEBUG]: initializing invalid credentials");

        let mut invalid_credentials = VALID_CREDENTIALS.clone();
        invalid_credentials.access_token.access_token = String::from("invalid-token");

        invalid_credentials
    } );

    /// Used to ensure that tests that curl the local server don't run in parallel
    pub static LOCAL_SERVER_IN_USE: Lazy<Mutex<u8>> = Lazy::new(|| {
        Mutex::new(0)
    });

    /// Used to ensure that tests that use local storage don't run in parallel
    pub static LOCAL_STORAGE_IN_USE: Lazy<Mutex<u8>> = Lazy::new(|| {
        Mutex::new(0)
    });

    pub fn curl_local_server( url: &str ) -> io::Result<String> {
        // Have to add a delay in order for the local server to bind to the port
        let time = ::std::time::Duration::from_millis(800);
        ::std::thread::sleep(time);

        let local_server = LocalServer::new();

        let output = Command::new("curl")
            .arg( format!("{}/{}", local_server.uri, url) )
            .output()?;

        Ok( String::from_utf8_lossy(&output.stdout).to_string() )
    }
}
