use crate::{Credentials, resources};

/// A Google Drive
/// [discovery document](https://developers.google.com/discovery/v1/reference/apis),
/// which is a machine-readable specification for describing and consuming REST
/// APIs.
///
/// It is used to build client libraries, IDE plugins, and other tools
/// that interact with Google APIs.
///
/// This discovery document contains the Google Drive API requests which are
/// currently implemented in [`drive_v3`](crate).
#[derive(Debug, Clone)]
pub struct Drive {
    /// Uses the
    /// [v3.about](https://developers.google.com/drive/api/reference/rest/v3/about)
    /// resource to get information about the user, the user's Drive, and system
    /// capabilities.
    pub about: resources::About,

    /// Uses the
    /// [v3.apps](https://developers.google.com/drive/api/reference/rest/v3/apps)
    /// resource to get information about a user's apps.
    pub apps: resources::Apps,

    /// Uses the
    /// [v3.changes](https://developers.google.com/drive/api/reference/rest/v3/changes)
    /// resource to get information about a change to a file or shared drive.
    pub changes: resources::Changes,

    /// Uses the
    /// [v3.channels](https://developers.google.com/drive/api/reference/rest/v3/channels)
    /// resource to manage channels.
    pub channels: resources::Channels,

    /// Uses the
    /// [v3.comments](https://developers.google.com/drive/api/reference/rest/v3/comments)
    /// resource to manage comments.
    pub comments: resources::Comments,

    /// Uses the
    /// [v3.drives](https://developers.google.com/drive/api/reference/rest/v3/drives)
    /// resource to manage comments.
    pub drives: resources::Drives,

    /// Uses the
    /// [v3.files](https://developers.google.com/drive/api/reference/rest/v3/files)
    /// resource to get information about the user's files.
    pub files: resources::Files,

    /// Uses the
    /// [v3.permissions](https://developers.google.com/drive/api/reference/rest/v3/permissions)
    /// resource to get information about permissions.
    pub permissions: resources::Permissions,

    /// Uses the
    /// [v3.replies](https://developers.google.com/drive/api/reference/rest/v3/replies)
    /// resource to get information about replies.
    pub replies: resources::Replies,

    /// Uses the
    /// [v3.revisions](https://developers.google.com/drive/api/reference/rest/v3/revisions)
    /// resource to get information about replies.
    pub revisions: resources::Revisions,
}

impl Drive {
    /// Creates a new `Drive` discovery document, with the given [`Credentials`].
    pub fn new( credentials: &Credentials ) -> Self {
        Self {
            about: resources::About::new(credentials),
            apps: resources::Apps::new(credentials),
            changes: resources::Changes::new(credentials),
            channels: resources::Channels::new(credentials),
            comments: resources::Comments::new(credentials),
            drives: resources::Drives::new(credentials),
            files: resources::Files::new(credentials),
            permissions: resources::Permissions::new(credentials),
            replies: resources::Replies::new(credentials),
            revisions: resources::Revisions::new(credentials),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Drive;
    use crate::utils::test::VALID_CREDENTIALS;
    use crate::resources::{About, Apps, Files, Changes, Channels, Comments,
        Drives, Permissions, Replies, Revisions};

    #[test]
    fn new_test() {
        let credentials = VALID_CREDENTIALS.clone();
        let drive = Drive::new(&credentials);

        assert_eq!( drive.about, About::new(&credentials) );
        assert_eq!( drive.apps, Apps::new(&credentials) );
        assert_eq!( drive.changes, Changes::new(&credentials) );
        assert_eq!( drive.channels, Channels::new(&credentials) );
        assert_eq!( drive.comments, Comments::new(&credentials) );
        assert_eq!( drive.drives, Drives::new(&credentials) );
        assert_eq!( drive.files, Files::new(&credentials) );
        assert_eq!( drive.permissions, Permissions::new(&credentials) );
        assert_eq!( drive.replies, Replies::new(&credentials) );
        assert_eq!( drive.revisions, Revisions::new(&credentials) );
    }
}
