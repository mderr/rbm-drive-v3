use std::fmt;
use regex::Regex;
use syn::token::Comma;
use syn::parse::Parser;
use proc_macro2::Ident;
use quote::format_ident;
use syn::spanned::Spanned;
use syn::punctuated::Punctuated;
use syn::{Expr, Lit, ExprLit, LitStr, Meta, MetaNameValue};

use crate::utils::error;
use crate::symbols::ATTRIBUTE_MACRO_NAME;

#[derive(Debug)]
pub enum Argument {
    Method(Expr),
    Url(LitStr),
    ReturnType(Option<Expr>),
    ReturnBytes(bool),
}

#[cfg(not(tarpaulin_include))]
impl fmt::Display for Argument {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        match &self {
            Self::Method(_) => write!(f, "method"),
            Self::Url(_) => write!(f, "url"),
            Self::ReturnType(_) => write!(f, "returns"),
            Self::ReturnBytes(_) => write!(f, "returns_bytes"),
        }
    }
}

impl Argument {
    pub fn variants() -> [&'static str; 4] {
        [
            "method",
            "url",
            "returns",
            "returns_bytes",
        ]
    }

    fn get_supported_arguments_string() -> String {
        Self::variants().iter()
            .map( |kind| format!("`{}`", kind) )
            .collect::<Vec<String>> ()
            .join(", ")
    }
}

impl TryFrom<&MetaNameValue> for Argument {
    type Error = syn::Error;

    fn try_from( value: &MetaNameValue ) -> Result<Self, Self::Error> {
        let path_identifier = value.path.require_ident()?;

        match path_identifier.to_string().as_str() {
            "method" => Ok(
                Self::Method( value.value.clone() )
            ),
            "url" => {
                let literal_str = match &value.value {
                    Expr::Lit( ExprLit{ lit: syn::Lit::Str(literal), .. } ) => {
                        literal.clone()
                    },
                    _ => return Err( error::spanned(
                        &value.value,
                        format!("value of `{}` must be a string literal", path_identifier)
                    ) ),
                };

                Ok( Self::Url(literal_str) )
            },
            "returns" => Ok(
                Self::ReturnType( Some(value.value.clone()) )
            ),
            "returns_bytes" => {
                if let Expr::Lit(literal) = &value.value {
                    if let Lit::Bool(bool) = &literal.lit {
                        return Ok( Self::ReturnBytes(bool.value) )
                    }
                }

                Err( error::spanned(
                    &value.value,
                    format!(
                        "value of `returns_bytes` must be a bool\n\
                        help: give `returns_bytes` a boolean value: `{}=true`",
                        path_identifier,
                    ),
                ) )
            },
            _ => Err( error::spanned(
                path_identifier,
                format!(
                    "unrecognized `{}` argument\nhelp: supported arguments are: {}",
                    path_identifier,
                    Self::get_supported_arguments_string(),
                ),
            ) ),
        }
    }
}

#[derive(Debug)]
pub struct Arguments {
    pub method: Expr,
    pub url: LitStr,
    pub return_type: Option<Expr>,
    pub return_bytes: bool,
}

impl Arguments {
    pub fn get_url_fragments( &self ) -> syn::Result< Vec<Ident> > {
        let mut url_fragment_identifiers = Vec::new();

        let fragments_regex = match Regex::new(r"\{(.*?)\}") {
            Ok(regex) => regex,
            Err(error) => return Err( error::spanned(&self.url, error) ),
        };

        let url_string = self.url.token().to_string();

        for ( _, [fragment] ) in fragments_regex.captures_iter(&url_string).map(| c| c.extract() ) {
            url_fragment_identifiers.push( format_ident!("{}", fragment) );
        }

        Ok(url_fragment_identifiers)
    }
}

impl TryFrom<&proc_macro2::TokenStream> for Arguments {
    type Error = syn::Error;

    fn try_from( value: &proc_macro2::TokenStream ) -> Result<Self, Self::Error> {
        let parsed_arguments: Punctuated<Meta, Comma> = Punctuated::parse_terminated.parse2( value.clone() )?;

        if parsed_arguments.is_empty() {
            return Err( error::spanned(
                parsed_arguments,
                format!(
                    "the `{attribute}` macro requires name-value arguments\nhelp: consider giving `{attribute}` some arguments: `#[{attribute}(name=value, other=value)]`",
                    attribute=ATTRIBUTE_MACRO_NAME,
                ),
            ) )
        }

        let mut request_method = None;
        let mut request_url = None;
        let mut request_return_type = None;
        let mut request_return_bytes = None;

        for meta in &parsed_arguments {
            match meta {
                Meta::NameValue(name_value) => {
                    let path_identifier = name_value.path.require_ident()?;

                    match Argument::try_from(name_value)? {
                        Argument::Method(method) => {
                            match request_method {
                                Some(previous) => return Err( error::multiple_definition(path_identifier, &[
                                    previous,
                                    method,
                                ]) ),
                                None => request_method = Some(method),
                            };
                        },
                        Argument::Url(url) => {
                            match request_url {
                                Some(previous) => return Err( error::multiple_definition(path_identifier, &[
                                    previous,
                                    url,
                                ]) ),
                                None => request_url = Some(url),
                            };
                        },
                        Argument::ReturnType(return_type) => {
                            match request_return_type {
                                Some(previous) => return Err( error::multiple_definition(path_identifier, &[
                                    previous,
                                    return_type,
                                ]) ),
                                None => request_return_type = Some(return_type),
                            };
                        },
                        Argument::ReturnBytes(return_bytes) => {
                            match request_return_bytes {
                                Some(previous) => return Err( error::multiple_definition(path_identifier, &[
                                    previous,
                                    return_bytes,
                                ]) ),
                                None => request_return_bytes = Some(return_bytes),
                            };
                        },
                    };
                },
                Meta::Path(path) => {
                    let default_ident = syn::Ident::new("argument", path.span());
                    let argument_name = path.get_ident().unwrap_or(&default_ident).to_string();

                    let fixed_arguments = value.to_string().replace(
                        &argument_name,
                        &format!("{}=value", argument_name)
                    );

                    return Err( error::spanned(
                        path,
                        format!(
                            "the `{attribute}` macro requires name-value arguments\nhelp: consider giving `{}` a value: `#[{attribute}({})]`",
                            argument_name,
                            fixed_arguments,
                            attribute=ATTRIBUTE_MACRO_NAME,
                        )
                    ) )
                },
                Meta::List(list) => {
                    return Err( error::spanned(
                        list,
                        format!(
                            "the `{}` macro requires name-value arguments\nhelp: consider using only one of the values",
                            ATTRIBUTE_MACRO_NAME,
                        )
                    ) )
                }
            }
        }

        let method = request_method.ok_or( error::spanned(
            &parsed_arguments,
            format!(
                "missing required `{argument}` argument\nhelp: consider adding `{argument}` to the macro call: `#[{}({}, {argument}=value)]`",
                ATTRIBUTE_MACRO_NAME,
                value,
                argument="method",
            ),
        ) )?;

        let url = request_url.ok_or( error::spanned(
            &parsed_arguments,
            format!(
                "missing required `{argument}` argument\nhelp: consider adding `{argument}` to the macro call: `#[{}({}, {argument}=value)]`",
                ATTRIBUTE_MACRO_NAME,
                value,
                argument="url",
            ),
        ) )?;

        let return_type = request_return_type.and_then( |ty| ty );
        let return_bytes = request_return_bytes.unwrap_or(false);

        Ok( Self{
            method,
            url,
            return_type,
            return_bytes,
        } )
    }
}

#[cfg(test)]
mod tests {
    use quote::quote;
    use proc_macro2::TokenStream;
    use syn::{MetaNameValue, Expr, LitStr};

    use super::{Argument, Arguments};

    #[test]
    fn invalid_argument_test() {
        let meta_name_value: MetaNameValue = syn::parse_str("invalid=true").unwrap();
        let argument = Argument::try_from(&meta_name_value);

        assert!( argument.is_err() );
    }

    #[test]
    fn method_argument_test() {
        let meta_name_value: MetaNameValue = syn::parse_str("method=true").unwrap();
        let argument = Argument::try_from(&meta_name_value).unwrap();

        assert!(
            matches!( argument, Argument::Method { .. } )
        );
    }

    #[test]
    fn url_argument_test() {
        let meta_name_value: MetaNameValue = syn::parse_str("url=\"test\"").unwrap();
        let argument = Argument::try_from(&meta_name_value).unwrap();

        assert!(
            matches!( argument, Argument::Url { .. } )
        );
    }

    #[test]
    fn invalid_url_argument_test() {
        let meta_name_value: MetaNameValue = syn::parse_str("url=1").unwrap();
        let argument = Argument::try_from(&meta_name_value);

        assert!( argument.is_err() );
    }

    #[test]
    fn returns_argument_test() {
        let meta_name_value: MetaNameValue = syn::parse_str("returns=1").unwrap();
        let argument = Argument::try_from(&meta_name_value).unwrap();

        assert!(
            matches!( argument, Argument::ReturnType { .. } )
        );
    }

    #[test]
    fn returns_bytes_argument_test() {
        let meta_name_value: MetaNameValue = syn::parse_str("returns_bytes=true").unwrap();
        let argument = Argument::try_from(&meta_name_value).unwrap();

        assert!(
            matches!( argument, Argument::ReturnBytes { .. } )
        );
    }

    #[test]
    fn invalid_returns_bytes_argument_test() {
        let meta_name_value: MetaNameValue = syn::parse_str("returns_bytes=47").unwrap();
        let argument = Argument::try_from(&meta_name_value);

        assert!( argument.is_err() );
    }

    #[test]
    fn arguments_test() {
        let method: Expr = syn::parse_str("reqwest::Method::GET").unwrap();
        let url: LitStr = syn::parse_str("\"test-url\"").unwrap();
        let return_type: Expr = syn::parse_str("String").unwrap();
        let return_bytes = true;

        let tokens: TokenStream = quote!{ method=#method, url=#url, returns=#return_type, returns_bytes=#return_bytes };
        let arguments = Arguments::try_from(&tokens).unwrap();

        assert_eq!(arguments.method, method);
        assert_eq!(arguments.url, url);
        assert_eq!( arguments.return_type, Some(return_type) );
        assert_eq!(arguments.return_bytes, return_bytes);
    }

    #[test]
    fn multiple_methods_test() {
        let tokens: TokenStream = quote!{ method=String, method=u8 };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn multiple_urls_test() {
        let tokens: TokenStream = quote!{ url="one", url="two" };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn multiple_returns_test() {
        let tokens: TokenStream = quote!{ returns=String, returns=i32 };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn multiple_returns_bytes_test() {
        let tokens: TokenStream = quote!{ returns_bytes=false, returns_bytes=true };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn arguments_defaults_test() {
        let method: Expr = syn::parse_str("reqwest::Method::GET").unwrap();
        let url: LitStr = syn::parse_str("\"test-url\"").unwrap();

        let tokens: TokenStream = quote!{ method=#method, url=#url };
        let arguments = Arguments::try_from(&tokens).unwrap();

        assert_eq!(arguments.method, method);
        assert_eq!(arguments.url, url);
        assert_eq!(arguments.return_type, None);
        assert_eq!(arguments.return_bytes, false);
    }

    #[test]
    fn empty_arguments_test() {
        let tokens: TokenStream = quote!{};
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn missing_arguments_test() {
        let tokens: TokenStream = quote!{ returns_bytes=true };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );

        let tokens: TokenStream = quote!{ method=test, returns_bytes=true };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn list_arguments_test() {
        let tokens: TokenStream = quote!{ method(1, 2) };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn path_arguments_test() {
        let tokens: TokenStream = quote!{ method };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn invalid_url_arguments_test() {
        let method: Expr = syn::parse_str("reqwest::Method::GET").unwrap();
        let url: Expr = syn::parse_str("34").unwrap();

        let tokens: TokenStream = quote!{ method=#method, url=#url };
        let arguments = Arguments::try_from(&tokens);

        assert!( arguments.is_err() );
    }

    #[test]
    fn get_url_fragments_test() {
        let tokens: TokenStream = quote!{ method=String, url="test_url/{fragment_1}/path/{fragment_2}/other/test" };
        let arguments = Arguments::try_from(&tokens).unwrap();

        let expected_fragments = vec![
            String::from("fragment_1"),
            String::from("fragment_2"),
        ];

        let fragments = arguments.get_url_fragments().unwrap();

        for (index, fragment) in fragments.iter().enumerate() {
            assert_eq!( fragment.to_string(), expected_fragments[index] )
        }
    }

    #[test]
    fn get_empty_url_fragments_test() {
        let tokens: TokenStream = quote!{ method=String, url="test_url/fragment_1/path/fragment_2/other/test" };
        let arguments = Arguments::try_from(&tokens).unwrap();

        let fragments = arguments.get_url_fragments().unwrap();

        assert!( fragments.is_empty() )
    }
}
