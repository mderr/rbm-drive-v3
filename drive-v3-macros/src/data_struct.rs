use quote::ToTokens;
use syn::spanned::Spanned;
use syn::{Field, Fields, DeriveInput, DataStruct, Ident};

use crate::utils::error;
use crate::utils::types::TypeUtils;
use crate::field::get_standard_fields;
use crate::symbols::{ATTRIBUTE_MACRO_NAME, DERIVE_MACRO_NAME};

pub trait DataStructUtils {
    fn get_option_fields( &self ) -> Vec<Field>;
    fn require_option_fields( &self ) -> syn::Result< Vec<Field> >;
    fn require_standard_request_fields( &self ) -> syn::Result<()>;
    fn extend_fields( &mut self, fields: &[Field] ) -> syn::Result<()>;
}

impl DataStructUtils for DataStruct {
    fn get_option_fields( &self ) -> Vec<Field> {
        let mut option_fields = Vec::new();

        if let Fields::Named(fields) = &self.fields {
            for field in &fields.named {
                if field.ty.is_an_option() {
                    option_fields.push( field.clone() )
                }
            }
        }

        option_fields
    }

    fn require_option_fields( &self ) -> syn::Result< Vec<Field> > {
        let mut option_fields = Vec::new();

        if let Fields::Named(fields) = &self.fields {
            for field in &fields.named {
                if !field.ty.is_an_option() {
                    let default_ident = Ident::new( "field", field.span() );

                    return Err( error::spanned(
                        &field.ty,
                        format!(
                            "fields of a struct that derives `{}` must be optional\n\
                            help: consider making `{field}` optional: `{field}: Option<{}>`\n",
                            DERIVE_MACRO_NAME,
                            field.ty.to_token_stream(),
                            field=field.ident.as_ref().unwrap_or(&default_ident),
                        )
                    ) )
                }

                option_fields.push( field.clone() )
            }
        }

        Ok(option_fields)
    }

    fn require_standard_request_fields( &self ) -> syn::Result<()> {
        if let Fields::Named(fields) = &self.fields {
            for standard_field in get_standard_fields()? {
                let exists_in_struct = fields.named.iter()
                    .any( |existing_field| existing_field == &standard_field );

                if !exists_in_struct {
                    return Err( error::spanned(
                        &self.fields,
                        format!(
                            "structs that derive the `{}` trait must have the standard request fields\n\
                            help: consider using the `{attribute}` attribute to add them: `#[{attribute}(method=Method::GET, url=\"url\")]`",
                            DERIVE_MACRO_NAME,
                            attribute=ATTRIBUTE_MACRO_NAME,
                        ),
                    ) )
                }
            }
        }

        Ok(())
    }

    fn extend_fields( &mut self, fields: &[Field] ) -> syn::Result<()> {
        match &mut self.fields {
            Fields::Named(current_fields) => {
                for field in fields {
                    current_fields.named.push( field.clone() );
                }

                Ok(())
            }
            Fields::Unnamed(current_fields) => {
                let mut fixed_fields = Vec::new();

                for (index, field) in current_fields.unnamed.iter().enumerate() {
                    fixed_fields.push( format!(
                        "field_{}: {}",
                        index + 1,
                        field.ty.to_token_stream(),
                    ) )
                }

                let help_message = fixed_fields.join(", ");

                Err( error::spanned(
                    current_fields,
                    format!(
                        "the `{}` attribute cannot be applied to structs with unnamed fields\n\
                        help: consider giving named fields to this struct: `{{ {} }}`",
                        ATTRIBUTE_MACRO_NAME,
                        help_message,
                    ),
                ) )
            },
            Fields::Unit => Err( error::spanned(
                self.struct_token,
                format!(
                    "the `{}` attribute cannot be applied to unit structs\n\
                    help: consider using an empty struct: `struct MyStruct {{}};`",
                    ATTRIBUTE_MACRO_NAME,
                ),
            ) ),
        }
    }
}

pub fn get_derive_data_struct( ast: &DeriveInput ) -> syn::Result<&DataStruct> {
    for attribute in &ast.attrs {
        if attribute.path().is_ident(ATTRIBUTE_MACRO_NAME) {
            return Err( error::spanned(
                attribute,
                format!(
                    "the `{attribute}` attribute must be placed before `#[derive({derive})]`\n\
                    note: this is required since `{attribute}` adds fields to the struct which will not be present when it is used after `derive`\n\
                    help: consider moving `{}` before `#[derive({derive})]`",
                    attribute.to_token_stream(),
                    derive=DERIVE_MACRO_NAME,
                    attribute=ATTRIBUTE_MACRO_NAME,
                ),
            ) )
        }
    }

    match ast.data {
        syn::Data::Struct(ref data_struct) => Ok(data_struct),
        syn::Data::Enum(ref data_enum) => Err( error::spanned(
            data_enum.enum_token,
            format!(
                "the `{}` trait cannot be derived for `Enums`, only `Structs` with named fields are supported",
                DERIVE_MACRO_NAME,
            ),
        ) ),
        syn::Data::Union(ref data_union) => Err( error::spanned(
            data_union.union_token,
            format!(
                "the `{}` trait cannot be derived for `Unions`, only `Structs` with named fields are supported",
                DERIVE_MACRO_NAME,
            ),
        ) ),
    }
}

pub fn get_attribute_data_struct( ast: &mut DeriveInput ) -> syn::Result<&mut DataStruct> {
    match ast.data {
        syn::Data::Struct(ref mut data_struct) => Ok(data_struct),
        syn::Data::Enum(ref data_enum) => Err( error::spanned(
            data_enum.enum_token,
            format!(
                "the `{}` attribute cannot be applied to `Enums`, only `Structs` with named fields are supported",
                ATTRIBUTE_MACRO_NAME,
            ),
        ) ),
        syn::Data::Union(ref data_union) => Err( error::spanned(
            data_union.union_token,
            format!(
                "the `{}` attribute cannot be applied to `Unions`, only `Structs` with named fields are supported",
                ATTRIBUTE_MACRO_NAME,
            ),
        ) ),
    }
}

#[cfg(test)]
mod tests {
    use quote::ToTokens;
    use syn::{DataStruct, DeriveInput};

    use crate::field::get_standard_fields;
    use super::{DataStructUtils, get_derive_data_struct, get_attribute_data_struct};

    fn get_invalid_test_struct() -> DataStruct {
        let derive_input: DeriveInput = syn::parse_str("
            struct TestStruct {
                field_0: String,
                field_1: Option<u8>,
                field_2: Vec<i32>,
            }
        ").unwrap();

        get_derive_data_struct(&derive_input).unwrap().clone()
    }

    fn get_test_struct() -> DataStruct {
        let derive_input: DeriveInput = syn::parse_str("
            struct TestStruct {
                field_0: Option<String>,
                field_1: Option<u8>,
                field_2: Option<Vec<i32>>,
            }
        ").unwrap();

        get_derive_data_struct(&derive_input).unwrap().clone()
    }

    fn get_expected_field_strings() -> Vec<String> {
        vec![
            String::from("field_0 : Option < String >"),
            String::from("field_1 : Option < u8 >"),
            String::from("field_2 : Option < Vec < i32 > >"),
        ]
    }

    fn get_test_struct_with_standard_fields() -> DataStruct {
        let mut derive_input: DeriveInput = syn::parse_str("
            struct TestStruct {
                field_0: Option<String>,
                field_1: Option<u8>,
                field_2: Option<Vec<i32>>,
            }
        ").unwrap();

        let data_struct = get_attribute_data_struct(&mut derive_input).unwrap();
        data_struct.extend_fields( &get_standard_fields().unwrap() ).unwrap();

        data_struct.clone()
    }

    #[test]
    fn get_option_fields_test() {
        let test_struct = get_test_struct();
        let option_fields = test_struct.get_option_fields();

        let expected = get_expected_field_strings();

        for (index, field) in option_fields.iter().enumerate() {
            assert_eq!( field.to_token_stream().to_string(), expected[index] );
        }
    }

    #[test]
    fn require_option_fields_test() {
        let test_struct = get_test_struct();
        let option_fields = test_struct.require_option_fields().unwrap();

        let expected = get_expected_field_strings();

        for (index, field) in option_fields.iter().enumerate() {
            assert_eq!( field.to_token_stream().to_string(), expected[index] );
        }
    }

    #[test]
    fn invalid_require_option_fields_test() {
        let test_struct = get_invalid_test_struct();
        let option_fields = test_struct.require_option_fields();

        assert!( option_fields.is_err() );
    }

    #[test]
    fn require_standard_request_fields_test() {
        let test_struct = get_test_struct_with_standard_fields();
        let option_fields = test_struct.require_standard_request_fields();

        assert!( option_fields.is_ok() );
    }

    #[test]
    fn invalid_require_standard_request_fields_test() {
        let test_struct = get_invalid_test_struct();
        let option_fields = test_struct.require_standard_request_fields();

        assert!( option_fields.is_err() );
    }

    #[test]
    fn unnamed_extend_fields_test() {
        let mut derive_input: DeriveInput = syn::parse_str("
            struct TestStruct(String, u8);
        ").unwrap();

        let data_struct = get_attribute_data_struct(&mut derive_input).unwrap();
        let result = data_struct.extend_fields( &get_standard_fields().unwrap() );

        assert!( result.is_err() );
    }

    #[test]
    fn unit_extend_fields_test() {
        let mut derive_input: DeriveInput = syn::parse_str("
            struct TestStruct;
        ").unwrap();

        let data_struct = get_attribute_data_struct(&mut derive_input).unwrap();
        let result = data_struct.extend_fields( &get_standard_fields().unwrap() );

        assert!( result.is_err() );
    }

    #[test]
    fn derive_bad_attribute_order_test() {
        let derive_input: DeriveInput = syn::parse_str("
            #[request(method=String, url=\"test\")]
            struct TestStruct {
                field_0: Option<String>,
                field_1: Option<u8>,
                field_2: Option<Vec<i32>>,
            }
        ").unwrap();

        let data_struct = get_derive_data_struct(&derive_input);

        assert!( data_struct.is_err() );
    }

    #[test]
    fn derive_enum_test() {
        let derive_input: DeriveInput = syn::parse_str("
            enum TestEnum {
                One,
                Two,
                Three,
            }
        ").unwrap();

        let data_struct = get_derive_data_struct(&derive_input);

        assert!( data_struct.is_err() );
    }

    #[test]
    fn derive_union_test() {
        let derive_input: DeriveInput = syn::parse_str("
            union TestUnion {}
        ").unwrap();

        let data_struct = get_derive_data_struct(&derive_input);

        assert!( data_struct.is_err() );
    }

    #[test]
    fn attribute_enum_test() {
        let mut derive_input: DeriveInput = syn::parse_str("
            enum TestEnum {
                One,
                Two,
                Three,
            }
        ").unwrap();

        let data_struct = get_attribute_data_struct(&mut derive_input);

        assert!( data_struct.is_err() );
    }

    #[test]
    fn attribute_union_test() {
        let mut derive_input: DeriveInput = syn::parse_str("
            union TestUnion {}
        ").unwrap();

        let data_struct = get_attribute_data_struct(&mut derive_input);

        assert!( data_struct.is_err() );
    }
}
