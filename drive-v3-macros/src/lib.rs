mod utils;
mod field;
mod symbols;
mod request;
mod data_struct;
mod derive_macro;
mod attribute_macro;

#[proc_macro_attribute]
#[cfg(not(tarpaulin_include))]
pub fn request( args: proc_macro::TokenStream, input: proc_macro::TokenStream ) -> proc_macro::TokenStream {
    let result = match attribute_macro::implement_for( args.into(), input.into() ) {
        Ok(tokens) => tokens,
        Err(error) => error.to_compile_error(),
    };

    result.into()
}

#[proc_macro_derive( DriveRequestBuilder, attributes(drive_v3) )]
#[cfg(not(tarpaulin_include))]
pub fn drive_request_builder_derive( token_stream: proc_macro::TokenStream ) -> proc_macro::TokenStream {
    let result = match derive_macro::implement_for( token_stream.into() ) {
        Ok(tokens) => tokens,
        Err(error) => error.to_compile_error(),
    };

    result.into()
}
