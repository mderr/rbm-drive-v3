use syn::Type;

pub trait TypeUtils {
    fn is_an_option( &self ) -> bool;
}

impl TypeUtils for Type {
    fn is_an_option( &self ) -> bool {
        if let Type::Path(path) = self {
            let type_segments: Vec<String> = path.path.segments.iter()
                .map( |s| s.ident.to_string() )
                .collect();

            let type_as_string = type_segments.join("::");

            return matches!(
                type_as_string.as_ref(),
                "Option" |
                "std::option::Option" |
                "::std::option::Option" |
                "core::option::Option" |
                "::core::option::Option",
            )
        }

        false
    }
}

#[cfg(test)]
mod tests {
    use syn::Type;
    use super::TypeUtils;

    const NON_OPTIONAL_TYPES: [&str; 4] = [
        "String",
        "alloc::vec::Vec<u8>",
        "u8",
        "::syn::Expr",
    ];

    const OPTIONAL_TYPES: [&str; 5] = [
        "Option<T>",
        "std::option::Option<String>",
        "::std::option::Option<i32>",
        "core::option::Option<usize>",
        "::core::option::Option<u8>",
    ];

    #[test]
    fn non_optional_test() {
        for non_optional_type in NON_OPTIONAL_TYPES {
            let ty: Type = syn::parse_str(non_optional_type).unwrap();

            assert!( !ty.is_an_option() );
        }
    }

    #[test]
    fn optional_test() {
        for optional_type in OPTIONAL_TYPES {
            let ty: Type = syn::parse_str(optional_type).unwrap();

            assert!( ty.is_an_option() );
        }
    }
}
