use syn::Expr;

pub trait ExprUtils {
    fn is_empty_tuple( &self ) -> bool;
}

impl ExprUtils for Expr {
    fn is_empty_tuple( &self ) -> bool {
        if let Expr::Tuple(tuple) = self {
            return tuple.elems.is_empty()
        }

        false
    }

}

#[cfg(test)]
mod tests {
    use syn::Expr;
    use super::ExprUtils;

    const NON_EMPTY_TUPLES: [&str; 3] = [
        "(1, 2)",
        "(String, u8, usize)",
        "( \"1\", \"test\" )",
    ];

    const NON_TUPLES: [&str; 5] = [
        "String",
        "test",
        "test_function(1, 2)",
        "Vec",
        "test=true",
    ];

    #[test]
    fn empty_tuple_test() {
        let expression: Expr = syn::parse_str("()").unwrap();

        assert!( expression.is_empty_tuple() )
    }

    #[test]
    fn non_empty_tuple_test() {
        for non_empty_tuple in NON_EMPTY_TUPLES {
            let expression: Expr = syn::parse_str(non_empty_tuple).unwrap();

            assert!( !expression.is_empty_tuple() );
        }
    }

    #[test]
    fn non_tuple_test() {
        for non_tuple in NON_TUPLES {
            let expression: Expr = syn::parse_str(non_tuple).unwrap();

            assert!( !expression.is_empty_tuple() );
        }
    }
}
