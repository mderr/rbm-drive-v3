use std::fmt;
use proc_macro2::Span;

pub fn multiple_definition<N, T> ( name: &N, tokens: &[T] ) -> syn::Error
    where
        N: fmt::Display,
        T: quote::ToTokens,
{
    match tokens.len() {
        0 => syn::Error::new(
            Span::call_site(),
            format!("`{}` was defined multiple times", name)
        ),
        1 => spanned(
            &tokens[0],
            format!("`{}` was defined multiple times", name)
        ),
        _ => {
            let mut tokens_iter = tokens.iter();

            let mut error = spanned(
                tokens_iter.next(),
                format!("multiple definitions of `{}`", name)
            );

            tokens_iter.for_each( |token| {
                error.combine( spanned(
                    token,
                    format!("value of `{}` redefined here", name)
                ) );
            } );

            error
        },
    }
}

pub fn spanned<T, M> ( tokens: T, message: M ) -> syn::Error
    where
        T: quote::ToTokens,
        M: fmt::Display,
{
    syn::Error::new_spanned(tokens, message)
}

pub fn not_spanned<T: fmt::Display> ( message: T ) -> syn::Error {
    syn::Error::new( Span::call_site(), message )
}
