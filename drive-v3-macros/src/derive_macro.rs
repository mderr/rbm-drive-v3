use quote::quote;
use syn::DeriveInput;
use proc_macro2::TokenStream;

use crate::symbols::get_drive_v3_ident;
use crate::field::{FieldKind, FieldArrayUtils};
use crate::data_struct::{get_derive_data_struct, DataStructUtils};

pub fn implement_for( token_stream: TokenStream ) -> syn::Result<TokenStream> {
    let ast: DeriveInput = syn::parse2(token_stream)?;
    let struct_name = &ast.ident;
    let drive_v3 = get_drive_v3_ident()?;

    let data_struct = get_derive_data_struct(&ast)?;

    data_struct.require_standard_request_fields()?;

    let existing_fields = data_struct.get_option_fields();

    let parameter_fields = existing_fields.of_kind(FieldKind::Parameter)?;
    let body_fields = existing_fields.of_kind(FieldKind::Body)?;

    let parameter_identifiers= parameter_fields.get_identifiers()?;
    let body_identifiers= body_fields.get_identifiers()?;

    let request_body: Option<TokenStream> = body_identifiers.first()
        .map( |body_identifier| {
            quote!(
                .header(
                    "Content-Length",
                    ::serde_json::to_string(
                        &self.#body_identifier.clone().unwrap_or_default()
                    )?
                    .as_bytes()
                    .len()
                    .to_string()
                )
                .body(
                    ::serde_json::to_string(
                        &self.#body_identifier.clone().unwrap_or_default()
                    )?
                )
            )
        }
    );

    let generated_tokens = quote! {
        impl DriveRequestBuilder for #struct_name {
            /// Gets the current parameters for this request.
            fn get_parameters( &self ) -> Vec<(String, String)> {
                let mut parameters = Vec::new();

                if let Some(fields) = &self.fields {
                    parameters.push(( "fields".into(), fields.to_string() ));
                }

                #(
                    if let Some(value) = &self.#parameter_identifiers {
                        use convert_case::{Case, Casing};

                        let mut parameter_name = stringify!(#parameter_identifiers)
                            .from_case(Case::Snake)
                            .to_case(Case::Camel);

                        // Rename kind to type since type is reserved
                        if &parameter_name == "kind" {
                            parameter_name = "type".to_string();
                        }

                        parameters.push(( parameter_name, value.to_string() ));
                    }
                )*

                parameters
            }

            /// Builds this request and returns a reqwest [`RequestBuilder`](::reqwest::blocking::RequestBuilder).
            fn build( &self ) -> #drive_v3::Result<::reqwest::blocking::RequestBuilder> {
                let parameters = self.get_parameters();
                let request_url = ::reqwest::Url::parse_with_params(&self.url, parameters)?;

                Ok(
                    ::reqwest::blocking::Client::new()
                        .request( self.method.clone(), request_url )
                        .bearer_auth( self.credentials.get_access_token() )
                        #request_body
                )
            }

            /// Sends the built request.
            #[allow(dead_code)] // More complex request don't use this
            fn send( &self ) -> #drive_v3::Result<::reqwest::blocking::Response> {
                let request = self.build()?;
                let response = request.send()?;

                if !response.status().is_success() {
                    return Err( response.into() );
                }

                Ok(response)
            }
        }
    };

    Ok(generated_tokens)
}

#[cfg(test)]
mod tests {
    use quote::quote;
    use trybuild::TestCases;
    use proc_macro2::TokenStream;

    use super::implement_for;

    #[test]
    #[ignore = "causes hanging in (CI/CD)"]
    fn compile_tests() {
        TestCases::new().compile_fail("tests/ui/derive_macro/*.rs");
    }

    #[test]
    fn implement_test() {
        let tokens: TokenStream = quote!{
            struct TestStruct {
                credentials: drive_v3::Credentials,
                method: ::reqwest::Method,
                url: String ,
                fields: Option<String>,

                #[drive_v3(body)]
                field: Option<String>,
            }
        };

        let result = implement_for(tokens);

        assert!( result.is_ok() );
    }
}
