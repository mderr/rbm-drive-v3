use syn::Ident;
use proc_macro2::Span;
use proc_macro_crate::{crate_name, FoundCrate};

use crate::utils::error;

pub const ATTRIBUTE_MACRO_NAME: &str = "request";
pub const DERIVE_MACRO_NAME: &str = "DriveRequestBuilder";
pub const HELPER_ATTRIBUTE_NAME: &str = "drive_v3";
pub const DRIVE_V3_CRATE_NAME: &str = "drive-v3";

pub fn get_drive_v3_ident() -> syn::Result<Ident> {
    let found_crate = match crate_name(DRIVE_V3_CRATE_NAME) {
        Ok(found) => found,
        Err(_) => return Err( error::not_spanned(format!(
            "this macro requires the `{}` crate to be a dependency or dev-dependency",
            DRIVE_V3_CRATE_NAME,
        )) ),
    };

    let crate_identifier = match found_crate {
        FoundCrate::Itself => Ident::new( "crate", Span::call_site() ),
        FoundCrate::Name(name) => Ident::new( &name, Span::call_site() ),
    };

    Ok(crate_identifier)
}

#[cfg(test)]
mod tests {
    use super::get_drive_v3_ident;

    #[test]
    fn crate_test() {
        let crate_symbol = get_drive_v3_ident().unwrap().to_string();

        assert_eq!(&crate_symbol, "drive_v3");
    }
}
