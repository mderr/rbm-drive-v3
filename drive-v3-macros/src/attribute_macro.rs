use quote::quote;
use syn::DeriveInput;
use proc_macro2::TokenStream;

use crate::request;
use crate::utils::expr::ExprUtils;
use crate::symbols::get_drive_v3_ident;
use crate::field::{FieldKind, FieldArrayUtils, get_standard_fields};
use crate::data_struct::{DataStructUtils, get_attribute_data_struct};

pub fn implement_for( arguments: TokenStream, input: TokenStream ) -> syn::Result<TokenStream> {
    let mut ast: DeriveInput = syn::parse2(input)?;
    let struct_name = ast.ident.clone();
    let drive_v3 = get_drive_v3_ident()?;

    let data_struct = get_attribute_data_struct(&mut ast)?;
    let existing_fields = data_struct.require_option_fields()?;

    let parameter_fields = existing_fields.of_kind(FieldKind::Parameter)?;
    let callback_fields = existing_fields.of_kind(FieldKind::Callback)?;
    let body_fields = existing_fields.of_kind(FieldKind::Body)?;
    let other_fields = existing_fields.of_kind(FieldKind::Other)?;

    let parameter_identifiers = parameter_fields.get_identifiers()?;
    let callback_identifiers = callback_fields.get_identifiers()?;
    let body_identifiers = body_fields.get_identifiers()?;
    let other_identifiers = other_fields.get_identifiers()?;

    let parameter_types = parameter_fields.get_inner_types()?;
    let callback_types = callback_fields.get_inner_types()?;
    let body_types = body_fields.get_inner_types()?;
    let other_types = other_fields.get_inner_types()?;

    let parameter_docs = parameter_fields.get_docs()?;
    let callback_docs = callback_fields.get_docs()?;
    let body_docs = body_fields.get_docs()?;
    let other_docs = other_fields.get_docs()?;

    let request_arguments = request::Arguments::try_from(&arguments)?;

    let request_method = &request_arguments.method;
    let request_url = &request_arguments.url;
    let url_fragments = request_arguments.get_url_fragments()?;

    data_struct.extend_fields( &get_standard_fields()? )?;

    let default_execute_function = request_arguments.return_type.map( |returns| {
        let parsed_return = match request_arguments.return_bytes {
            true => quote!{ Ok( response.bytes()?.into() ) },
            false if returns.is_empty_tuple() => quote!{ Ok(()) },
            false => quote!{ Ok( ::serde_json::from_str( &response.text()? )? ) },
        };

        quote! {
            /// Executes this request.
            ///
            /// # Errors:
            ///
            /// - a [`UrlParsing`](drive_v3::ErrorKind::UrlParsing) error, if the creation of the request's URL failed.
            /// - a [`Request`](drive_v3::ErrorKind::Request) error, if unable to send the request or get a body from the response.
            /// - a [`Response`](drive_v3::ErrorKind::Response) error, if the request returned an error response.
            /// - a [`Json`](drive_v3::ErrorKind::Json) error, if unable to parse the response's body to into the return type.
            pub fn execute( &self ) -> #drive_v3::Result<#returns> {
                let response = self.send()?;

                #parsed_return
            }
        }
    } );

    let new_function_docs = format!(
        "Creates a new [`{}`] builder, authorized with the given [`Credentials`]({}::Credentials).",
        struct_name,
        drive_v3
    );

    let generated_tokens = quote! {
        #[derive(Debug, Clone)]
        #ast

        impl #struct_name {
            #[doc = #new_function_docs]
            pub fn new( credentials: &#drive_v3::Credentials, #(#url_fragments: impl AsRef<str>),* ) -> Self {
                Self {
                    credentials: credentials.clone(),
                    method: #request_method,
                    url: format!(
                        #request_url,
                        #( #url_fragments = #url_fragments.as_ref() ),*
                    ),
                    fields: None,
                    #( #parameter_identifiers: None, )*
                    #( #callback_identifiers: None, )*
                    #( #body_identifiers: None, )*
                    #( #other_identifiers: None, )*
                }
            }

            #default_execute_function

            /// You can set this parameter to return the exact fields you need,
            /// and improve performance in your method call.
            ///
            /// # Note:
            ///
            /// By default, the server sends back a set of fields specific to the
            /// resource being queried. For example, the files.get method might
            /// only return the id, name, and mimeType for the files resource.
            /// The permissions.get method returns a different set of default
            /// fields for a permissions resource.
            ///
            /// See Google's
            /// [documentation](https://developers.google.com/drive/api/guides/fields-parameter)
            pub fn fields<T> ( mut self, value: T ) -> Self
                where
                    T: Into<String>
            {
                self.fields = Some( value.into() );

                self
            }

            #(
                #( #[doc = #parameter_docs] )*
                pub fn #parameter_identifiers<T> ( mut self, value: T ) -> Self
                    where
                        T: Into<#parameter_types>
                {
                    self.#parameter_identifiers = Some( value.into() );

                    self
                }
            )*

            #(
                #( #[doc = #callback_docs] )*
                pub fn #callback_identifiers( mut self, function: #callback_types ) -> Self {
                    self.#callback_identifiers = Some(function);

                    self
                }
            )*

            #(
                #( #[doc = #body_docs] )*
                pub fn #body_identifiers<T> ( mut self, value: T ) -> Self
                    where
                        T: Into<#body_types>
                {
                    self.#body_identifiers = Some( value.into() );

                    self
                }
            )*

            #(
                #( #[doc = #other_docs] )*
                pub fn #other_identifiers<T> ( mut self, value: T ) -> Self
                    where
                        T: Into<#other_types>
                {
                    self.#other_identifiers = Some( value.into() );

                    self
                }
            )*
        }
    };

    Ok(generated_tokens)
}

#[cfg(test)]
mod tests {
    use quote::quote;
    use trybuild::TestCases;
    use proc_macro2::TokenStream;

    use super::implement_for;

    #[test]
    #[ignore = "causes hanging in (CI/CD)"]
    fn compile_tests() {
        TestCases::new().compile_fail("tests/ui/attribute_macro/*.rs");
    }

    #[test]
    fn implement_test() {
        let input: TokenStream = quote!{
            struct TestStruct {
                #[drive_v3(parameter)]
                field_0: Option<String>,

                #[drive_v3(parameter)]
                field_1: Option<u8>,

                #[drive_v3(body)]
                field_2: Option<i32>,

                #[drive_v3(callback)]
                field_3: Option<String>,
            }
        };

        let arguments: TokenStream = quote!{ method=::reqwest::Method::GET, url="test/{arg}/url", returns=() };

        let result = implement_for(arguments, input);

        assert!( result.is_ok() );
    }

    #[test]
    fn implement_returns_bytes_test() {
        let input: TokenStream = quote!{
            struct TestStruct {
                #[drive_v3(parameter)]
                field_0: Option<String>,

                #[drive_v3(parameter)]
                field_1: Option<u8>,

                #[drive_v3(body)]
                field_2: Option<i32>,

                #[drive_v3(callback)]
                field_3: Option<String>,
            }
        };

        let arguments: TokenStream = quote!{ method=::reqwest::Method::GET, url="test/{arg}/url", returns=Vec::<u8>, returns_bytes=true };

        let result = implement_for(arguments, input);

        assert!( result.is_ok() );
    }

    #[test]
    fn implement_returns_test() {
        let input: TokenStream = quote!{
            struct TestStruct {
                #[drive_v3(parameter)]
                field_0: Option<String>,

                #[drive_v3(parameter)]
                field_1: Option<u8>,

                #[drive_v3(body)]
                field_2: Option<i32>,

                #[drive_v3(callback)]
                field_3: Option<String>,
            }
        };

        let arguments: TokenStream = quote!{ method=::reqwest::Method::GET, url="test/{arg}/url", returns=String };

        let result = implement_for(arguments, input);

        assert!( result.is_ok() );
    }
}
