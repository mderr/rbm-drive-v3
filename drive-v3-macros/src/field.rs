use std::fmt;
use syn::parse::Parser;
use syn::spanned::Spanned;
use quote::{ToTokens, quote};
use syn::punctuated::Punctuated;
use syn::{Expr, Ident, Meta, Path, Type, Field};

use crate::utils::error;
use crate::symbols::get_drive_v3_ident;
use crate::symbols::HELPER_ATTRIBUTE_NAME;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum FieldKind {
    Parameter,
    Callback,
    Body,
    Other,
}

impl fmt::Display for FieldKind {
    fn fmt( &self, f: &mut fmt::Formatter<'_> ) -> fmt::Result {
        match self {
            Self::Parameter => write!(f, "parameter"),
            Self::Callback => write!(f, "callback"),
            Self::Body => write!(f, "body"),
            Self::Other => write!(f, "other"),
        }
    }
}

impl FieldKind {
    pub fn variants() -> [Self; 4] {
        [
            Self::Parameter,
            Self::Callback,
            Self::Body,
            Self::Other,
        ]
    }

    fn get_supported_kinds_string() -> String {
        Self::variants().iter()
            .map( |kind| format!("`{}`", kind) )
            .collect::<Vec<String>> ()
            .join(", ")
    }
}

impl TryFrom<&Path> for FieldKind {
    type Error = syn::Error;

    fn try_from( value: &Path ) -> Result<Self, Self::Error> {
        let path_identifier = value.require_ident()?;

        match path_identifier.to_string().as_str() {
            "parameter" => Ok(Self::Parameter),
            "callback" => Ok(Self::Callback),
            "body" => Ok(Self::Body),
            "other" => Ok(Self::Other),
            _ => Err( error::spanned(
                value,
                format!(
                    "unrecognized `{}` argument\nhelp: supported arguments are: {}",
                    path_identifier,
                    Self::get_supported_kinds_string(),
                ),
            ) ),
        }
    }
}

pub trait FieldArrayUtils {
    fn of_kind( &self, kind: FieldKind ) -> syn::Result< Vec<Field> >;
    fn get_identifiers( &self ) -> syn::Result< Vec<&Ident> >;
    fn get_inner_types( &self ) -> syn::Result< Vec<&Type> >;
    fn get_docs( &self ) -> syn::Result< Vec<Vec<&Expr>> >;
}

impl FieldArrayUtils for [Field] {
    fn of_kind( &self, kind: FieldKind ) -> syn::Result< Vec<Field> > {
        let mut fields_of_kind = Vec::new();

        for field in self {
            let mut field_has_drive_attribute = false;

            for attribute in &field.attrs {
                if attribute.path().is_ident(HELPER_ATTRIBUTE_NAME) {
                    field_has_drive_attribute = true;

                    match &attribute.meta {
                        Meta::List(attribute) => {
                            let parsed_arguments = attribute.parse_args_with(
                                Punctuated::<Meta, syn::token::Comma>::parse_terminated
                            )?;

                            if parsed_arguments.is_empty() {
                                return Err( error::spanned(
                                    attribute,
                                    format!(
                                        "the `{attribute}` attribute requires a path\n\
                                        help: consider using an argument: `#[{attribute}({})]`",
                                        FieldKind::Parameter,
                                        attribute=HELPER_ATTRIBUTE_NAME,
                                    )
                                ) )
                            }

                            if parsed_arguments.len() > 1 {
                                return Err( error::spanned(
                                    attribute,
                                    format!(
                                        "the `{attribute}` attribute requires a single path\n\
                                        help: consider using only one of the arguments: `#[{attribute}({})]`",
                                        parsed_arguments[0].to_token_stream(),
                                        attribute=HELPER_ATTRIBUTE_NAME,
                                    )
                                ) )
                            }

                            for argument in parsed_arguments {
                                match argument {
                                    syn::Meta::Path(path) => {
                                        if kind == FieldKind::try_from(&path)? {
                                            fields_of_kind.push( field.clone() );
                                        }
                                    },
                                    _ => return Err( error::spanned(
                                        &argument,
                                        format!(
                                            "the `{attribute}` attribute requires a path\n\
                                            help: consider using: `#[{attribute}({})]`",
                                            argument.path().get_ident().unwrap_or( &syn::Ident::new("arg", argument.span()) ),
                                            attribute=HELPER_ATTRIBUTE_NAME,
                                        )
                                    ) ),
                                };
                            };
                        },
                        Meta::NameValue(name_value) => {
                            let fixed_arguments = format!(
                                "{}({})",
                                HELPER_ATTRIBUTE_NAME,
                                name_value.value.to_token_stream()
                            );

                            return Err( error::spanned(
                                name_value,
                                format!(
                                    "the `{}` attribute requires a path\n\
                                    help: consider using the value: `#[{}]`",
                                    HELPER_ATTRIBUTE_NAME,
                                    fixed_arguments,
                                )
                            ) )
                        } ,
                        Meta::Path(path) => return Err( error::spanned(
                            path,
                            format!(
                                "the `{attribute}` attribute requires a list\n\
                                help: consider giving the attribute an argument `#[{attribute}({})]`",
                                FieldKind::Parameter,
                                attribute=HELPER_ATTRIBUTE_NAME,
                            )
                        ) ),
                    };
                }
            }

            if !field_has_drive_attribute && kind == FieldKind::Other {
                fields_of_kind.push( field.clone() );
            }
        }

        if kind == FieldKind::Body && fields_of_kind.len() > 1 {
            return Err( error::multiple_definition(
                &FieldKind::Body,
                &fields_of_kind
            ) );
        }

        Ok(fields_of_kind)
    }

    fn get_identifiers( &self ) -> syn::Result< Vec<&Ident> > {
        let mut identifiers = Vec::new();

        for field in self {
            identifiers.push( field.require_ident()? );
        }

        Ok(identifiers)
    }

    fn get_inner_types( &self ) -> syn::Result< Vec<&Type> > {
        let mut types = Vec::new();

        for field in self {
            types.push( field.get_inner_type()? );
        }

        Ok(types)
    }

    fn get_docs( &self ) -> syn::Result< Vec<Vec<&Expr>> > {
        let mut docs = Vec::new();

        for field in self {
            docs.push( field.get_docs()? );
        }

        Ok(docs)
    }
}

pub trait FieldUtils {
    fn require_ident( &self ) -> syn::Result<&Ident>;
    fn get_inner_type( &self ) -> syn::Result<&Type>;
    fn get_docs( &self ) -> syn::Result< Vec<&Expr> >;
}

impl FieldUtils for Field {
    fn require_ident( &self ) -> syn::Result<&Ident> {
        self.ident.as_ref().ok_or( error::spanned(
            self,
            "unable to get identifier of this field",
        ) )
    }

    fn get_inner_type( &self ) -> syn::Result<&Type> {
        let field_type = match self.ty {
            syn::Type::Path(ref type_path) if type_path.qself.is_none() => Some(&type_path.path),
            _ => None,
        }
        .and_then( |path| path.segments.last() )
        .and_then( |path_seg| {
            let type_params = &path_seg.arguments;

            // It should have only on angle-bracketed param ("<String>"):
            match *type_params {
                syn::PathArguments::AngleBracketed(ref params) => params.args.first(),
                _ => None,
            }
        })
        .and_then(|generic_arg| match *generic_arg {
            syn::GenericArgument::Type(ref ty) => Some(ty),
            _ => None,
        });

        field_type.ok_or( error::spanned(
            &self.ty,
            "unable to get the inner type of this option",
        ) )
    }

    fn get_docs( &self ) -> syn::Result< Vec<&Expr> > {
        let mut field_doc_strings = Vec::new();

        for attribute in &self.attrs {
            if attribute.path().is_ident("doc") {
                match &attribute.meta {
                    Meta::NameValue(name_value) => {
                        field_doc_strings.push(&name_value.value);
                    },
                    _ => return Err( error::spanned(
                        &attribute.meta,
                        "unable to get docstring for this field"
                    ) ),
                }
            }
        }

        Ok(field_doc_strings)
    }
}

pub fn get_standard_fields() -> syn::Result< Vec<Field> > {
    let drive_v3 = get_drive_v3_ident()?;

    Ok( vec![
        Field::parse_named.parse2( quote! { credentials: #drive_v3::Credentials } )?,
        Field::parse_named.parse2( quote! { method: ::reqwest::Method } )?,
        Field::parse_named.parse2( quote! { url: String } )?,
        Field::parse_named.parse2( quote! { fields: Option<String> } )?,
    ] )
}

#[cfg(test)]
mod tests {
    use quote::ToTokens;
    use syn::{Path, Field, parse_quote};

    use super::{FieldKind, FieldArrayUtils};

    fn get_test_fields() -> Vec<Field> {
        vec![
            parse_quote! {
                #[drive_v3(parameter)]
                field_0: String
            },
            parse_quote! {
                #[drive_v3(parameter)]
                field_1: usize
            },
            parse_quote! { field_2: u64 },
            parse_quote! {
                #[drive_v3(body)]
                field_3: i32
            },
            parse_quote! {
                #[drive_v3(callback)]
                field_4: u8
            },
            parse_quote! {
                #[drive_v3(parameter)]
                field_5: Option<String>
            },
            parse_quote! {
                #[drive_v3(callback)]
                field_6: Option<usize>
            },
            parse_quote! { field_7: u64 },
        ]
    }

    #[test]
    fn kind_test() {
        for variant in FieldKind::variants() {
            let path: Path = syn::parse_str( &format!("{}", variant) ).unwrap();
            let kind = FieldKind::try_from(&path).unwrap();

            assert_eq!(kind, variant);
        }
    }

    #[test]
    fn invalid_kind_test() {
        let path: Path = syn::parse_str("invalid").unwrap();
        let kind = FieldKind::try_from(&path);

        assert!( kind.is_err() );
    }

    #[test]
    fn fields_of_kind_test() {
        let fields = get_test_fields();
        let parameter_fields = fields.of_kind(FieldKind::Parameter).unwrap();

        assert_eq!( parameter_fields.len(), 3 );

        assert_eq!( parameter_fields[0], fields[0] );
        assert_eq!( parameter_fields[1], fields[1] );
        assert_eq!( parameter_fields[2], fields[5] );

        let callback_fields = fields.of_kind(FieldKind::Callback).unwrap();

        assert_eq!( callback_fields.len(), 2 );

        assert_eq!( callback_fields[0], fields[4] );
        assert_eq!( callback_fields[1], fields[6] );

        let body_fields = fields.of_kind(FieldKind::Body).unwrap();

        assert_eq!( body_fields.len(), 1 );

        assert_eq!( body_fields[0], fields[3] );

        let other_fields = fields.of_kind(FieldKind::Other).unwrap();

        assert_eq!( other_fields.len(), 2 );

        assert_eq!( other_fields[0], fields[2] );
        assert_eq!( other_fields[1], fields[7] );
    }

    #[test]
    fn invalid_fields_of_kind_test() {
        let fields: Vec<Field> = vec![
            parse_quote! {
                #[drive_v3(parameter=true)]
                field_0: String
            }
        ];

        let parameter_fields = fields.of_kind(FieldKind::Parameter);

        assert!( parameter_fields.is_err() );

        let fields: Vec<Field> = vec![
            parse_quote! {
                #[drive_v3(parameter(1, 2))]
                field_0: String
            }
        ];

        let parameter_fields = fields.of_kind(FieldKind::Parameter);

        assert!( parameter_fields.is_err() );
    }

    #[test]
    fn path_attribute_fields_of_kind_test() {
        let fields: Vec<Field> = vec![
            parse_quote! {
                #[drive_v3]
                field_0: String
            }
        ];

        let parameter_fields = fields.of_kind(FieldKind::Parameter);

        assert!( parameter_fields.is_err() );
    }

    #[test]
    fn name_value_attribute_fields_of_kind_test() {
        let fields: Vec<Field> = vec![
            parse_quote! {
                #[drive_v3=false]
                field_0: String
            }
        ];

        let parameter_fields = fields.of_kind(FieldKind::Parameter);

        assert!( parameter_fields.is_err() );
    }

    #[test]
    fn empty_list_fields_of_kind_test() {
        let fields: Vec<Field> = vec![
            parse_quote! {
                #[drive_v3()]
                field_0: String
            }
        ];

        let parameter_fields = fields.of_kind(FieldKind::Parameter);

        assert!( parameter_fields.is_err() );
    }

    #[test]
    fn multiple_paths_fields_of_kind_test() {
        let fields: Vec<Field> = vec![
            parse_quote! {
                #[drive_v3(callback, parameter)]
                field_0: String
            }
        ];

        let parameter_fields = fields.of_kind(FieldKind::Parameter);

        assert!( parameter_fields.is_err() );
    }

    #[test]
    fn multiple_body_fields_of_kind_test() {
        let fields: Vec<Field> = vec![
            parse_quote! {
                #[drive_v3(body)]
                field_0: String
            },
            parse_quote! {
                #[drive_v3(body)]
                field_1: u8
            }
        ];

        let body_fields = fields.of_kind(FieldKind::Body);

        assert!( body_fields.is_err() );
    }

    #[test]
    fn get_identifiers_test() {
        let fields = get_test_fields();

        let identifiers = fields.get_identifiers().unwrap();

        for i in 0..fields.len() {
            let identifier_as_string = format!("field_{}", i);

            assert_eq!( identifier_as_string, identifiers[i].to_string() );
        }
    }

    #[test]
    fn unnamed_fields_get_identifiers_test() {
        let fields: Vec<Field> = vec![
            parse_quote! {
                String
            }
        ];

        let parameter_fields = fields.get_identifiers();

        assert!( parameter_fields.is_err() );
    }

    #[test]
    fn get_inner_types_test() {
        let fields =  vec![
            parse_quote! {
                field_0: ::core::option::Option<String>
            },
            parse_quote! {
                field_1: Option<usize>
            },
        ];

        let expected_types = vec![
            String::from("String"),
            String::from("usize"),
        ];

        let types = fields.get_inner_types().unwrap();

        for i in 0..fields.len() {
            assert_eq!( types[i].to_token_stream().to_string(), expected_types[i] );
        }
    }

    #[test]
    fn non_option_inner_types_test() {
        let fields =  vec![
            parse_quote! {
                field_0: String
            }
        ];

        let types = fields.get_inner_types();

        assert!( types.is_err() );
    }

    #[test]
    fn non_path_inner_types_test() {
        let fields =  vec![
            parse_quote! {
                field_0: [String; 23]
            }
        ];

        let types = fields.get_inner_types();

        assert!( types.is_err() );
    }

    #[test]
    fn non_generic_inner_types_test() {
        let fields =  vec![
            parse_quote! {
                field_0: Option<'a>
            }
        ];

        let types = fields.get_inner_types();

        assert!( types.is_err() );
    }

    #[test]

    fn get_docs_test() {
        let fields =  vec![
            parse_quote! {
                /// this is a test doc
                field_0: ::core::option::Option<String>
            },
            parse_quote! {
                #[doc = "another doc, but this one is"]
                #[doc = "multi-line"]
                field_1: Option<usize>
            },
            parse_quote! {
                // No docs
                field_2: Option<i32>
            },
        ];

        let expected_docs = vec![
            vec!["r\" this is a test doc\""],
            vec![
                "\"another doc, but this one is\"",
                "\"multi-line\""
            ],
            Vec::new(),
        ];

        let docs = fields.get_docs().unwrap();

        for (doc_index, doc) in docs.iter().enumerate() {
            for (line_index, line) in doc.iter().enumerate() {
                assert_eq!( line.to_token_stream().to_string(), expected_docs[doc_index][line_index] );
            }
        }
    }

    #[test]

    fn get_invalid_docs_test() {
        let fields =  vec![
            parse_quote! {
                #[doc(not_valid)]
                field_0: ::core::option::Option<String>
            }
        ];

        let docs = fields.get_docs();

        assert!( docs.is_err() );
    }
}
