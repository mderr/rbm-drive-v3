use drive_v3_macros::{DriveRequestBuilder, request};

#[request(method=reqwest::Method::GET, url="test")]
#[derive(DriveRequestBuilder)]
struct Test1 {
    #[drive_v3(parameter=true)]
    parameter: Option<String>,
}

#[request(method=reqwest::Method::GET, url="test")]
#[derive(DriveRequestBuilder)]
struct Test2 {
    #[drive_v3(body(1, 2))]
    body: Option<String>,
}

#[request(method=reqwest::Method::GET, url="test")]
#[derive(DriveRequestBuilder)]
struct Test3 {
    #[drive_v3="test"]
    field: Option<String>,
}

#[request(method=reqwest::Method::GET, url="test")]
#[derive(DriveRequestBuilder)]
struct Test4 {
    #[drive_v3]
    field: Option<String>,
}

#[request(method=reqwest::Method::GET, url="test")]
#[derive(DriveRequestBuilder)]
struct Test5 {
    #[drive_v3(parameter, body)]
    field: Option<String>,
}

fn main() {}
