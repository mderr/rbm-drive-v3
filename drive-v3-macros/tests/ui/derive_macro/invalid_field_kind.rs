use drive_v3_macros::{DriveRequestBuilder, request};

#[request(method=reqwest::Method::GET, url="test")]
#[derive(DriveRequestBuilder)]
struct Test1 {
    #[drive_v3(invalid)]
    field: Option<String>
}

fn main() {}
