use drive_v3_macros::{DriveRequestBuilder, request};

#[derive(DriveRequestBuilder)]
struct Test1 {}

#[derive(DriveRequestBuilder)]
#[request(method=reqwest::Method::GET, url="test")]
struct Test2 {}

fn main() {}
