use drive_v3_macros::{DriveRequestBuilder, request};

#[request(method=reqwest::Method::GET, url="test")]
#[derive(DriveRequestBuilder)]
struct Test1 {
    #[drive_v3(body)]
    body_1: Option<String>,

    #[drive_v3(body)]
    body_2: Option<u8>,
}

fn main() {}
