use drive_v3_macros::request;

#[request(method, url="test")]
struct Test1 {}

#[request(method=reqwest::Method::GET, url("test", "test"))]
struct Test2 {}

fn main() {}
