use drive_v3_macros::request;

const STATIC_STR: &str = "test";

#[request(method=reqwest::Method::GET, url=STATIC_STR)]
struct Test1 {}

#[request(method=reqwest::Method::GET, url=42)]
struct Test2 {}

fn main() {}
