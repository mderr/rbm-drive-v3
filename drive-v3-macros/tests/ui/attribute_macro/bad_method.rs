use drive_v3_macros::request;

const STATIC_STR: &str = "test";

#[request(method=21, url="test")]
struct Test1 {}

#[request(method=STATIC_STR, url="test")]
struct Test2 {}

fn main() {}
