use drive_v3_macros::request;

#[request(method=reqwest::Method::GET, url="test")]
struct Test1(String, u64);

#[request(method=reqwest::Method::GET, url="test")]
struct Test2;

#[request(method=reqwest::Method::GET, url="test")]
enum Test3 {}

#[request(method=reqwest::Method::GET, url="test")]
union Test4 {}

fn main() {}
