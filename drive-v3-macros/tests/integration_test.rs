use reqwest::Method;
use drive_v3::Credentials;
use drive_v3::resources::DriveRequestBuilder;
use drive_v3_macros::{DriveRequestBuilder, request};

#[request(
    method=Method::POST,
    url="https://www.googleapis.com/drive/v3/files/{file_id}/copy",
    returns=(),
)]
#[derive(DriveRequestBuilder)]
struct TestStruct {
    field: Option<String>
}

#[test]
fn integration_test() {
    let scopes: Vec<&str> = Vec::new();
    let credentials = Credentials::from_file(
        "../.secure-files/google_drive_credentials.json",
        &scopes,
    ).unwrap();

    let mut test_struct = TestStruct::new(&credentials, "test_file_id");

    println!("{:#?}", test_struct);
    assert_eq!(test_struct.credentials, credentials);
    assert_eq!(test_struct.method, Method::POST);
    assert_eq!(&test_struct.url, "https://www.googleapis.com/drive/v3/files/test_file_id/copy");
    assert_eq!(test_struct.fields, None);
    assert_eq!(test_struct.field, None);

    test_struct = test_struct.field("test");

    assert_eq!( test_struct.field, Some(String::from("test")) );

    test_struct = test_struct.fields("test");

    assert_eq!( test_struct.fields, Some(String::from("test")) );
}
